#include "buildings.hpp"

//BUILDINGS 
static castle cas;
static Rectangle castle_floor;
static float floor_size=200.0;

//TEXTURES
static vector<Image> textures;
static vec2 bricks_size_repeat=vec2(5.0f,5.0f);
static vec2 donjon_bricks_size_repeat=vec2(10.0f,10.0f);
//static vec2 floor_repeat=vec2(3.0,3.0);
static vec2 floor_floor_repeat=vec2(5.0,5.0);

static vec3 light_color = vec3(1.0,0.5,0.1);

static vector<Pbuilding> all_buildings;

static Pbuilding pbu;
static void new_Sbuild()
{
	pbu.pavers.resize(0);
}
template<typename... TArgs>
static void add_Sbuild(TArgs... args)
{
	pbu.colorp(args...);
	all_buildings.push_back(pbu);
}

static void add_Pbuild(Hexahedron_solid p)
{
	p.move(cas.position + vec3(cas.tour_size.x, 0.0, cas.tour_size.z));
	pbu.pavers.push_back(p);
}
static vec3 ctt()
{
	return cas.total_length();
}

static vec3 donjon_size=vec3(5.0,18.0,10.0);
static vec3 Sdonjon_size=vec3(1.0,donjon_size.y,6);
static vec2 vbridge_params=vec2(2.0, donjon_size.y*2.0f/3.0f);
static vec3 vbridge=vec3(3.0,donjon_size.y,donjon_size.z/2.0);
static float back_terr_len=5.0;
static float terr_piler=2.0;
static vec2 back_piler=vec2(terr_piler+1.5, 1.0);
static vec3 support_plan=vec3(1.0, 0.5, 1.0);
static float oplan_house_dec=support_plan.x;
static float oplan_house_h=4.0;

static vec3 light_donjon_d1=vec3(12.0,0.0,1.0);
static vec3 light_donjon_d2=vec3(-2.0,0.0,12.0);
static vec3 light_donjon_d3=vec3(-11.0,9.0,-13.0);
static vec3 light_donjon_h=vec3(0.0,1.0,0.0);

static Light castle_light(vec3 pos, float intensity=70.0 , float aaa=4.0f, float aaaa=0.3f)
{
	return Light(pos, intensity, light_color, aaa, aaaa);
} 

static Hexahedron_solid add_upper_block(vec3 pos, vec3 down_size, vec3 up_size)
{
	return Hexahedron_solid(pos+vec3((down_size.x-up_size.x)/2.0,down_size.y,(down_size.z-up_size.z)/2.0)
			,up_size);
}

static void add_tour(vec3 pos, float h=15.0)
{
	vec3 big_tour_size = vec3(4.0,h,4.0);
	vec3 big_tour_lvl1 = vec3(4.5,0.8,4.5);
	vec3 big_tour_lvl2 = vec3(5.0,4.0,5.0);
	vec3 big_tour_lvl3 = vec3(6.0,0.5,6.0);
	vec3 big_tour_lvl4 = vec3(4.5,1.0,4.5);


	vec3 bgt=pos;
	vec3 bgd=bgt;
	new_Sbuild();
		add_Pbuild(Hexahedron_solid(bgt,
			big_tour_size));
		bgd=big_tour_size;
	add_Sbuild(textures[MARBER], donjon_bricks_size_repeat/2.0f);

	new_Sbuild();
		add_Pbuild(add_upper_block(bgt, bgd, big_tour_lvl1));
	add_Sbuild(textures[MARBER], donjon_bricks_size_repeat/2.0f);

	new_Sbuild();
		bgd+=vec3(0.0,big_tour_lvl1.y,0.0);
		add_Pbuild(add_upper_block(bgt, bgd, big_tour_lvl2));
		bgd+=vec3(0.0,big_tour_lvl2.y,0.0);
	add_Sbuild(textures[BRICKS], donjon_bricks_size_repeat/4.0f);



	new_Sbuild();
		add_Pbuild(add_upper_block(bgt, bgd, big_tour_lvl3));
		bgd+=vec3(0.0,big_tour_lvl3.y,0.0);
		add_Pbuild(add_upper_block(bgt, bgd, big_tour_lvl4));
	add_Sbuild(textures[ROOTH], donjon_bricks_size_repeat/2.0f);
}

static void build_secondary_buldings()
{
	all_buildings.resize(0);

	vec3 bd=ctt()/2.0f- proj_floor(donjon_size/2.0f);

	new_Sbuild();
		add_light(castle_light(bd-light_donjon_d1+light_donjon_h,70.0, 5.0,0.0));
		add_light(castle_light(bd-light_donjon_d2+light_donjon_h,70.0, 5.0,0.0));
		add_light(castle_light(bd+donjon_size+vec3(-19.0,6.0+oplan_house_h,-22.0),150.0, 3.0,0.2));
		add_light(castle_light(bd+donjon_size+vec3(1.0,3.0+oplan_house_h,-7.0),120.0, 3.0,0.2));
		add_light(castle_light(bd+light_donjon_d3+light_donjon_h,150.0, 5.0,0.0));
		add_Pbuild(Hexahedron_solid(bd
						,donjon_size));
		add_Pbuild(Hexahedron_solid(bd-vec3(Sdonjon_size.x,0.0,-donjon_size.z/2.0+Sdonjon_size.z/2.0)
						,Sdonjon_size));
		add_Pbuild(Hexahedron_solid(bd+vec3(donjon_size.x, 0.0, donjon_size.z*2.0/3.0)
						,vec3(2.0, donjon_size.y, donjon_size.z/5)));
		Hexahedron_solid ht(bd+vec3(donjon_size.x,vbridge_params.y,0.0)
							,vec3(0.0,-vbridge_params.x,0.0)
							,vec3(vbridge.x, vbridge.y-vbridge_params.y, 0.0)
							,vec3(0.0,0.0,vbridge.z));
		add_Pbuild(ht);
		add_Pbuild(Hexahedron_solid(bd+vec3(donjon_size.x+vbridge.x, donjon_size.y-vbridge_params.x,0.0)
						,vec3(back_terr_len, vbridge_params.x, donjon_size.z/2.0)));
		add_Pbuild(Hexahedron_solid(bd+vec3(donjon_size.x+vbridge.x+back_terr_len/2.0-terr_piler/2.0, back_piler.y,back_terr_len/2.0-terr_piler/2.0)
						,vec3(terr_piler,donjon_size.y-vbridge_params.x-2*back_piler.y, terr_piler)));
		add_Pbuild(Hexahedron_solid(bd+vec3(donjon_size.x+vbridge.x+back_terr_len/2.0-back_piler.x/2.0, 0.0,back_terr_len/2.0-back_piler.x/2.0)
						,vec3(back_piler.x,back_piler.y, back_piler.x)));
		add_Pbuild(Hexahedron_solid(bd+vec3(donjon_size.x+vbridge.x+back_terr_len/2.0-back_piler.x/2.0, donjon_size.y- vbridge_params.x -back_piler.y,back_terr_len/2.0-back_piler.x/2.0)
						,vec3(back_piler.x,back_piler.y, back_piler.x)));
		vec3 support_plan_bg=bd-vec3(Sdonjon_size.x + support_plan.x, -donjon_size.y ,support_plan.z);
		vec3 support_plan_size=support_plan*2.0f+vec3(Sdonjon_size.x+donjon_size.x+vbridge.x+back_terr_len,0.0,donjon_size.z);
		add_Pbuild(Hexahedron_solid(support_plan_bg
						,support_plan_size));
		add_Pbuild(Hexahedron_solid(support_plan_bg+vec3(0.0, oplan_house_h, 0.0)
						,support_plan_size));
		add_Pbuild(Hexahedron_solid(support_plan_bg+vec3(oplan_house_dec/2.0, oplan_house_h+2*support_plan_size.y, oplan_house_dec/2.0)
						,vec3(support_plan_size.x-oplan_house_dec, support_plan_size.y/2.0, support_plan_size.z-oplan_house_dec)));
	add_Sbuild(textures[MARBER], donjon_bricks_size_repeat/2.0f);

	new_Sbuild();
		support_plan_bg+=vec3(0.0, support_plan_size.y, 0.0);
		add_Pbuild(Hexahedron_solid(support_plan_bg+vec3(oplan_house_dec, 0.0, oplan_house_dec)
						,support_plan_size+vec3(-oplan_house_dec*2.0, oplan_house_h, -oplan_house_dec*2.0)));
	add_Sbuild(textures[BRICKS], donjon_bricks_size_repeat/3.0f);

	add_tour(vec3(cas.tour_size.x*1.5f, 0.0, cas.tour_size.z*1.2f));
	add_tour(vec3(cas.wall_size.x-cas.tour_size.x*4.0, 0.0, cas.tour_size.z*1.5f),10.0);
	add_tour(vec3(cas.wall_size.x-cas.tour_size.x*4.0, 0.0, cas.wall_size.x-cas.tour_size.z*4.0-cas.tour_size.z*1.5f),17.0);
	add_tour(vec3(bd-vec3(donjon_size.x, 0.0, donjon_size.z) + vec3(-6.0,0.0,10.0)),18.0);
	
}	

void build_buldings()
{
	//TEXTURES
	textures.resize(N_TEXTURES);

	textures[BRICKS].load_BMP("textures/text.bmp");
	textures[FLOOR].load_BMP("textures/sol.bmp");
	textures[BORDER].load_BMP("textures/border.bmp");
	textures[MARBER].load_BMP("textures/marber.bmp");
	textures[ROOTH].load_BMP("textures/rooth.bmp");
	textures[TOURS].load_BMP("textures/tours.bmp");

	//BUILDINGS

	castle_floor=Rectangle(vec3(-floor_size/2.0,0.0,-floor_size/2.0),
	 	vec3(0.0,0.0,floor_size),
	 	vec3(floor_size,0.0,0.0));
	castle_floor.apply_texture(textures[FLOOR], floor_floor_repeat);
	
	cas.init();
	build_secondary_buldings();

}

void draw_buildings()
{
	castle_floor.draw();
	for(Hexahedron_solid& stru : cas.walls)
	{
		stru.draw();
	}
	for(Hexahedron_solid& stru : cas.woods)
	{
		stru.draw();
	}
	for(Pbuilding& pb : all_buildings)
	{
		pb.draw();
	}
}

void free_buildings()
{
	for(Image tex : textures)
	{
		tex.destroy();
	}
}

vec3 castle::total_length()
{
	float tl=(proj(wall_size,vec3(1.0,0.0,0.0))
		   +proj(tour_size,vec3(1.0,0.0,0.0))*2.0f).x;
	return vec3(tl, 0.0, tl);
}

void castle::init()
{
	float d_place_tour=proj(tour_size+wall_size, vec3(1.0,0.0,0.0)).x;

	walls.resize(0);
	woods.resize(0);

	//TOWERS


	woods.push_back(Hexahedron_solid(position, tour_size));
	woods.push_back(Hexahedron_solid(position+vec3(d_place_tour,0.0,0.0), tour_size));
	woods.push_back(Hexahedron_solid(position+vec3(0.0,0.0,d_place_tour), tour_size));
	woods.push_back(Hexahedron_solid(position+vec3(d_place_tour,0.0,d_place_tour), tour_size));

	for(int i=0; i<4;i++)
	{
		vec3 beg=woods[i].get_position()+vec3(-overtours.x/2.0,tour_size.y,-overtours.z/2.0);
		woods.push_back(Hexahedron_solid(beg, overtours+vec3(tour_size.x, 0.0, tour_size.z)));
		
		beg+=vec3((-overtours_rooths.x)/2.0,overtours.y,(-overtours_rooths.z)/2.0);
		woods.push_back(Hexahedron_solid(beg, overtours_rooths+vec3(tour_size.x+overtours.x, 0.0, tour_size.z+overtours.z)));

		beg+=vec3(0.0,overtours_rooths.y,0.0);
		woods.push_back(Hexahedron_solid(beg, tour_rooths));
		woods.push_back(Hexahedron_solid(beg+vec3(overtours_rooths.x+overtours.x+tour_size.x-tour_rooths.x,0.0,0.0), tour_rooths));
		woods.push_back(Hexahedron_solid(beg+vec3(0.0,0.0,overtours.z+tour_size.z-tour_rooths.z+overtours_rooths.z), tour_rooths));
		woods.push_back(Hexahedron_solid(beg+vec3(overtours_rooths.x+overtours.x+tour_size.x-tour_rooths.x,0.0,overtours.z+tour_size.z-tour_rooths.z+overtours_rooths.z), tour_rooths));
		

		add_light(Light(beg+vec3(tour_size.x/2.0,tour_rooths.y/2.0,tour_size.z/2.0), 150.0, light_color, 3.0, 0.6));

		beg+=vec3(0.0,tour_rooths.y,0.0);
		
		woods.push_back(Hexahedron_solid(beg, overtours_rooths+vec3(tour_size.x+overtours.x, 0.0, tour_size.z+overtours.z)));

		beg+=vec3(overtours_rooths.x/2.0,overtours_rooths.y,overtours_rooths.z/2.0);

		woods.push_back(Hexahedron_solid(beg, overtours+vec3(tour_size.x, 0.0, tour_size.z)));
	}	

	//WALLS
	vec3 nwalls=vec3(wall_size.z ,wall_size.y ,wall_size.x);
	
	walls.push_back(Hexahedron_solid(position+vec3(tour_size.x,0.0,0.0)-vec3(0.0,0.0,decal_tour_wall)
			+vec3(0.0,0.0,d_place_tour)+vec3(0.0,0.0,tour_size.x) - vec3(0.0,0.0,wall_size.z), wall_size));
	walls.push_back(Hexahedron_solid(position+vec3(0.0,0.0,tour_size.z)+vec3(decal_tour_wall,0.0,0.0), nwalls));
	walls.push_back(Hexahedron_solid(position+vec3(0.0,0.0,tour_size.x)-vec3(decal_tour_wall,0.0,0.0)
					+vec3(d_place_tour,0.0,0.0)+vec3(tour_size.x,0.0,0.0)- vec3(wall_size.z,0.0,0.0), vec3(wall_size.z,wall_size.y,wall_size.x)));


	walls.push_back(Hexahedron_solid(position+vec3(tour_size.x,0.0,0.0)+vec3(0.0,0.0,decal_tour_wall), vec3((wall_size.x-door_size.x)/2.0,wall_size.y, wall_size.z)));
	walls.push_back(Hexahedron_solid(position+vec3(tour_size.x,0.0,0.0)+vec3(0.0,0.0,decal_tour_wall)+vec3((wall_size.x+door_size.x)/2.0,0.0, 0.0), vec3((wall_size.x-door_size.x)/2.0, wall_size.y, wall_size.z)));
	walls.push_back(Hexahedron_solid(position+vec3(tour_size.x,0.0,0.0)+vec3(0.0,0.0,decal_tour_wall)+vec3((wall_size.x-door_size.x)/2.0,door_size.y, 0.0), vec3(door_size.x, wall_size.y-door_size.y, wall_size.z)));

	add_light(Light(position+vec3(tour_size.x,0.0,0.0)+vec3(0.0,0.0,decal_tour_wall)+vec3((wall_size.x)/2.0,door_size.y, 0.0),
		100.0, light_color, 3.0, 0.4));

	for(Hexahedron_solid& stru : walls)
	{
		stru.apply_texture(textures[MARBER], donjon_bricks_size_repeat);
	}
	for(uint i=0;i<woods.size() ;i++ )
	{
		if(i<=3)
			woods[i].apply_texture(textures[BRICKS], bricks_size_repeat/2.4f);
		else
			woods[i].apply_texture(textures[MARBER], bricks_size_repeat);

	}
}