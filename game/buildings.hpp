#ifndef _BUILDINGS_HPP_
#define _BUILDINGS_HPP_

#include "../sgl/sgl.hpp"

struct castle
{
	vector<Hexahedron_solid> walls;
	vector<Hexahedron_solid> rooths;
	vector<Hexahedron_solid> woods;

	void init();
	vec3 total_length();

	vec3 position=vec3(-10,0.0,-10.0);
	vec3 wall_size=vec3(50.0,5.0,0.5);
	vec3 tour_size=vec3(2.0,10.0,2.0);
	float decal_tour_wall=0.3;
	vec3 door_size=vec3(5.0,3.0,0.3);
	vec3 overtours=vec3(0.5,0.5,0.5);
	vec3 overtours_rooths=vec3(0.5,0.4,0.5);
	vec3 tour_rooths=vec3(0.3,1.5,0.3);
};

struct Pbuilding
{
	vector<Hexahedron_solid> pavers;
	void draw()
	{
		for(Hexahedron_solid& p : pavers)
		{
			p.draw();
		}
	}
	void colorp(const vec3& c)
	{
		for(Hexahedron_solid& p : pavers)
		{
			p.apply_color(c);
		}
	}
	void colorp(Image i, vec2 r=vec2(0.0,0.0))
	{
		for(Hexahedron_solid& p : pavers)
		{
			p.apply_texture(i,r);
		}
	}
};

enum TEXTURES_NAMES {BRICKS=0, FLOOR,
					 BORDER,
					 MARBER,
					 ROOTH,
					 TOURS,

					N_TEXTURES};

void build_buldings();
void draw_buildings();
void free_buildings();

inline vec3 proj_floor(const vec3& v){return vec3(v.x,0.0,v.z);}; 

#endif //_BUILDINGS_HPP_
