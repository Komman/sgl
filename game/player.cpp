#include "player.hpp"

static vec3 relative_player_view = vec3(-3.0, 5.0, 0.0);

Player::Player(const vec3& start_position)
: Narrator(start_position,
relative_player_view)
{
	Model m("models/epee");
	Shape_model sm(m, vec3(0.0,3.0,0.0));
	sm.scale(0.2);

	{
		Spheric_solid s(vec3(0.0,0.0,0.0), 3.0, BLUE);
		Hexahedron_solid h(vec3(-5.0,3.0,-1.0), vec3(2.0,1.0,3.0), dark(RED));

		this->add(h);
		this->add(s);
		this->add(sm);
	}
}

