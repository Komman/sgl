CC=g++
WFLAGS=-Wall -g
LDFLAGS= -lglut -lGLU -lGL -lGLEW -lglfw3 -lX11 -lXxf86vm -lXrandr -lpthread -lXi -ldl -lXinerama -lXcursor 
EXE=run

all: code

sgl/window.hpp:  sgl/err.hpp sgl/default.hpp

sgl/utils.hpp: 

sgl/utils_debug.hpp: 

sgl/unid.hpp:  sgl/err.hpp

sgl/tests/includes/tests_unid.hpp: 

sgl/tests/includes/tests_object.hpp: 

sgl/tests/includes/tests_hitboxs.hpp: 

sgl/tests/includes/tests_group.hpp: 

sgl/tests/includes/tests_collisions.hpp: 

sgl/solid_specific.hpp:  sgl/solid.hpp

sgl/solid.hpp:  sgl/shape.hpp sgl/hitboxs.hpp sgl/err.hpp sgl/orientation.hpp

sgl/shape_specific.hpp: 

sgl/shape.hpp:  sgl/err.hpp sgl/image.hpp sgl/colors.hpp sgl/models.hpp sgl/lights.hpp sgl/utils.hpp sgl/orientation.hpp sgl/shape_specific.hpp

sgl/sgl.hpp:  sgl/window.hpp sgl/err.hpp sgl/display.hpp sgl/camera.hpp sgl/keyboard_and_mouse.hpp sgl/shape.hpp sgl/colors.hpp sgl/image.hpp sgl/lights.hpp sgl/hitboxs.hpp sgl/solid_specific.hpp sgl/models.hpp sgl/instant_draw.hpp sgl/object.hpp sgl/narrator.hpp

sgl/reactive_function.hpp: 

sgl/print_hitbox_algo.hpp: 

sgl/orientation.hpp:  sgl/utils.hpp

sgl/object.hpp:  sgl/shape.hpp sgl/hitboxs.hpp sgl/solid.hpp sgl/lights.hpp sgl/orientation.hpp sgl/hitbox_group.hpp sgl/unid.hpp sgl/object_dev.hpp

sgl/object_dev.hpp: 

sgl/narrator.hpp:  sgl/object.hpp sgl/camera.hpp

sgl/models.hpp:  sgl/err.hpp

sgl/main.hpp: 

sgl/lights.hpp:  sgl/colors.hpp sgl/utils.hpp

sgl/keyboard_and_mouse.hpp:  sgl/window.hpp

sgl/instant_draw.hpp:  sgl/shape.hpp

sgl/image.hpp:  sgl/err.hpp

sgl/hitboxs_specific.hpp: 

sgl/hitboxs.hpp:  sgl/err.hpp sgl/collisions.hpp sgl/instant_draw.hpp sgl/hitboxs_specific.hpp

sgl/hitbox_group.hpp:  sgl/hitboxs.hpp

sgl/err.hpp: 

sgl/display.hpp:  sgl/err.hpp sgl/default.hpp sgl/image.hpp sgl/lights.hpp

sgl/default.hpp: 

sgl/colors.hpp: 

sgl/collisions.hpp:  sgl/err.hpp sgl/reactive_function.hpp

sgl/collision_algorithm_axis_data.hpp:  sgl/hitboxs.hpp sgl/collisions.hpp sgl/utils_debug.hpp

sgl/camera.hpp:  sgl/display.hpp sgl/window.hpp sgl/keyboard_and_mouse.hpp sgl/hitboxs.hpp

game/player.hpp: 

game/game.hpp:  game/buildings.hpp game/player.hpp

game/buildings.hpp: 

obj/sgl_window.o: sgl/window.cpp  sgl/window.hpp sgl/err.hpp sgl/keyboard_and_mouse.hpp sgl/window.hpp
	${CC} ${WFLAGS} -c sgl/window.cpp -o obj/sgl_window.o

obj/sgl_solid_specific.o: sgl/solid_specific.cpp  sgl/solid_specific.hpp sgl/solid.hpp sgl/utils.hpp 
	${CC} ${WFLAGS} -c sgl/solid_specific.cpp -o obj/sgl_solid_specific.o

obj/sgl_solid.o: sgl/solid.cpp  sgl/solid.hpp sgl/shape.hpp sgl/display.hpp sgl/err.hpp sgl/utils.hpp 
	${CC} ${WFLAGS} -c sgl/solid.cpp -o obj/sgl_solid.o

obj/sgl_shape_specific.o: sgl/shape_specific.cpp  sgl/shape.hpp sgl/err.hpp sgl/utils.hpp  sgl/colors.hpp 
	${CC} ${WFLAGS} -c sgl/shape_specific.cpp -o obj/sgl_shape_specific.o

obj/sgl_shape_implem.o: sgl/shape_implem.cpp  sgl/shape.hpp sgl/err.hpp sgl/utils.hpp  sgl/display.hpp sgl/err.hpp
	${CC} ${WFLAGS} -c sgl/shape_implem.cpp -o obj/sgl_shape_implem.o

obj/sgl_orientation.o: sgl/orientation.cpp  sgl/orientation.hpp sgl/utils.hpp sgl/err.hpp 
	${CC} ${WFLAGS} -c sgl/orientation.cpp -o obj/sgl_orientation.o

obj/sgl_object.o: sgl/object.cpp  sgl/object.hpp sgl/shape.hpp
	${CC} ${WFLAGS} -c sgl/object.cpp -o obj/sgl_object.o

obj/sgl_narrator.o: sgl/narrator.cpp  sgl/narrator.hpp sgl/object.hpp
	${CC} ${WFLAGS} -c sgl/narrator.cpp -o obj/sgl_narrator.o

obj/sgl_models.o: sgl/models.cpp  sgl/models.hpp sgl/err.hpp
	${CC} ${WFLAGS} -c sgl/models.cpp -o obj/sgl_models.o

obj/sgl_lights.o: sgl/lights.cpp  sgl/utils.hpp  sgl/lights.hpp sgl/colors.hpp sgl/err.hpp  sgl/display.hpp sgl/err.hpp sgl/colors.hpp 
	${CC} ${WFLAGS} -c sgl/lights.cpp -o obj/sgl_lights.o

obj/sgl_instant_draw.o: sgl/instant_draw.cpp  sgl/instant_draw.hpp sgl/shape.hpp sgl/colors.hpp 
	${CC} ${WFLAGS} -c sgl/instant_draw.cpp -o obj/sgl_instant_draw.o

obj/sgl_hitboxs_specific.o: sgl/hitboxs_specific.cpp  sgl/collisions.hpp sgl/err.hpp sgl/hitboxs.hpp sgl/err.hpp sgl/utils.hpp  sgl/orientation.hpp sgl/utils.hpp
	${CC} ${WFLAGS} -c sgl/hitboxs_specific.cpp -o obj/sgl_hitboxs_specific.o

obj/sgl_hitboxs.o: sgl/hitboxs.cpp  sgl/hitboxs.hpp sgl/err.hpp sgl/orientation.hpp sgl/utils.hpp
	${CC} ${WFLAGS} -c sgl/hitboxs.cpp -o obj/sgl_hitboxs.o

obj/sgl_display.o: sgl/display.cpp  sgl/display.hpp sgl/err.hpp sgl/lights.hpp sgl/colors.hpp
	${CC} ${WFLAGS} -c sgl/display.cpp -o obj/sgl_display.o

obj/sgl_collisions.o: sgl/collisions.cpp  sgl/collisions.hpp sgl/err.hpp sgl/hitboxs.hpp sgl/err.hpp
	${CC} ${WFLAGS} -c sgl/collisions.cpp -o obj/sgl_collisions.o

obj/sgl_collision_algorithm.o: sgl/collision_algorithm.cpp  sgl/hitboxs.hpp sgl/err.hpp sgl/collisions.hpp sgl/err.hpp sgl/collision_algorithm_axis_data.hpp sgl/hitboxs.hpp sgl/utils_debug.hpp  sgl/keyboard_and_mouse.hpp sgl/window.hpp sgl/print_hitbox_algo.hpp 
	${CC} ${WFLAGS} -c sgl/collision_algorithm.cpp -o obj/sgl_collision_algorithm.o

obj/sgl_camera.o: sgl/camera.cpp  sgl/camera.hpp sgl/display.hpp
	${CC} ${WFLAGS} -c sgl/camera.cpp -o obj/sgl_camera.o

obj/main.o: main.cpp  sgl/sgl.hpp sgl/window.hpp game/game.hpp game/buildings.hpp
	${CC} ${WFLAGS} -c main.cpp -o obj/main.o

obj/game_player.o: game/player.cpp  game/player.hpp 
	${CC} ${WFLAGS} -c game/player.cpp -o obj/game_player.o

obj/game_buildings.o: game/buildings.cpp  game/buildings.hpp 
	${CC} ${WFLAGS} -c game/buildings.cpp -o obj/game_buildings.o

code: obj  obj/sgl_window.o obj/sgl_solid_specific.o obj/sgl_solid.o obj/sgl_shape_specific.o obj/sgl_shape_implem.o obj/sgl_orientation.o obj/sgl_object.o obj/sgl_narrator.o obj/sgl_models.o obj/sgl_lights.o obj/sgl_instant_draw.o obj/sgl_hitboxs_specific.o obj/sgl_hitboxs.o obj/sgl_display.o obj/sgl_collisions.o obj/sgl_collision_algorithm.o obj/sgl_camera.o obj/main.o obj/game_player.o obj/game_buildings.o
	${CC} ${WFLAGS}   obj/sgl_window.o obj/sgl_solid_specific.o obj/sgl_solid.o obj/sgl_shape_specific.o obj/sgl_shape_implem.o obj/sgl_orientation.o obj/sgl_object.o obj/sgl_narrator.o obj/sgl_models.o obj/sgl_lights.o obj/sgl_instant_draw.o obj/sgl_hitboxs_specific.o obj/sgl_hitboxs.o obj/sgl_display.o obj/sgl_collisions.o obj/sgl_collision_algorithm.o obj/sgl_camera.o obj/main.o obj/game_player.o obj/game_buildings.o -o ${EXE} ${LDFLAGS}

obj:
	if [ ! -d obj ]; then mkdir obj;fi

run: code
	./${EXE}

clean:
	rm -rf obj
	rm -rf ${EXE}

tests: code
	cd ./sgl/tests; make run -s

