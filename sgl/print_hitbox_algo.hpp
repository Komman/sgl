
#ifndef _PRINT_HITBOX_AGLO_HPP_
#define _PRINT_HITBOX_AGLO_HPP_

#define COL << "\033[1;32m" 
#define COLA << "\033[1;34m" 
#define ECOL <<"\033[0m"

void print_hitbox_algo(int axismax)
{
	//return;
	vector<int> gbinding=binding();
	vector<collision_data> ghitboxs_infos=hitboxs_infos();

	static int print_number=0;
	cout<<"======================"<<endl;
	cout<<"=== HITBOX ALGO: "<<print_number<<" ==="<<endl;
	cout<<"======================"<<endl<<endl;;

	cout COL <<"bindings: { " ECOL;
	print_vector(gbinding);
	cout<<"}"<<endl;

	cout COL<<"hitboxs_infos : {" ECOL <<endl;;
	for(collision_data c : ghitboxs_infos)
	{
		cout<<"	 {";
		cout<<"{ ";
		for(unsigned int i=0;i< c.float_infos.size();i++)
		{
			print_vec3(c.float_infos[i]);
			if(i!=c.float_infos.size()-1)
			{
				cout<<", ";
			}
			else
			{
				cout<<" ";
			}
		}
		cout<<"}, t:";
		cout<<c.type;
		cout<<", rf:";
		cout<<c.reactive_function;
		cout<<" },"<<endl;
	}
	cout<<"}"<<endl;

	for(unsigned int i=0;i<(unsigned int)axismax;i++)
	{
		axis_data ax = axis[i];
		cout COLA <<" === "<<axis_string[i]<<" ===" ECOL <<  endl;


		cout COL <<"minsmaxs: {" ECOL;
		for(unsigned int j=0;j<ax.minsmaxs.size();j++)
		{
			cout<<"("<<ax.minsmaxs[j].value<<", "<<minmax_string[ax.minsmaxs[j].t]<<", id:"<<ax.minsmaxs[j].id()<<")";

			if(j!=ax.minsmaxs.size()-1)
			{
				cout<<", ";
			}
		}
		cout<<"}"<<endl;


		cout COL <<"sort_binding: { " ECOL;
		print_vector(ax.sort_binding);
		cout<<"}"<<endl;

		cout COL <<"sort_binding_indice: { " ECOL;
		for(unsigned int j=0;j<axis[i].minsmaxs.size();j++)
		{
			cout<<axis[i].minsmaxs[j].sort_binding_indice<<", ";
		}
		cout<<"}"<<endl;

		

		cout COL<<"areas: {" ECOL;
		for(unsigned int j=0; j<ax.areas.size();j++)
		{
			cout<<"[";
			print_vector(ax.areas[j]);
			cout<<"]";

			if(j!=ax.areas.size()-1)
			{
				cout<<", ";
			}
		}
		cout<<"}"<<endl;

		cout COL <<"hitboxs_to_areas: {" ECOL <<endl;
		for(unsigned int j=0;j<ax.hitboxs_to_areas.size();j++)
		{
			cout<<"	 {";
			print_vector(ax.hitboxs_to_areas[j]);
			cout<<"}";

			if(j!=ax.hitboxs_to_areas.size()-1)
			{
				cout<<", ";
			}
			cout<<endl;
		}
		cout<<"}"<<endl;

	}

	cout COL <<"current_collisions: {" ECOL <<endl;
	for(unsigned int j=0;j<current_collisions.size();j++)
	{
		cout<<"	 {";
		print_vector(current_collisions[j].ids.inid);
		cout<<"}";

		if(j!=current_collisions.size()-1)
		{
			cout<<", ";
		}
		cout<<endl;
	}
	cout<<"}"<<endl;


	cout<<endl<<endl;
	print_number++;
}

#endif //_PRINT_HITBOX_AGLO_HPP_
