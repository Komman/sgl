#include "window.hpp"
#include "keyboard_and_mouse.hpp"


using namespace glm;
using namespace std;
using namespace sgl;

namespace sgl
{
	static GLFWwindow* window;
	static int escape_key=SGL_ESCAPE_KEY;
	static uint_xy win_size;

	static int fps=0;
    static double t_fps;
    static float dt=0.0;
    static float time_old;
    static int mouse_in_game=1;
    static bool has_to_quit = false; 
    static bool fps_loop=false;

    static int KEYS_QWERTY_TO_AZERTY[GLFW_KEY_LAST];

    static inline void switch_keys(int key1, int key2)
	{
		KEYS_QWERTY_TO_AZERTY[key1]=key2;
		KEYS_QWERTY_TO_AZERTY[key2]=key1;
	}

	static void adapt_keyboard_to_azerty()
	{
		for(int i=0;i<GLFW_KEY_LAST;i++)
		{
			KEYS_QWERTY_TO_AZERTY[i]=i;
		}
		//TOFAIREQUELQUECHOSE POUR CET INCAPABLE DE GLFW ET AZERTY
		switch_keys('A','Q');
		switch_keys('W','Z');
		KEYS_QWERTY_TO_AZERTY[(unsigned int)'M']=';';
		KEYS_QWERTY_TO_AZERTY[(unsigned int)',']='M';
	}

	static inline int gkey(int key)
	{
		return KEYS_QWERTY_TO_AZERTY[key];
	}

	void init_window(uint size_x, uint size_y, const char* name)
	{
		if( !glfwInit() ) 
		    err("Failed to initialize GLFW");  

		glfwWindowHint(GLFW_SAMPLES, 4); // antialiasing 4x 
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // OpenGL 3.3 
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); 
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // Nous ne voulons pas de support de l'ancien OpenGL

			// Ouvre une fenêtre et crée son contexte OpenGL
		window = glfwCreateWindow(size_x, size_y, name, NULL, NULL); 
		if( window == NULL ){ 
			glfwTerminate();
			cout<<"non"<<endl;
		    err("Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials."); 
		}
		win_size={size_x,size_y}; 

		glfwMakeContextCurrent(window); 
		 
		// Initialise GLEW 
		glewExperimental=true; // Nécessaire pour le profil core
		if (glewInit() != GLEW_OK)
		    err("Failed to initialize GLEW"); 

		// S'assure que l'on puisse capturer la touche échap utilisée plus bas
		glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

		t_fps=glfwGetTime();
		time_old=glfwGetTime();

		adapt_keyboard_to_azerty();

		if(SGL_ENABLE_CULLING)
		{
			glEnable(GL_CULL_FACE);
		}

		#if SGL_ENABLE_BLEND==true
			glEnable(GL_BLEND); 
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		#else

		#endif
	}

	void get_events()
	{
		glfwPollEvents();

	    dt=glfwGetTime()-time_old;
	    time_old=glfwGetTime();
	    fps_loop=false;

        if(glfwGetTime()-t_fps>1.0)
        {
            t_fps=glfwGetTime();
            fps_loop=true;

            #if (SGL_PRINT_FPS == 1)
            	printf("FPS: %d\n", fps);
            #endif

            fps=0;
        }

		fps++; 

		if(key_pressed(SGL_PAUSE_KEY))
		{
			mouse_quit_game();
		}
	}

	float delta_time()
	{
		return dt;
	}


	void flip()
	{
		glfwSwapBuffers(window);
	}

	void exit_window()
	{
		has_to_quit=true;
	}

	bool quit()
	{
		return (has_to_quit || (glfwGetKey(window, escape_key ) == GLFW_PRESS) || glfwWindowShouldClose(window));
	}

	void close_window()
	{
		glfwDestroyWindow(window); 	
		glfwTerminate();
	}

	uint_xy window_size()
	{
		return win_size;
	}
	int is_mouse_in_game()
	{
		return mouse_in_game;
	}
	void mouse_quit_game()
	{
		mouse_in_game=0;
		set_mouse_visibility(1);
	}
	void mouse_enter_game()
	{
		mouse_in_game=1;
		set_mouse_visibility(0);
	}
	bool is_fps_loop()
	{
		return fps_loop;
	}



	//KEYBOARD AND MOUSE
	vec2 mouse_position()
	{
		double xm;
		double ym;
		glfwGetCursorPos(window, &xm, &ym);
		return vec2(xm/(float)(win_size.x),ym/(float)(win_size.y));
	};
	void set_mouse_position(const vec2& pos)
	{
		glfwSetCursorPos(window, pos.x*(double)(win_size.x), pos.y*(double)(win_size.y));
	};

	bool key_pressed(int key)
	{
		return glfwGetKey(window, gkey(key)) == GLFW_PRESS;
	}

	void set_mouse_visibility(bool visible)
	{
		if(visible)
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		else
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	}

};

//date: ??:
/*
==13741== Memcheck, a memory error detector
==13741== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==13741== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==13741== Command: ./run
==13741== 
==13741== 
==13741== HEAP SUMMARY:
==13741==     in use at exit: 271,947 bytes in 3,630 blocks
==13741==   total heap usage: 64,045 allocs, 60,415 frees, 17,798,966 bytes allocated
==13741== 
==13741== LEAK SUMMARY:
==13741==    definitely lost: 844 bytes in 1 blocks
==13741==    indirectly lost: 0 bytes in 0 blocks
==13741==      possibly lost: 0 bytes in 0 blocks
==13741==    still reachable: 271,103 bytes in 3,629 blocks
==13741==         suppressed: 0 bytes in 0 blocks
==13741== Rerun with --leak-check=full to see details of leaked memory
==13741== 
==13741== For lists of detected and suppressed errors, rerun with: -s
==13741== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 2 from 2)
*/

//date: 05/02/2020:
/*

==62287== HEAP SUMMARY:
==62287==     in use at exit: 274,347 bytes in 3,633 blocks
==62287==   total heap usage: 102,823 allocs, 99,190 frees, 35,208,141 bytes allocated
==62287== 
==62287== LEAK SUMMARY:
==62287==    definitely lost: 3,244 bytes in 4 blocks
==62287==    indirectly lost: 0 bytes in 0 blocks
==62287==      possibly lost: 0 bytes in 0 blocks
==62287==    still reachable: 271,103 bytes in 3,629 blocks
==62287==         suppressed: 0 bytes in 0 blocks
==62287== Rerun with --leak-check=full to see details of leaked memory
==62287== 
==62287== For lists of detected and suppressed errors, rerun with: -s
==62287== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 2 from 2)
*/

//date: 24/02/2020

/*
==10777== HEAP SUMMARY:
==10777==     in use at exit: 278,260 bytes in 3,633 blocks
==10777==   total heap usage: 81,232 allocs, 77,599 frees, 24,089,651 bytes allocated
==10777== 
==10777== LEAK SUMMARY:
==10777==    definitely lost: 7,157 bytes in 4 blocks
==10777==    indirectly lost: 0 bytes in 0 blocks
==10777==      possibly lost: 0 bytes in 0 blocks
==10777==    still reachable: 271,103 bytes in 3,629 blocks
==10777==         suppressed: 0 bytes in 0 blocks
==10777== Rerun with --leak-check=full to see details of leaked memory
==10777== 
==10777== For lists of detected and suppressed errors, rerun with: -s
==10777== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 2 from 2)
*/