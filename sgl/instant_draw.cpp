#include "instant_draw.hpp"
#include "colors.hpp"
#include <vector>

using namespace std;
using namespace sgl;
using namespace glm;

#define N_PAVER_BORDER 12

static vector<Paver> border = vector<Paver>(N_PAVER_BORDER);
static vec3 border_color=RED+vec3(-0.2,0.2,0.2);
static bool border_init=false;

namespace sgl
{
	void draw_border(const vec3& p1, const vec3& p2, float thickness)
	{
		const vec3 pmin=glm::min(p1,p2);
		const vec3 pmax=glm::max(p1,p2);
		const float& lx=(pmax-pmin).x;
		const float& ly=(pmax-pmin).y;
		const float& lz=(pmax-pmin).z;
		const float& d=thickness;

		if(!border_init)
		{
			for(Paver& p : border)
			{
				p=Paver(vec3(0.0,0.0,0.0), vec3(1.0,1.0,1.0));
			}
			border_init=true;
		}

		border[0].transform(pmin,vec3(d, d, lz), border_color);
		border[1].transform(pmin+vec3(lx-d,0,0), vec3(d, d, lz), border_color);
		border[2].transform(pmin+vec3(d,0,0), vec3(lx-2.0*d, d, d), border_color);
		border[3].transform(pmin+vec3(d,0,lz-d), vec3(lx-2.0*d, d, d), border_color);
		border[4].transform(pmin+vec3(0,d,0), vec3(d, ly-2.0*d, d), border_color);
		border[5].transform(pmin+vec3(lx-d,d,0), vec3(d, ly-2.0*d, d), border_color);
		border[6].transform(pmin+vec3(lx-d,d,lz-d), vec3(d, ly-2.0*d, d), border_color);
		border[7].transform(pmin+vec3(0,d,lz-d), vec3(d, ly-2.0*d, d), border_color);
		border[8].transform(pmin+vec3(0,ly-d,0), vec3(d, d, lz), border_color);
		border[9].transform(pmin+vec3(lx-d,ly-d,0), vec3(d, d, lz), border_color);
		border[10].transform(pmin+vec3(d,ly-d,0), vec3(lx-2.0*d, d, d), border_color);
		border[11].transform(pmin+vec3(d,ly-d,lz-d), vec3(lx-2.0*d, d, d), border_color);

		for(Paver& p : border)
		{
			p.draw();
		}
	}
};