#version 330 core

in vec3 fragmentColor;
in vec2 uv;
in vec3 position;
//out vec4 color4; 
out vec3 color; 
in vec3 normal;

#define DRAW_TYPE_COLOR_ONLY 0
#define DRAW_TYPE_TEXTURE 1
#define MAX_LIGHTS 100

uniform sampler2D texture2D;

uniform int draw_type;

#define SIZE_LIGHT_INFO 4

uniform vec3 lights[SIZE_LIGHT_INFO*MAX_LIGHTS];
uniform int lights_types[MAX_LIGHTS];
uniform int light_count;

uniform vec3 camera_position[];

void main(){ 
    //vec3 color;
    if(draw_type==DRAW_TYPE_COLOR_ONLY)
    {
        color = fragmentColor; 
    }
    if(draw_type==DRAW_TYPE_TEXTURE)
    {
        color=texture(texture2D,uv).rgb*fragmentColor;
    }
    //TOOPT: pus the maximum of calculs in vertex shader
    

    float max_light=99999.0;
    float dec_length_by_intensity=0.03;
    float max_distance_by_intensity=0.17;

    vec3 min_level=vec3(0.2,0.2,0.2);

    vec3 lights_sum=vec3(0.0,0.0,0.0);
    vec3 additive_light=vec3(0.0,0.0,0.0);
    vec3 light_i;
    float inv_d;
    float overlight;
    float dec_length;
    float max_distance;

    for(int i=0; i<light_count; i++)
    {
        dec_length=dec_length_by_intensity*lights[SIZE_LIGHT_INFO*i].x;
        max_distance=max_distance_by_intensity*lights[SIZE_LIGHT_INFO*i].x;
        inv_d=distance(lights[SIZE_LIGHT_INFO*i+2],position);
        if(inv_d<max_distance+dec_length)
        {   
            if(inv_d>max_distance)
            {
                inv_d=(1.0/(max_distance*max_distance))*(max_distance-inv_d+dec_length)/dec_length;
            }
            else
            {
                inv_d=1.0/(inv_d*inv_d); 
            }
              
            light_i=clamp(lights[SIZE_LIGHT_INFO*i+1]*(lights[SIZE_LIGHT_INFO*i].x)*inv_d*(lights[SIZE_LIGHT_INFO*i+3].x+(1.0-lights[SIZE_LIGHT_INFO*i+3].x)*dot(normal, normalize(lights[SIZE_LIGHT_INFO*i+2]-position))),0.0,max_light);
            overlight=max(light_i.x, max(light_i.y, light_i.z));
            if(overlight>lights[SIZE_LIGHT_INFO*i].z)
            {
                light_i=light_i/overlight*lights[SIZE_LIGHT_INFO*i].z;
            }

            additive_light+=light_i*lights[SIZE_LIGHT_INFO*i].y;
            
            

            lights_sum+=light_i;
        }
    }
    color*=(min_level+lights_sum);
    color+=additive_light;  

   //color4=vec4(color, 0.5);
}

/*
        inv_d=distance(lights[SIZE_LIGHT_INFO*i+2],position);
        inv_d=1.0/(inv_d*inv_d); 

        light_i=clamp(lights[SIZE_LIGHT_INFO*i+1]*(lights[SIZE_LIGHT_INFO*i].x)*inv_d*(lights[SIZE_LIGHT_INFO*i+3].x+(1.0-lights[SIZE_LIGHT_INFO*i+3].x)*dot(normal, normalize(lights[SIZE_LIGHT_INFO*i+2]-position))),0.0,max_light);
        overlight=max(light_i.x, max(light_i.y, light_i.z));
        if(overlight>lights[SIZE_LIGHT_INFO*i].z)
        {
            light_i=light_i/overlight*lights[SIZE_LIGHT_INFO*i].z;
        }  

        additive_light+=light_i*lights[SIZE_LIGHT_INFO*i].y;
        lights_sum+=light_i;
*/