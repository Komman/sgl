#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace; 
layout(location = 1) in vec3 vertexColor;

out gl_PerVertex {
    vec4 gl_Position;
};
out vec3 fragmentColor;
out vec3 position;
out vec2 uv;
out vec3 normal;


#define DRAW_TYPE_COLOR_ONLY 0
#define DRAW_TYPE_TEXTURE 1
uniform int draw_type;
uniform vec3 plan[];
/* uniform vec3 plan_initial_point;
uniform vec3 plan_direction1;
uniform vec3 plan_direction2; */


uniform mat4 MVP; 
 
void main(){ 
 
    // Obtient la position du sommet, dans l'espace de découpe : MVP * position
    position=vertexPosition_modelspace;
    vec4 v = vec4(vertexPosition_modelspace,1); // Transforme un vecteur 4D homogène
    gl_Position = MVP * v; 
    fragmentColor = vertexColor;

    normal= normalize(cross(plan[1], plan[2]));

    if(draw_type==DRAW_TYPE_TEXTURE)
    {
        vec3 plan_initial_point =plan[0];
        vec3 plan_direction1    =plan[1];
        vec3 plan_direction2    =plan[2];
            
        uv.x=dot(normalize(plan_direction1), (vertexPosition_modelspace-plan_initial_point))/length(plan_direction1);
        uv.y=dot(normalize(plan_direction2), (vertexPosition_modelspace-plan_initial_point))/length(plan_direction2);
    }
}   