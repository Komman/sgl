#ifndef _HITBOX_GROUP_HPP_
#define _HITBOX_GROUP_HPP_

#include "hitboxs.hpp"

namespace sgl
{
	class Group
	{
	public:
		Group(){}

		inline void add(const int id)
		{
			this->update_ids_size();

			#ifdef SGL_CHECK_ERRORS
			if(id >= (int)ids.size())
				err("Group::add: hitbox do not exist or this->ids not well sized (" + to_string(id) + "/" + to_string(ids.size()) + ")");
			#endif

			ids[id]=true;
		}

		inline void del(const int id)
		{
			this->update_ids_size();

			#ifdef SGL_CHECK_ERRORS
			if(id >= (int)ids.size())
				err("Group::del: hitbox do not exist or this->ids not well sized (" + to_string(id) + "/" + to_string(ids.size()) + ")");
			#endif

			ids[id]=false;
		}

		inline const bool contains(const int id) 
		{
			update_ids_size();

			#ifdef SGL_CHECK_ERRORS
			if(id >= (int)ids.size())
				err("Group::contains: hitbox do not exist or this->ids not well sized (" + to_string(id) + "/" + to_string(ids.size()) + ")");
			#endif

			return ids[id];
		}

		inline void reset()
		{
			ids = vector<bool>(maximum_hitbox_numer(), false);
		}

		inline void destroy()
		{
			ids.clear();
		}

	protected:
		inline void update_ids_size()
		{
			const unsigned int m=maximum_hitbox_numer();

			while(ids.size() < m)
			{
				ids.push_back(false);
			}
			while(ids.size() > m)
			{
				ids.pop_back();
			}
		}

		vector<bool> ids = vector<bool>(0);
	};
};

#endif //_HITBOX_GROUP_HPP_
