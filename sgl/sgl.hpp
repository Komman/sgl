#ifndef _SGL_HPP_
#define _SGL_HPP_

#include "window.hpp"
#include "err.hpp"
#include "display.hpp"
#include "camera.hpp"
#include "keyboard_and_mouse.hpp"
#include "shape.hpp"
#include "colors.hpp"
#include "image.hpp"
#include "lights.hpp"
#include "hitboxs.hpp"
#include "solid_specific.hpp"
#include "models.hpp"
#include "instant_draw.hpp"
#include "object.hpp"
#include "narrator.hpp"

using namespace sgl;

namespace sgl
{
	inline void init_sgl(uint size_x, uint size_y, const char* name=DF_WINDOW_NAME)
	{
		init_window(size_x, size_y, name);
		init_display();
	}
	inline void close_sgl()
	{
		close_window();
	}
}

#endif //_SGL_HPP_
