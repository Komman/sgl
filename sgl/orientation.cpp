#include "orientation.hpp"
#include <glm/gtx/vector_angle.hpp>
#include "err.hpp"

using namespace std;
using namespace glm;
using namespace sgl;

namespace sgl
{
	bool is_normalized(const vec3& v1)
	{
		return (eq_0(glm::length(v1)-1.0000));
	}
	bool is_orthogonal(const vec3& v1, const vec3& v2)
	{
		return (eq_0(glm::dot(v1, v2)));
	}
	void Orientation::check_normalized()
	{
		if(!is_normalized(up))
		{
			err("Orientation::up is not normalized");
		}
		if(!is_normalized(front))
		{
			err("Orientation::front is not normalized");
		}
	}
	void Orientation::check_ortho()
	{
		if(!is_orthogonal(up, front))
		{
			err("Orientation::up and Orientation::front are not orthogonal");
		}
	}
	void Orientation::set_rotation(const vec3& up_vector, const vec3& front_vector)
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_normalized(up_vector))
		{
			err("up_vector is not normalized");
		}
		if(!is_normalized(front_vector))
		{
			err("front_vector is not normalized");
		}
		if(!is_orthogonal(up_vector, front_vector))
		{
			err("up_vector and front_vector most be orthogonal");
		}
		this->check_normalized();
		this->check_ortho();
		#endif

		float up_angle=glm::angle(this->up, up_vector);
		if(!eq_0(up_angle))
		{
			this->rotate(up_angle, glm::cross(up, up_vector), this->get_position());
		}
		
		float front_angle=glm::angle(this->front, front_vector);
		if(!eq_0(front_angle))
		{
			this->rotate(front_angle, glm::cross(front, front_vector), this->get_position());
		}
	}
	void Orientation::rotate(float angle, const vec3& axis, const vec3& point)
	{
		this->compute_rotate(angle, axis, point);

		up=glm::normalize(glm::rotate(up, angle, axis));
		front=glm::normalize(glm::rotate(front, angle, axis));
	}
};