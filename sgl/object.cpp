#include "object.hpp"

using namespace glm;
using namespace std;
using namespace sgl;
using namespace sgl::dev;

#define DESTROY_OBJ_ATTR_VECTOR(type, vect)\
	for(obj_attr<type>& att : vect)        \
	{\
		att.destroy();\
	}

#define FOR_OBJ_VECT(type, vect) for(obj_attr<type>& att : vect)

namespace sgl
{
	Object::Object()
	{
		SGL_OBJECT_ATT_METHOD(resize, 0)
		up    = VEC3_UP;
		front = VEC3_FRONT;
	}
	Object::Object(const vec3& new_position) : Object()
	{
		position=new_position;
	}

	//======= ADD ========

	#define RETURN_UNID_ADD_METHODS(vector_name)\
		vector_name[vector_name.size()-1].vector_indice=(int)(vector_name.size()-1);\
		return vector_name[vector_name.size()-1].vector_indice;

	unid Object::add(const Solid& s)
	{
		if(!s.is_init())
			err("try to add a not-initialized solid");

		this->add_obj_attr(solids, s);
		Solid& ns=*solids[solids.size()-1].attr;
		ns.move(this->position);

		in_hbx.add(ns.hitbox_id_indice());

		RETURN_UNID_ADD_METHODS(solids)
	}

	unid Object::add(const Shape& s)
	{
		this->add_obj_attr(shapes, s);
		Shape& ns=*shapes[shapes.size()-1].attr;
		ns.move(this->position);

		RETURN_UNID_ADD_METHODS(shapes)
	}

	unid Object::add(const Object& m)
	{
		this->add_obj_attr(objects, m);
		Object& no=*objects[objects.size()-1].attr;
		no.move(this->position);

		FOR_OBJ_VECT(Solid, solids)
		{
			in_hbx.add(att.attr->hitbox_id_indice());
		}
		FOR_OBJ_VECT(Hitbox, hitboxs)
		{
			in_hbx.add(att.attr->get_id_indice());
		}


		RETURN_UNID_ADD_METHODS(objects)
	}

	unid Object::add(const Light& l)
	{
		this->add_obj_attr(lights, l);
		if(!l.is_set())
		{
			set_light(*(lights[lights.size()-1].attr));
		}
		Light& nl=*lights[lights.size()-1].attr;
		nl.move(this->position);

		RETURN_UNID_ADD_METHODS(lights)
	}

	unid Object::add(const Hitbox& h)
	{
		if(!h.is_created())
			err("try to add a not-created hitbox");

		this->add_obj_attr(hitboxs, h);
		Hitbox& nh=*hitboxs[hitboxs.size()-1].attr;
		nh.move(this->position);

		in_hbx.add(nh.get_id_indice());

		RETURN_UNID_ADD_METHODS(hitboxs)
	}

	unid Object::put_in(Solid& s)
	{
		unid ret=this->add(s);
		s.destroy();
		return ret;
	}
	unid Object::put_in(Shape& s)
	{
		unid ret=this->add(s);
		return ret;
	}
	unid Object::put_in(Object& m)
	{
		unid ret=this->add(m);
		m.destroy();
		return ret;
	}
	//if the light isn't set, it will be set automaticly
	unid Object::put_in(Light& s)
	{
		unid ret=this->add(s);
		return ret;
	}
	//the hitbox most be installed
	unid Object::put_in(Hitbox& h)
	{
		unid ret=this->add(h);
		h.destroy();
		return ret;
	}

#define OBJECT_DESTROY_INDICED(vector_name, unid_name)               \
	in_hbx.del(unid_name.id());                                      \
	vector_name[ vector_name.size()-1 ].vector_indice=unid_name.id();\
	vector_name[ unid_name.id() ].destroy();                         \
	switch_pop_vector(vector_name, unid_name.id());

	void Object::destroy_solid(const unid& indice)
	{
		OBJECT_DESTROY_INDICED(solids, indice)
	}
	void Object::destroy_shape(const unid& indice)
	{
		OBJECT_DESTROY_INDICED(shapes, indice)
	}
	void Object::destroy_object(const unid& indice)
	{
		OBJECT_DESTROY_INDICED(objects, indice)
	}
	void Object::destroy_light(const unid& indice)
	{
		OBJECT_DESTROY_INDICED(lights, indice)
	}
	void Object::destroy_hitbox(const unid& indice)
	{
		OBJECT_DESTROY_INDICED(hitboxs, indice)
	}


#define OBJECT_POP_INDICED(vector_name, unid_name, type)             \
	in_hbx.del(unid_name.id());                                      \
	type* copy_ptr=vector_name[ unid_name.id() ].attr->get_copy();   \
	vector_name[ vector_name.size()-1 ].vector_indice=unid_name.id();\
	vector_name[ unid_name.id() ].destroy();                         \
	switch_pop_vector(vector_name, unid_name.id());                  \
	return copy_ptr;

	Solid* Object::pop_solid(const unid& indice)
	{
		OBJECT_POP_INDICED(solids, indice, Solid)
	}
	Shape* Object::pop_shape(const unid& indice)
	{
		OBJECT_POP_INDICED(shapes, indice, Shape)
	}
	Object* Object::pop_object(const unid& indice)
	{
		OBJECT_POP_INDICED(objects, indice, Object)
	}
	Light* Object::pop_light(const unid& indice)
	{
		OBJECT_POP_INDICED(lights, indice, Light)
	}
	Hitbox* Object::pop_hitbox(const unid& indice)
	{
		OBJECT_POP_INDICED(hitboxs, indice, Hitbox)
	}


	//TODO: solid methods

	Solid&  Object::select_solid(const unid& solid_indice)
	{
		return *solids[solid_indice.id()].attr;
	}
	Shape&  Object::select_shape(const unid& shape_indice)
	{
		return *shapes[shape_indice.id()].attr;
	}
	Hitbox& Object::select_hitbox(const unid& hitbox_indice)
	{
		return *hitboxs[hitbox_indice.id()].attr;
	}
	Object& Object::select_object(const unid& object_indice)
	{
		return *objects[object_indice.id()].attr;
	}
	Light&  Object::select_light(const unid& light_indice)
	{
		return *lights[light_indice.id()].attr;
	}

	unsigned int Object::count_hitboxs()
	{
		return hitboxs.size();
	}
	unsigned int Object::count_solids()
	{
		return solids.size();
	}
	unsigned int Object::count_shapes()
	{
		return shapes.size();
	}
	unsigned int Object::count_objects()
	{
		return objects.size();
	}
	unsigned int Object::count_lights()
	{
		return lights.size();
	}

	void Object::draw_borders(float thickness)
	{
		FOR_OBJ_VECT(Object, objects)
		{
			att.attr->draw_borders();
		}
		FOR_OBJ_VECT(Solid, solids)
		{
			att.attr->draw_borders();
		}
		FOR_OBJ_VECT(Hitbox, hitboxs)
		{
			att.attr->draw_borders();
		}
	}

	vec3 Object::get_position() const
	{
		return position;
	}

	void Object::tp(const vec3& new_position)
	{
		FOR_OBJ_VECT(Shape, shapes)
		{
			vec3 dp=att.attr->get_position()-position;
			att.attr->tp(new_position + dp);
		}
		FOR_OBJ_VECT(Solid, solids)
		{
			vec3 dp=att.attr->get_position()-position;
			att.attr->tp(new_position + dp);
		}
		FOR_OBJ_VECT(Object, objects)
		{
			vec3 dp=att.attr->get_position()-position;
			att.attr->tp(new_position + dp);
		}

		position=new_position;
	}
	void Object::move(const vec3& dp)
	{
		this->tp(position+dp);
	}
	void Object::compute_rotate(float angle, const vec3& axis, const vec3& point)
	{
		FOR_OBJ_VECT(Shape, shapes)
		{
			att.attr->rotate(angle, axis, point);
		}
		FOR_OBJ_VECT(Solid, solids)
		{
			att.attr->rotate(angle, axis, point);
		}
		FOR_OBJ_VECT(Object, objects)
		{
			att.attr->rotate(angle, axis, point);
		}
	}
	void Object::scale(float ratio)
	{
		this->scale_by_point(ratio, position);
	}
	void Object::scale_by_point(float ratio, const vec3& point)
	{
		FOR_OBJ_VECT(Shape, shapes)
		{
			att.attr->scale_by_point(ratio, point);
		}
		FOR_OBJ_VECT(Solid, solids)
		{
			att.attr->scale_by_point(ratio, point);
		}
		FOR_OBJ_VECT(Object, objects)
		{
			att.attr->scale_by_point(ratio, point);
		}

		sgl::scale(position, ratio, point);
	}

	void Object::draw()
	{
		FOR_OBJ_VECT(Shape, shapes)
		{
			att.attr->draw();
		}
		FOR_OBJ_VECT(Solid, solids)
		{
			att.attr->draw();
		}
		FOR_OBJ_VECT(Object, objects)
		{
			att.attr->draw();
		}
	}

	bool Object::is_hitbox_in(const int& hitbox_id_indice)
	{
		if(in_hbx.contains(hitbox_id_indice))
			return true;

		FOR_OBJ_VECT(Object, objects)
		{
			if(att.attr->is_hitbox_in(hitbox_id_indice))
			{
				return true;
			}
		}

		return false;
	}

	vector<collision> Object::collisions()
	{
		vector<collision> ret(0);
		for(obj_attr<Hitbox>& att : hitboxs)
		{
			for(const collision& col : att.attr->collisions())
			{
				if(!this->is_hitbox_in(col.id_indice))
				{
					ret.push_back(col);
				}
			}
		}
		for(obj_attr<Solid>& att : solids)
		{
			for(const collision& col : att.attr->collisions())
			{
				if(!this->is_hitbox_in(col.id_indice))
				{
					ret.push_back(col);
				}
			}
		}
		for(obj_attr<Object>& att : objects)
		{
			for(const collision& col : att.attr->collisions())
			{
				if(!this->is_hitbox_in(col.id_indice))
				{
					ret.push_back(col);
				}
			}
		}

		return ret;
	}
	vector<collision> Object::all_collisions()
	{
		vector<collision> ret(0);
		for(obj_attr<Hitbox>& att : hitboxs)
		{
			for(const collision& col : att.attr->collisions())
			{
				ret.push_back(col);
			}
		}
		for(obj_attr<Solid>& att : solids)
		{
			
			for(const collision& col : att.attr->collisions())
			{
				ret.push_back(col);
			}
		}
		for(obj_attr<Object>& att : objects)
		{
			for(const collision& col : att.attr->collisions())
			{
				ret.push_back(col);
			}
		}

		return ret;
	}
	bool Object::is_in_collision()
	{
		for(collision& c : this->collisions())
		{
			if(c.id_indice != -1 && !in_hbx.contains(c.id_indice))
			{
				return true;
			}
		}
		return false;
	}
	bool Object::is_in_all_collision()
	{
		for(collision& c : this->all_collisions())
		{
			if(c.id_indice != -1)
				return true;
		}
		return false;
	}
	vector<collision> Object::collision_with(const Hitbox& h)
	{
		vector<collision> ret(0);
		collision c;
		for(obj_attr<Hitbox>& att : hitboxs)
		{
			c=att.attr->collision_with(h);
			if(c.id_indice!=-1)
				ret.push_back(c);
		}
		for(obj_attr<Solid>& att : solids)
		{
			c=att.attr->collision_with(h);
			if(c.id_indice!=-1)
				ret.push_back(c);
		}
		for(obj_attr<Object>& att : objects)
		{
			for(collision& cv : att.attr->collision_with(h))
			{
				if(cv.id_indice!=-1)
					ret.push_back(c);
			}
		}

		return ret;
	}
	vector<collision> Object::collision_with(const Solid& s)
	{
		vector<collision> ret(0);
		collision c;
		for(obj_attr<Hitbox>& att : hitboxs)
		{
			c=att.attr->collision_with(s.hitbox_id_indice());
			if(c.id_indice!=-1)
				ret.push_back(c);
		}
		for(obj_attr<Solid>& att : solids)
		{
			c=att.attr->collision_with(s);
			if(c.id_indice!=-1)
				ret.push_back(c);
		}
		for(obj_attr<Object>& att : objects)
		{
			for(collision& cv : att.attr->collision_with(s))
			{
				if(cv.id_indice!=-1)
					ret.push_back(c);
			}
		}

		return ret;
	}
	vector<collision> Object::collision_with(Object& h)
	{
		vector<collision> v=this->collisions();
		vector<collision> ret(0);
		for(collision& c : v)
		{
			if(h.is_hitbox_in(c.id_indice))
				ret.push_back(c);
		}
		return ret;
	}
	bool Object::is_in_collision(const Hitbox& h)
	{
		return (collision_with(h).size()>0);
	}
	bool Object::is_in_collision(const Solid& s)
	{
		return (collision_with(s).size()>0);
	}
	bool Object::is_in_collision(Object& h)
	{
		return (collision_with(h).size()>0);
	}
	void Object::add_copy(const Object& o)
	{
		for(unsigned int i=0;i<o.solids.size();i++)
		{
			this->add_obj_attr(solids, *o.solids[i].attr);
		}
		for(unsigned int i=0;i<o.hitboxs.size();i++)
		{
			this->add_obj_attr(hitboxs, *o.hitboxs[i].attr);
		}
		for(unsigned int i=0;i<o.shapes.size();i++)
		{
			this->add_obj_attr(shapes, *o.shapes[i].attr);
		}
		for(unsigned int i=0;i<o.objects.size();i++)
		{
			this->add_obj_attr(objects, *o.objects[i].attr);
		}
		for(unsigned int i=0;i<o.lights.size();i++)
		{
			this->add_obj_attr(lights, *o.lights[i].attr);
		}
	}
	Object::Object(const Object& o) : Object()
	{
		in_hbx.reset();
		this->position=o.get_position();

		this->add_copy(o);
	}
	const Object& Object::operator=(const Object& o)
	{
		this->destroy();

		in_hbx.reset();
		this->position=o.get_position();

		this->add_copy(o);

		return *this;
	}

	void Object::destroy()
	{
		in_hbx.reset();
		DESTROY_OBJ_ATTR_VECTOR(Shape, shapes)
		DESTROY_OBJ_ATTR_VECTOR(Solid, solids)
		DESTROY_OBJ_ATTR_VECTOR(Object, objects)
		DESTROY_OBJ_ATTR_VECTOR(Light, lights)
		DESTROY_OBJ_ATTR_VECTOR(Hitbox, hitboxs)
		SGL_OBJECT_ATT_METHOD(clear)
	}

	Object::~Object()
	{	
		this->destroy();
	}


	
};