#ifndef _LIGHTS_HPP_
#define _LIGHTS_HPP_

#include <glm/glm.hpp> 
#include "colors.hpp"
#include "utils.hpp"

using namespace std;
using namespace glm;

namespace sgl
{

	void update_lights();

	enum light_types {
		SGL_NO_LIGHT        = (0   ),
		SGL_NORMAL_LIGHT    = (1<<0),
		SGL_OVERWHITE_LIGHT = (1<<1), //unused now
		SGL_ANTI_LIGHT      = (1<<2), //unused now

		SGL_NUMBER_LIGHT
	};	

	//if sizeof(light_info) changes, change it in SIZE_LIGHT_INFO of the fragment shader
	struct light_info
	{
		vec3 parameter;//vec3(INTENSITY, WHITE_CURSOR, MAX SATURATION)
		vec3 color;
		vec3 position;
		vec3 advanced_parameter; //vec3(distance_only_coef, # , # )
	};

	class Light
	{
		friend void set_light(Light& l);
	public:
		Light(){}
		Light(const vec3& new_position, float new_intensity, vec3 new_color=WHITE
		, float new_max_saturation=10.0f, float distance_only_coef=0.2f
		, float new_white_cursor=0.02f, int new_type=SGL_NORMAL_LIGHT);
		//white cursor: the white-additive coeffitient
		//max_saturation: the maximum color channel 

		bool is_set() const;

		void tp(const vec3& new_position);
		inline void move(const vec3& dp){tp(this->get_position()+dp);}
		void rotate(float angle, const vec3& axis, const vec3& point);
		const inline vec3& get_position(){return infos.position;} 

		SGL_VIRTUAL_GET_COPY(Light, Light)

		virtual ~Light(){}
	protected:
		light_info infos;
		int type=SGL_NORMAL_LIGHT;

		int index=-1;

		void modification_assert();
	};

	struct light_group
	{
		vector<light_info> lights_info;
		vector<int> light_types;
	};

	//keep light info
	void add_light(Light l);
	//don't keep light infos
	void set_light(Light& l);

};

#endif //_LIGHTS_HPP_
