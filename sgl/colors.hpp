#ifndef _COLORS_HPP_
#define _COLORS_HPP_

#include <glm/glm.hpp> 

using namespace glm;

#define BLACK  vec3(0.0,0.0,0.0)
#define RED    vec3(1.0,0.0,0.0)
#define GREEN  vec3(0.0,1.0,0.0)
#define BLUE   vec3(0.0,0.0,1.0)
#define PURPLE vec3(1.0,0.0,1.0)
#define YELLOW vec3(1.0,1.0,0.0)
#define CYAN   vec3(0.0,1.0,1.0)
#define WHITE  vec3(1.0,1.0,1.0)

namespace sgl
{
	inline vec3 dark(const vec3& color, float dark_coeff=0.3){return color*vec3(dark_coeff,dark_coeff,dark_coeff);};
};

#endif //_COLORS_HPP_
