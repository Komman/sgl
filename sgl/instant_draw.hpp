#ifndef _INSTANT_DRAW_HPP_
#define _INSTANT_DRAW_HPP_

#include "shape.hpp"

using namespace std;
using namespace sgl;
using namespace glm;

#define SGL_BORDER_SIZE 0.1

namespace sgl
{
	void draw_border(const vec3& p1, const vec3& p2, float thickness=SGL_BORDER_SIZE);
};


#endif //_INSTANT_DRAW_HPP_
