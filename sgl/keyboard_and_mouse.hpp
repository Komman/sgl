#ifndef _KEYBOARD_AND_MOUSE_HPP_
#define _KEYBOARD_AND_MOUSE_HPP_

#include <stdio.h> 
#include <stdlib.h>
#include <iostream>     
#include <fstream> 
#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp> 

#include "window.hpp"

using namespace glm;
using namespace std;
using namespace sgl;


namespace sgl
{
	//returns the relative position : mouse_position.x/y € [0.0,1.0] if the mouse is in the window
	vec2 mouse_position();
	void set_mouse_position(const vec2& pos);
	
	bool key_pressed(int key);
};

#endif //_KEYBOARD_AND_MOUSE_HPP_
