#ifndef _COLLISIONS_HPP_
#define _COLLISIONS_HPP_

#include <stdio.h> 
#include <stdlib.h>
#include <iostream>     
#include <fstream> 
#include <vector>

#include <glm/glm.hpp> 
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/projection.hpp>

#include "err.hpp"

#define SGL_CHECK_ALGO 

#define NO_COLLISION (collision(-1))
#define NULL_COLLISION (collision(0))

using namespace std;
using namespace glm;
using namespace sgl;

namespace sgl
{

	enum minmax_type {MIN=0, MAX};
	enum axis_names {NO_AXIS=-1, AXIS_X=0, AXIS_Y, AXIS_Z
		, NUMBER_AXIS};

	enum hitbox_type {SGL_HITBOX_NO_TYPE=0,
				  SGL_HITBOX_FIRST,
				  SGL_HITBOX_SPHERICAL=SGL_HITBOX_FIRST,
				  SGL_HITBOX_TRIANGLE,
				  SGL_HITBOX_HEXAHEDRON,
				  SGL_HITBOX_CYLINDER,
				  SGL_HITBOX_CIRCLE,

				  SGL_N_HITBOX_TYPES
				  };

	static vector<string> hitbox_type_string={
		"SGL_HITBOX_NO_TYPE",
		"SGL_HITBOX_SPHERICAL",
		"SGL_HITBOX_TRIANGLE",
		"SGL_HITBOX_HEXAHEDRON",
		"SGL_HITBOX_CYLINDER",
		"SGL_HITBOX_CIRCLE",
		"SGL_N_HITBOX_TYPES"
	};

	class intid_set
	{
	public:
		intid_set(int max_id=0){indices=vector<int>(max_id,-1);}

		inline void new_id()
		{
			indices.push_back(-1);
		}
		inline void add(int id)
		{
			if(indices[id]==-1)
			{
				inid.push_back(id);
				indices[id]=inid.size()-1;
			}
			
		}
		//Return if the element has been deleted
		bool del(int id)
		{
			if(indices[id]==-1)
			{
				return false;
			}
			else
			{
				if(inid.size()==0)
					err("current_collision.intid_set wrong set: try to remove "+to_string(id)+" while inid.size()=0");
			}
				

			if(indices[id]!=(int)inid.size()-1)
			{
				inid[ indices[id] ] = inid[ inid.size()-1 ];
				indices[ inid[ inid.size()-1 ] ]=indices[id];
			}

			inid.pop_back();
			indices[ id ]=-1;

			return true;
		}

		vector<int> inid    = vector<int>(0);
		vector<int> indices = vector<int>(0);
	};

	template<typename T> void switch_unpop_vector(vector<T>& v, unsigned int indice)
	{
		if(indice >= v.size())
			err("switch_pop_vector: indice out of range (" + to_string(indice) + "/" + to_string(v.size()) + ")");

		if(indice != v.size()-1)
		{	
			v[indice]=v[v.size()-1];
		}
	}
	template<typename T> void switch_pop_vector(vector<T>& v, unsigned int indice)
	{
		switch_unpop_vector(v, indice);
		v.pop_back();
	}

	#include "reactive_function.hpp"

	struct collision_data
	{
		//methods
		collision_data() {};
		collision_data(const vector<vec3>& infos,
					   hitbox_type t)
		{
			float_infos=infos;
			type=t;
		}
		
		collision sphere_on_sphere(collision_data& sphere_data);
		collision sphere_on_triangle(collision_data& triangle_data);
		collision sphere_on_hexahedron(collision_data& hexahedron_data);
		collision sphere_on_cylinder(collision_data& cylinder_data);
		collision sphere_on_circle(collision_data& circle_data);

		collision triangle_on_triangle(collision_data& triangle_data);
		collision triangle_on_sphere(collision_data& sphere_data);
		collision triangle_on_hexahedron(collision_data& hexahedron_data);
		collision triangle_on_cylinder(collision_data& cylinder_data);
		collision triangle_on_circle(collision_data& circle_data);

		collision hexahedron_on_triangle(collision_data& triangle_data);
		collision hexahedron_on_sphere(collision_data& sphere_data);
		collision hexahedron_on_hexahedron(collision_data& hexahedron_data);
		collision hexahedron_on_cylinder(collision_data& cylinder_data);
		collision hexahedron_on_circle(collision_data& circle_data);

		collision cylinder_on_triangle(collision_data& triangle_data);
		collision cylinder_on_sphere(collision_data& sphere_data);
		collision cylinder_on_hexahedron(collision_data& hexahedron_data);
		collision cylinder_on_cylinder(collision_data& cylinder_data);
		collision cylinder_on_circle(collision_data& circle_data);

		collision circle_on_triangle(collision_data& triangle_data);
		collision circle_on_sphere(collision_data& sphere_data);
		collision circle_on_hexahedron(collision_data& hexahedron_data);
		collision circle_on_cylinder(collision_data& cylinder_data);
		collision circle_on_circle(collision_data& circle_data);

#ifdef SGL_CHECK_ERRORS
#define SGL_COLLISION_DATA_CHECK_TYPE(checked_type)\
		if(this->type!=checked_type)               \
			err("collision_data: Wrong type used: " + hitbox_type_string[this->type] + " is not for " + string(__func__) + " (should be only "+ #checked_type +")");
#else
#define SGL_COLLISION_DATA_CHECK_TYPE(checked_type)
#endif

		//Get members
		//=== SPHERE ===
		inline vec3& sphere_center()
		{
			SGL_COLLISION_DATA_CHECK_TYPE(SGL_HITBOX_SPHERICAL)
			return float_infos[0];
		}
		inline float& sphere_radius()
		{
			SGL_COLLISION_DATA_CHECK_TYPE(SGL_HITBOX_SPHERICAL)
			return float_infos[1].x;
		}

		//=== CYLINDER ===
		inline vec3& cylinder_begin()
		{
			SGL_COLLISION_DATA_CHECK_TYPE(SGL_HITBOX_CYLINDER)
			return float_infos[0];
		}
		inline vec3& cylinder_sized_direction()
		{
			SGL_COLLISION_DATA_CHECK_TYPE(SGL_HITBOX_CYLINDER)
			return float_infos[1];
		}
		inline float& cylinder_radius()
		{
			SGL_COLLISION_DATA_CHECK_TYPE(SGL_HITBOX_CYLINDER)
			return float_infos[2][0];
		}

		//CIRCLE
		inline vec3& circle_center()
		{
			SGL_COLLISION_DATA_CHECK_TYPE(SGL_HITBOX_CIRCLE)
			return float_infos[0];
		}
		inline vec3& circle_normal()
		{
			SGL_COLLISION_DATA_CHECK_TYPE(SGL_HITBOX_CIRCLE)
			return float_infos[1];
		}
		inline float& circle_radius()
		{
			SGL_COLLISION_DATA_CHECK_TYPE(SGL_HITBOX_CIRCLE)
			return float_infos[2][0];
		}

		//=== POLYGONS (TRIANGLES, HEXAHEDRON) === 
		inline vector<vec3>& points()
		{
			return float_infos;
		}

		//members
		vector<vec3> float_infos;
		hitbox_type type;
		rfunc reactive_function =NULL;
		bool solid = true;
		hbx_infocol_autoset hbx_data;
	};

	collision_data sphere_collision_data(const vec3& position, float radius);
	collision_data triangle_collision_data(const vector<vec3>& points);
	collision_data cylinder_collision_data(const vec3& begin_center, const vec3& sized_direction, float radius);
	collision_data circle_collision_data(const vec3& center, const vec3& normal, float radius);

	typedef collision (collision_data::*test_collision)(collision_data&);

	void set_hitbox_solid(int id);
	void unset_hitbox_solid(int id);
	bool is_hitbox_solid(int id);

	test_collision get_test_collision(hitbox_type from, hitbox_type on);
	collision physic_id_collision(int id1, int id2);
	void call_reactive_function(int hbx_id, const collision& data_col);

	vector<int>& binding();
	vector<collision_data>& hitboxs_infos();

};

#endif //_COLLISIONS_HPP_
