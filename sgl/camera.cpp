#include "camera.hpp"

using namespace glm;
using namespace std;
using namespace sgl;

namespace sgl
{
	//CAMERA


	void Camera::init(const vec3& initial_position, CONTROLABLE_MODES cmode)
	{
		view_field=45.0f;
		min_view_distance=1.0f;
		max_view_distance=300.0f;
		
		tp(initial_position);

		up_head=vec3(0,1,0);

		look_at(initial_position+vec3(1.0,0.0,0.0));

		set_control_mode(cmode);

		// Obtient un identifiant pour notre variable uniforme "MVP". 
		// Seulement au moment de l'initialisation. 
		MatrixID = glGetUniformLocation(get_current_shader(), "MVP"); 

		update_camera();
		this->update_view();
	}

	void Camera::look_at(const vec3& look_at_position)
	{
		direction=look_at_position-position;
		
		update_camera();
	}

	void Camera::set_right_vector()
	{
		right_vector=-normalize(cross(vec3(0.0,1.0,0.0),direction));
	}

	void Camera::move(const vec3& move)
	{
		this->tp(position + move);
		update_camera();
	}

	void Camera::make_controled_move()
	{
		vec2 mp=mouse_position();
		
		set_right_vector();

		if(controlable!=CAMERA_ONLY_MOVE && controlable!=CAMERA_NO_CONTROL)
		{
			direction=normalize(rotate(direction, (float)((0.5-mp.x)*sensibility), vec3(0.0,1.0,0.0))); 
			
			vec3 dir_temp=normalize(rotate(direction, (float)((0.5-mp.y)*sensibility), right_vector)); 
			if(abs(dir_temp.y)<=0.98)
			{
				direction=dir_temp;	
			}
		}

		if(is_mouse_in_game())
		{
			set_mouse_position(vec2(0.5,0.5));
		}
		

		if(controlable!=CAMERA_ONLY_ROTATION && controlable!=CAMERA_NO_CONTROL && is_mouse_in_game())
		{
			if(key_pressed(control_keys[CAMERA_CONTROL_RIGHT]))
			{
				this->move(+right_vector*speed*delta_time());
			}
			if(key_pressed(control_keys[CAMERA_CONTROL_LEFT]))
			{
				this->move(-right_vector*speed*delta_time());
			}
			if(key_pressed(control_keys[CAMERA_CONTROL_FRONT]))
			{
				if(controlable==CAMERA_DIRECTIONAL_MOVE)
				{
					this->move(+direction*speed*delta_time());
				}
				else
				{
					this->move(+normalize(cross(vec3(0.0,1.0,0.0), right_vector))*speed*delta_time());	
				}
			}
			if(key_pressed(control_keys[CAMERA_CONTROL_BACK]))
			{
				if(controlable==CAMERA_DIRECTIONAL_MOVE)
				{
					this->move(-direction*speed*delta_time());
				}
				else
				{
					this->move(-normalize(cross(vec3(0.0,1.0,0.0), right_vector))*speed*delta_time());	
				}
			}
			if(key_pressed(control_keys[CAMERA_CONTROL_UP]))
			{
				this->move(+vec3(0.0,1.0,0.0)*speed*delta_time());
			}
			if(key_pressed(control_keys[CAMERA_CONTROL_DOWN]))
			{
				this->move(-vec3(0.0,1.0,0.0)*speed*delta_time());
			}
		}

		update_camera();
	}

	void Camera::update_view()
	{
		if(controlable!=CAMERA_NO_CONTROL && controlable!=CAMERA_UNDER_CONTROL && is_mouse_in_game())
		{
			this->make_controled_move();
		}

		if(view_changed)
		{
			// Matrice de projection : Champ de vision de 45° , ration 4:3, distance d'affichage : 0.1 unités <-> 100 unités 
			uint_xy ws=window_size();
			Projection = glm::perspective(view_field, (float)(ws.x) / (float)(ws.y), min_view_distance, max_view_distance); 

			View= glm::lookAt( 
			    position, // La caméra est à (4,3,3), dans l'espace monde
			    position+direction, // et regarde l'origine
			    up_head  // La tête est vers le haut (utilisez 0,-1,0 pour regarder à l'envers) 
			);

			// Matrice de modèle : une matrice d'identité (le modèle sera à l'origine) 
			Model= glm::mat4(1.0f);  // Changez pour chaque modèle ! 
			// Notre matrice ModelViewProjection : la multiplication des trois  matrices 
			MVP= Projection * View * Model; // Souvenez-vous, la multiplication de matrice fonctionne dans l'autre sens
			// Envoie notre transformation au shader actuel dans la variable uniforme "MVP" 
			// Pour chaque modèle affiché, comme la MVP sera différente (au moins pour la partie M)
			glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
			view_changed=false;

		}
		update_camera_position(position);
	}

	void Camera::set_control_mode(CONTROLABLE_MODES new_mode)
	{
		if(new_mode!=controlable)
		{
			controlable=new_mode;

			if(controlable!=CAMERA_NO_CONTROL)
			{
				control_keys.resize(CAMERA_NUMBER_CONTROLS);

				control_keys[CAMERA_CONTROL_RIGHT] = SGL_KEY(D);
				control_keys[CAMERA_CONTROL_LEFT]  = SGL_KEY(Q);
				control_keys[CAMERA_CONTROL_FRONT] = SGL_KEY(Z);
				control_keys[CAMERA_CONTROL_BACK]  = SGL_KEY(S);
				control_keys[CAMERA_CONTROL_UP]    = SGL_KEY(SPACE);
				control_keys[CAMERA_CONTROL_DOWN]  = SGL_KEY(LEFT_SHIFT);

				sensibility=3.0;
				speed=10.0;
				if(is_mouse_in_game())
				{
					set_mouse_position(vec2(0.5,0.5));
					if(true)
					{
						set_mouse_visibility(false);
					}
				}
				
			}
		}
	}
}