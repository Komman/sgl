#ifndef _MODELS_HPP_
#define _MODELS_HPP_

#include <stdio.h> 
#include <stdlib.h>  
#include <fstream> 
#include <iostream>
#include <vector>

#include "err.hpp"

#include <glm/glm.hpp> 

using namespace std;
using namespace sgl;
using namespace glm;

#define SGL_STRING_UNDECLARED "sgl_string_undeclared"
#define SGL_VERTEX_COLOR_UNDECLARED vec3(-1.0,-1.0,-1.0)

namespace sgl_dev
{
	template<typename T>
	struct indiced_data
	{
		vector<T> data;
		vector<unsigned int> indices;
	};
	struct material
	{
		material(){
			names.resize(0);
			colors.resize(0);
		}

		vector<string> names;
		vector<vec3> colors;

		void check()
		{
			if(names.size()!=colors.size())
			{
				string s1="-Number of names: " + to_string(names.size())+"\n";
		    	string s2="-Number of colors : " + to_string(colors.size())+"\n";
		    	err("No names to all colors:\n"+s1+s2);
			}
		}
	};
};

using namespace sgl_dev;


namespace sgl
{
	struct Model
	{
	public:
		Model(){}

		template<typename... Targs> Model(Targs... args) {this->init(args...);}

		void init(const char* path_and_name);

		indiced_data<vec3> vertexs;
		vector<vec3> colors;
		indiced_data<vec3> normals;
		indiced_data<uvec2> uv_coord;

		material mats;

		//the model is in a paver form (position) to (position+size)
		vec3 position;
		vec3 size;

	protected:
		void build_line_obj(const vector<string>& line);
		void build_line_mtn(const vector<string>& line);
	
		void invalid_current_mat()
		{
			if(current_mat_dec==SGL_STRING_UNDECLARED)
				err("Material not declared yet in mtl file");
		}
		void invalid_current_color()
		{
			if(current_vertex_color==SGL_VERTEX_COLOR_UNDECLARED)
				err("Vertex color not declared yet in mtl file");
		}
		void set_vertex_color(const string& material_name)
		{
			mats.check();

			bool found=false;
			for(unsigned int i=0;i<mats.names.size();i++)
			{
				if(mats.names[i] == material_name)
				{
					current_vertex_color = mats.colors[i];
					found=true;
				}
			}
			if(!found)
			{
				err("Vertex color do not exist: " + material_name);
			}
		}
		void set_limits()
		{
			const vector<vec3>& v=vertexs.data;
			if(v.size()==0)
				err("no vertexs");

			position=v[0];
			size=v[0];
			for(const vec3& vi : v)
			{
				position=glm::min(vi,position);
				size=glm::max(vi,size);
			}
			size=size-position;
		}

		string current_mat_dec    = SGL_STRING_UNDECLARED;
		vec3 current_vertex_color = SGL_VERTEX_COLOR_UNDECLARED;
	};
};

#endif //_MODELS_HPP_
