#ifndef _REACTIVE_FUNCTION_HPP_
#define _REACTIVE_FUNCTION_HPP_

//(for dev: will be automaticly set to the hitbox id if it is !=-1 in function:physic_id_collision)
struct hbx_infocol_autoset
{
	hbx_infocol_autoset(){}
};


struct collision
{
	collision(){};
	collision(int nid_indice): id_indice(nid_indice)
	{
	}
	collision(const vec3& contact_point, const vec3& contact_normal)
		: point(contact_point), normal(contact_normal), id_indice(0)
	{
	}

	//point of the collition
	vec3 point;
	//normal of the collision contact
	vec3 normal;

	//id_indice of the hitbox that is in collision with the hitbox set
	//(for dev: will be automaticly set to the hitbox id if it is !=-1 in function:physic_id_collision)
	int id_indice;//-1 if there is no collision, and somthing >0 else

	//additionnal data
	hbx_infocol_autoset hbx_data;
};



typedef void (*rfunc)(const collision&);

#endif //_REACTIVE_FUNCTION_HPP_
