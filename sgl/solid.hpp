#ifndef _MESH_HPP_
#define _MESH_HPP_

#include <stdio.h> 
#include <stdlib.h>
#include <iostream>     
#include <fstream> 
#include <vector>

#include <glm/glm.hpp> 
#include <glm/gtx/rotate_vector.hpp>

#include "shape.hpp"
#include "hitboxs.hpp"
#include "err.hpp"
#include "orientation.hpp" 

using namespace glm;
using namespace std;
using namespace sgl;


namespace sgl
{
	class Solid
	{
	public:
		Solid(){}

		virtual Solid* get_copy() const=0;

		void tp(const vec3& pos);
		vec3 get_position() const {return shape_ptr->get_position();}
		void move(const vec3& dp){this->tp(this->get_position()+dp);}
		void rotate(float angle, const vec3& axis, const vec3& point);
		void scale_by_point(float ratio, const vec3& point);
		void scale(float ratio);
		void set_rotation(const vec3& up_vector = VEC3_UP, const vec3& front_vector = VEC3_FRONT);

		void draw() const;

		void apply_texture(Image new_texture, vec2 fixed_size=vec2(0.0,0.0));
		void apply_color(const vec3& new_color);

		void set_solid();
		void unset_solid();
		bool is_solid() const;

		void draw_borders(float thickness=SGL_BORDER_SIZE) const;


		int hitbox_id_indice() const;

		vector<collision> collisions() const;
		bool is_in_collision() const;
		collision collision_with(const Hitbox& h) const;
		collision collision_with(const Solid& h) const;
		bool is_in_collision(const Hitbox& h) const;
		bool is_in_collision(const Solid& h) const;

		void destroy();

		inline bool is_init() const {return (hbx_ptr!=NULL && shape_ptr!=NULL);}

		virtual ~Solid()
		{
			this->destroy();
		}
	protected:

		Hitbox* hbx_ptr   = NULL;
		Shape*  shape_ptr = NULL;	
	};

};

#endif //_MESH_HPP_
