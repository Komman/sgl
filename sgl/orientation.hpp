#ifndef _ORIENTATION_HPP_
#define _ORIENTATION_HPP_

#include "utils.hpp"
#include <glm/glm.hpp> 
#include <glm/gtx/rotate_vector.hpp>

using namespace std;
using namespace glm;
using namespace sgl;

namespace sgl
{
	bool is_normalized(const vec3& v1);
	bool is_orthogonal(const vec3& v1, const vec3& v2);
	

	class Orientation
	{
	public:
		void set_rotation(const vec3& up_vector = VEC3_UP, const vec3& front_vector = VEC3_FRONT);
		virtual void rotate(float angle, const vec3& axis, const vec3& point);
		virtual vec3 get_position() const=0; 

		const vec3& get_up_vector()    const {return up;   }
		const vec3& get_front_vector() const {return front;}

		void check_normalized();
		void check_ortho();
	protected:
		virtual void compute_rotate(float angle, const vec3& axis, const vec3& point)=0;

		// /!\ MOST BE NORMALIZED AND ORTHOGONAL
		vec3 up    = VEC3_UP;
		vec3 front = VEC3_FRONT;
	};
};

#endif //_ORIENTATION_HPP_
