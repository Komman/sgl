#ifndef _HITBOXS_HPP_
#define _HITBOXS_HPP_

#include <stdio.h> 
#include <stdlib.h>
#include <iostream>     
#include <fstream> 
#include <vector>

#include "err.hpp"
#include "collisions.hpp"
#include "instant_draw.hpp"


using namespace std;
using namespace glm;
using namespace sgl;

#define SGL_HITBOX_VIRTUAL_GET_COPY(solid_class_name)\
	SGL_VIRTUAL_GET_COPY(Hitbox, solid_class_name)

namespace sgl
{
	void print_hitbox_algo(int axismax=NUMBER_AXIS);

	unsigned int maximum_hitbox_numer();
	unsigned int hitboxs_total_count();

	class Hitbox
	{		
	public:
		//Triangle:
		void create(const vec3& p1, const vec3& p2, const vec3& p3);
		//Sphere:
		void create(const vec3& center, float radius);
		//Triangle or Hexahedron:
		void create(const vector<vec3>& points);
		//Cylinder:
		void create(const vec3& begin_center, const vec3& direction, float height, float radius);
		//Circle:
		void create(const vec3& center, const vec3& normal, float radius);

		virtual const Hitbox& operator=(const Hitbox& h);

		inline bool is_created() const {return (created && id_indice!=-1 && binding()[id_indice]!=-1);}
	
		virtual vec3 get_position() const=0;

		void tp(const vec3& position);
		void move(const vec3& dp);
		void rotate(float angle, const vec3& axis, const vec3& point);
		void set_rotation(const vec3& up_vector = VEC3_UP, const vec3& front_vector = VEC3_FRONT);
		void scale(float ratio);
		void scale_by_point(float ratio, const vec3& point);

		void set_solid();
		void unset_solid();
		bool is_solid() const;

		void draw_borders(float thickness=SGL_BORDER_SIZE);

		//reactive function: void new_reactive_function(const collision&);
		void set_reactive_collision(rfunc new_reactive_function);
		void unset_reactive_collision();

		vector<collision> collisions();
		collision collision_with(const int& hbx_id_indice);
		collision collision_with(const Hitbox& h);
		inline bool is_in_collision(const Hitbox& h) {return (this->collision_with(h).id_indice==h.id_indice);}
		inline bool is_in_collision(const int& hbx_id_indice) {return (this->collision_with(hbx_id_indice).id_indice==hbx_id_indice);}
		inline bool is_in_collision() {return (this->collisions().size()!=0);}

		inline const hitbox_type& get_type() const {return this->type;}
		
		const int& get_id_indice() const {return id_indice;}

		void destroy();

		virtual ~Hitbox(){if(this->is_created()) this->destroy();}

		virtual Hitbox* get_copy() const=0;
	protected:
		int id() const;

		void add_sphere(const vec3& position, float radius);
		void add_triangle(const vector<vec3>& three_points);
		void add_triangle(const vec3& p1, const vec3& p2, const vec3& p3);
		void add_hexahedron(const vector<vec3>& eight_points);
		void add_cylinder(const vec3& begin_center, const vec3& direction, float height, float radius);
		void add_circle(const vec3& center, const vec3& normal, float radius);

		void install();
		void install_current_collision(int slot_id);
		void update();

		void check_not_created();
		void check_created();

		void bind_hitbox();
		void destroy_self_binding_and_info();

		vector<vec3>& points() const;

		axis_names optimised_axis();
		int get_sort_minmax_indice(axis_names a, minmax_type mm);
		bool is_in_axis_collision(axis_names a, int hitbox_id);
		vector<int> all_potential_collisions_id_indice(axis_names a);
		vector<collision> full_collisions_test();

		int minmax_indice(minmax_type mm);
		inline int min_indice(){return minmax_indice(MIN);}
		inline int max_indice(){return minmax_indice(MAX);}

		vec3 min_coord_check(const vector<vec3>& v);
		vec3 max_coord_check(const vector<vec3>& v);
		virtual vec3 get_min()=0;
		virtual vec3 get_max()=0;
		virtual void build_copy(const Hitbox& h)=0;

		virtual void compute_tp(const vec3& position)=0;
		virtual void compute_rotate(float angle, const vec3& axis, const vec3& point)=0;
		virtual void compute_scale_by_point(float ratio, const vec3& point)=0;
		virtual void compute_set_rotation(const vec3& up_vector, const vec3& front_vector)=0;

		void compute_vertexed_rotation(const vec3& up_vector, const vec3& front_vector);

		int id_indice = -1;
		hitbox_type type = SGL_HITBOX_NO_TYPE;
		bool created=false;
	};

	#include "hitboxs_specific.hpp"
 
};


#endif //_HITBOXS_HPP_
