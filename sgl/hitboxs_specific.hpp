#ifndef _HITBOXS_SPECIFIC_HPP_
#define _HITBOXS_SPECIFIC_HPP_

#define SGL_BUILD_HITBOX_GETER(type, function) inline const type& get_ ## function () const {return function ();}

#define SGL_HITBOX_CONSTRUCTOR(hbx_type) template<typename... TArgs> hbx_type(TArgs... args) : hbx_type()\
	{\
		this->create(args...);\
	}\
	hbx_type(const Hitbox& h) {this->build_copy(h);}

class Spheric_hitbox : public Hitbox
{
public:
	Spheric_hitbox() {
		Hitbox::type=SGL_HITBOX_SPHERICAL;
	}
	SGL_HITBOX_CONSTRUCTOR(Spheric_hitbox)
	
	virtual vec3 get_min();
	virtual vec3 get_max();

	virtual vec3 get_position() const;

	SGL_BUILD_HITBOX_GETER(float, radius)
	SGL_BUILD_HITBOX_GETER(vec3 , center)

	SGL_HITBOX_VIRTUAL_GET_COPY(Spheric_hitbox)
protected:
	virtual void compute_tp(const vec3& position);
	virtual void compute_rotate(float angle, const vec3& axis, const vec3& point);
	virtual void compute_scale_by_point(float ratio, const vec3& point);
	virtual void compute_set_rotation(const vec3& up_vector, const vec3& front_vector);
	
	virtual void build_copy(const Hitbox& h);

	float& radius() const;
	vec3&  center() const;
};



class Triangle_hitbox : public Hitbox
{
public:	
	Triangle_hitbox() {
		Hitbox::type=SGL_HITBOX_TRIANGLE;
	}
	SGL_HITBOX_CONSTRUCTOR(Triangle_hitbox)
	
	virtual vec3 get_min();
	virtual vec3 get_max();

	virtual vec3 get_position() const;

	SGL_BUILD_HITBOX_GETER(vector<vec3>, points)

	SGL_HITBOX_VIRTUAL_GET_COPY(Triangle_hitbox)
protected:
	virtual void compute_tp(const vec3& position);
	virtual void compute_rotate(float angle, const vec3& axis, const vec3& point);
	virtual void compute_scale_by_point(float ratio, const vec3& point);
	virtual void compute_set_rotation(const vec3& up_vector, const vec3& front_vector);

	virtual void build_copy(const Hitbox& h);
};	


class Hexahedron_hitbox : public Hitbox
{
public:
	Hexahedron_hitbox() {
		Hitbox::type=SGL_HITBOX_HEXAHEDRON;
	}
	SGL_HITBOX_CONSTRUCTOR(Hexahedron_hitbox)

	virtual vec3 get_min();
	virtual vec3 get_max();

	virtual vec3 get_position() const;

	SGL_BUILD_HITBOX_GETER(vector<vec3>, points)

	SGL_HITBOX_VIRTUAL_GET_COPY(Hexahedron_hitbox)
protected:
	virtual void compute_rotate(float angle, const vec3& axis, const vec3& point);
	virtual void compute_tp(const vec3& position);
	virtual void compute_scale_by_point(float ratio, const vec3& point);
	virtual void compute_set_rotation(const vec3& up_vector, const vec3& front_vector);

	virtual void build_copy(const Hitbox& h);
};	


class Cylinder_hitbox : public Hitbox
{
public:
	Cylinder_hitbox() {
		Hitbox::type=SGL_HITBOX_CYLINDER;
	}
	SGL_HITBOX_CONSTRUCTOR(Cylinder_hitbox)

	virtual vec3 get_min();
	virtual vec3 get_max();

	virtual vec3 get_position() const;

	SGL_BUILD_HITBOX_GETER(vec3, begin)
	SGL_BUILD_HITBOX_GETER(vec3, sized_direction)
	SGL_BUILD_HITBOX_GETER(float, radius)

	SGL_HITBOX_VIRTUAL_GET_COPY(Cylinder_hitbox)
protected:
	virtual void compute_tp(const vec3& position);
	virtual void compute_rotate(float angle, const vec3& axis, const vec3& point);
	virtual void compute_scale_by_point(float ratio, const vec3& point);
	virtual void compute_set_rotation(const vec3& up_vector, const vec3& front_vector);
	
	virtual void build_copy(const Hitbox& h);

	vec3&  begin() const;
	vec3&  sized_direction() const;
	float& radius() const;
};

class Circle_hitbox : public Hitbox
{
public:
	Circle_hitbox() {
		Hitbox::type=SGL_HITBOX_CIRCLE;
	}
	SGL_HITBOX_CONSTRUCTOR(Circle_hitbox)

	virtual vec3 get_min();
	virtual vec3 get_max();

	virtual vec3 get_position() const;
	
	SGL_BUILD_HITBOX_GETER(vec3 , center)
	SGL_BUILD_HITBOX_GETER(vec3 , normal)
	SGL_BUILD_HITBOX_GETER(float, radius)

	SGL_HITBOX_VIRTUAL_GET_COPY(Circle_hitbox)
protected:
	virtual void compute_tp(const vec3& position);
	virtual void compute_rotate(float angle, const vec3& axis, const vec3& point);
	virtual void compute_scale_by_point(float ratio, const vec3& point);
	virtual void compute_set_rotation(const vec3& up_vector, const vec3& front_vector);
	
	virtual void build_copy(const Hitbox& h);

	vec3&  center() const;
	vec3&  normal() const;
	float& radius() const;

};

#endif //_HITBOXS_SPECIFIC_HPP_
