#ifndef _CAMERA_HPP_
#define _CAMERA_HPP_

#include "display.hpp"
#include "window.hpp"
#include "keyboard_and_mouse.hpp"
#include "hitboxs.hpp"

#include <stdio.h> 
#include <stdlib.h>
#include <iostream>     
#include <fstream> 
#include <iostream>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>

using namespace glm;
using namespace std;
using namespace sgl;


namespace sgl
{
	enum CONTROL_KEY_INDICES {
								CAMERA_CONTROL_RIGHT=0,
								CAMERA_CONTROL_LEFT=1,
								CAMERA_CONTROL_FRONT=2,
								CAMERA_CONTROL_BACK=3,
								CAMERA_CONTROL_UP=4,
								CAMERA_CONTROL_DOWN=5,

								CAMERA_NUMBER_CONTROLS
							 };

    enum CONTROLABLE_MODES {
    						CAMERA_NO_CONTROL=0,
    						CAMERA_DIRECTIONAL_MOVE,
    						CAMERA_CREATIVE,
    						CAMERA_ONLY_ROTATION,
    						CAMERA_ONLY_MOVE,
    						CAMERA_UNDER_CONTROL
    };

	class Camera
	{
	public:
		Camera(){};
		template<typename... TArgs> Camera(TArgs... args)
		{
			this->init(args...);
		}
		void init(const vec3& initial_position=vec3(4,4,4), CONTROLABLE_MODES cmode = CAMERA_CREATIVE);
		void update_view();
		void look_at(const vec3& look_at_position);
		inline void tp(const vec3& new_position){position=new_position;update_camera();};
		void move(const vec3& move);

		void set_control_mode(CONTROLABLE_MODES new_mode);
		inline const CONTROLABLE_MODES& get_control_mode(){return controlable;}

		inline const glm::vec3& get_position(){return position;};
		inline const glm::vec3& get_direction(){return direction;};
		inline const glm::vec3& get_right_vector(){return right_vector;};
		inline const glm::vec3& get_up_head(){return up_head;};
	
	protected:
		inline void update_camera(){view_changed=true;};
		void make_controled_move();
		void set_right_vector();

		glm::mat4 Projection;
		glm::mat4 View;
		glm::mat4 Model;
		glm::mat4 MVP;
		GLuint MatrixID;

		glm::vec3 position;	
		glm::vec3 direction;
		glm::vec3 right_vector;
		glm::vec3 up_head;

		float view_field;
		float min_view_distance;
		float max_view_distance;

		bool view_changed=true;
		CONTROLABLE_MODES controlable = CAMERA_NO_CONTROL;
		vector<int> control_keys;
		float sensibility;
		float speed;

	};
};

#endif //_CAMERA_HPP_
