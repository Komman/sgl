#ifndef _OBJECT_HPP_
#define _OBJECT_HPP_

#include <stdio.h> 
#include <stdlib.h>
#include <iostream>     
#include <fstream> 
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp> 
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/projection.hpp>

#include "shape.hpp"
#include "hitboxs.hpp"
#include "solid.hpp"
#include "lights.hpp"
#include "orientation.hpp"
#include "hitbox_group.hpp"
#include "unid.hpp"

using namespace glm;
using namespace std;
using namespace sgl;

#define SGL_OBJECT_VIRTUAL_GET_COPY(solid_class_name)\
	SGL_VIRTUAL_GET_COPY(Object, solid_class_name)

#define SGL_OBJECT_ATT_METHOD(method, ...)\
	solids.method( __VA_ARGS__  );         \
	shapes.method( __VA_ARGS__  );         \
	objects.method( __VA_ARGS__ );         \
	lights.method( __VA_ARGS__  );         \
	hitboxs.method( __VA_ARGS__ );

#define SGL_NOT_IN_OBJECT_INDICE (-1)

namespace sgl
{

	#include "object_dev.hpp"

	class Object : public Orientation
	{
	public:
		Object();
		Object(const vec3& new_position);

	/*
		add methods returns the unid of the instance added
	*/
		unid add(const Solid& s);
		unid add(const Shape& s);
		unid add(const Object& m);
		//if the light isn't set, it will be set automaticly
		unid add(const Light& s);
		unid add(const Hitbox& h);
	/*
		put_in methods add the move the instance in the object (destroys the original)
	*/
		unid put_in(Solid& s);
		unid put_in(Shape& s);
		unid put_in(Object& m);
		//if the light isn't set, it will be set automaticly
		unid put_in(Light& s);
		//the hitbox most be installed
		unid put_in(Hitbox& h);

		//take methods put the argument in the object without moving it (and destroys the old one) 
		template<typename obj_type> unid take(const obj_type& s){s.move(-this->position);return this->put_in(s);}


		void destroy_solid(const unid& indice);
		void destroy_shape(const unid& indice);
		void destroy_object(const unid& indice);
		void destroy_light(const unid& indice);
		void destroy_hitbox(const unid& indice);

	/*
		pop methods remove a part of the object and returns it
		    /!\ pop methods trigger memory allocation /!\ 
	*/
		Solid*  pop_solid(const unid& indice);
		Shape*  pop_shape(const unid& indice);
		Object* pop_object(const unid& indice);
		Light*  pop_light(const unid& indice);
		Hitbox* pop_hitbox(const unid& indice);


		//returns a reference to part of the object (in order to be able to modify those parts)
		Solid&  select_solid(const unid& solid_indice);
		Shape&  select_shape(const unid& shape_indice);
		Hitbox& select_hitbox(const unid& hitbox_indice);
		Object& select_object(const unid& object_indice);
		Light&  select_light(const unid& light_indice);

		unsigned int count_hitboxs();
		unsigned int count_solids();
		unsigned int count_shapes();
		unsigned int count_objects();
		unsigned int count_lights();
		
		void set_solid();
		void unset_solid();

		void draw_borders(float thickness=SGL_BORDER_SIZE);

		vec3 get_position() const;

		void tp(const vec3& new_position);
		void move(const vec3& dp);
		void scale_by_point(float ratio, const vec3& point);
		void scale(float ratio);

		void draw();

		//returns if the hitbox id is in the object
		bool is_hitbox_in(const int& hitbox_id_indice);

		//return the collisions with hitboxs that are not part of the object
		vector<collision> collisions();
		//returns the collisions with any hitbox (hitboxs of the object included)
		vector<collision> all_collisions();
		
		vector<collision> collision_with(const Hitbox& h);
		vector<collision> collision_with(const Solid& h);
		vector<collision> collision_with(Object& h);//the object should be const (is not because the in_hbx group is update)
			
		//only with outside hitboxs
		bool is_in_collision();
		bool is_in_collision(const Hitbox& h);
		bool is_in_collision(const Solid& h);
		bool is_in_collision(Object& h);//the object should be const (is not because the in_hbx group is update)

		//include self hitboxs
		bool is_in_all_collision();

		Object(const Object& o);
		virtual const Object& operator=(const Object& o);

		void destroy();

		virtual ~Object();

		SGL_OBJECT_VIRTUAL_GET_COPY(Object)
	protected:
		virtual void compute_rotate(float angle, const vec3& axis, const vec3& point);

		void add_copy(const Object& o);

		template<typename T> void add_obj_attr(vector< dev::obj_attr<T> >& vobj, const T& obj_to_add)
		{
			vobj.push_back(dev::obj_attr<T>(obj_to_add, (int)vobj.size()));
		}

		vec3 position;
		vec3& _position(){return position;};


		Group in_hbx;

		vector< dev::obj_attr<Solid > > solids;
		vector< dev::obj_attr<Shape > > shapes;
		vector< dev::obj_attr<Object> > objects;
		vector< dev::obj_attr<Light > > lights;
		vector< dev::obj_attr<Hitbox> > hitboxs;
	};

};


#endif //_OBJECT_HPP_
