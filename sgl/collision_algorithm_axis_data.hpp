#ifndef _COLLISION_ALGORITHM_AXIS_DATA_HPP_
#define _COLLISION_ALGORITHM_AXIS_DATA_HPP_

#include "hitboxs.hpp"
#include "collisions.hpp"

#include "utils_debug.hpp"

using namespace std;
using namespace glm;
using namespace sgl;

//#define SGL_PRINT_COLLISION_AGLO
//#define SGL_PRINT_COLLISION_AGLO_ADD_DEL
//#define SGL_PRINT_COLLISION_AGLO_OPT
//#define SGL_PRINT_COLLISION_AGLO_N_TEST
//#define SGL_PRINT_COLLISION_AGLO_N_TEST_UPDATE


namespace sgl
{
	//======= 1 : AXIS ======


	static vector<string> axis_string={"X-AXIS", "Y-AXIS", "Z-AXIS"};
	static vector<string> minmax_string={"min", "max"};

	class minmax
	{
	public:
		minmax_type t;
		float value;
		int sort_binding_indice = -444;

		int id()
		{
			#ifdef SGL_CHECK_ERRORS
			if(indice_id==-1)
				err("indice_id used but not set in minmax::id()");
			#endif
			return binding()[indice_id];
		}
		int get_indice_id()
		{
			#ifdef SGL_CHECK_ERRORS
			if(indice_id==-1)
				err("indice_id used but not set in minmax::get_indice_id()");
			#endif
			return indice_id;
		}
		void set_indice_id(int ii){indice_id=ii;}
	private:

		int indice_id=-1;
	};

	inline static int id_minmax_indice(int id, minmax_type mm)
	{
		return 2*id+(int)mm;
	}
	inline static int other_minmax_id(int indice)
	{
		return (indice/2)*2+1-(indice%2);
	}
	struct sgl_curcol
	{
		intid_set ids;
		bool curcol_enable=false;
	};

	static vector<sgl_curcol> current_collisions = vector<sgl_curcol>(0);
	/*
	current_collisions[i] contains the hiboxs id that are currently 
	in collision with the hitbox i
	*/
	
	struct axis_data
	{
		vector<int>         sort_binding       = vector<int>         (0);
		vector<minmax>      minsmaxs           = vector<minmax>      (0);
		vector<vector<int>> areas              = vector<vector<int>> (0);
		vector<vector<int>> hitboxs_to_areas   = vector<vector<int>> (0);
		axis_data* three_axis = NULL;

		inline float min_coord(int id)
		{
			return minsmaxs[2*id].value;
		}
		inline float max_coord(int id)
		{
			return minsmaxs[2*id+1].value;
		}
		inline int minmax_id_to_hitbox_id(int indice)
		{
			return minsmaxs[indice].id();	
		}
		inline vector<int> prev_area(int s_id)
		{
			if(s_id==0)
				return vector<int>(0);

			return this->areas[ this->sort_binding[s_id-1] ];
		}
		inline vector<int> prev_hitboxs_to_areas(int s_id)
		{	
			if(this->hitboxs_to_areas.size()==0)
				return vector<int>(0);

			if(s_id==0)
				return vector<int>(this->hitboxs_to_areas[0].size(),-1);
			
			return this->hitboxs_to_areas[ this->sort_binding[s_id-1] ];
		}

		inline int hitbox_id_to_id_indice(int hitbox_id)
		{
			#ifdef SGL_CHECK_ERRORS
			if(three_axis==NULL)
				err("three_axis not initialied in " + std::string(__FUNCTION__));
			#endif
			return minsmaxs[id_minmax_indice(hitbox_id, MIN)].get_indice_id();
		}
		void switch_sort_binding(int i1, int i2)
		{
			int x=sort_binding[i1];
			sort_binding[i1]=sort_binding[i2];
			sort_binding[i2]=x;

			x=minsmaxs[sort_binding[i1]].sort_binding_indice;
			minsmaxs[sort_binding[i1]].sort_binding_indice=minsmaxs[sort_binding[i2]].sort_binding_indice;
			minsmaxs[sort_binding[i2]].sort_binding_indice=x;
		}
		static bool is_in_area_collision_axis_group(axis_data axs[], int id1, int id2)
		{
			for(int a=0;a<NUMBER_AXIS;a++)
			{
				if(!axs[a].areas_collision(id1, id2))
				{
					return false;
				}
			}
			return true;
		}
		void del_one_current_collision(int on_id_hbx, int src_id_hbx)
		{
			if(current_collisions[on_id_hbx].curcol_enable 
				&& hitboxs_to_areas[ id_minmax_indice(on_id_hbx, MIN) ][src_id_hbx]==-1
				&& hitboxs_to_areas[ id_minmax_indice(src_id_hbx, MIN) ][on_id_hbx]==-1)
			{
				current_collisions[on_id_hbx].ids.del(hitbox_id_to_id_indice(src_id_hbx));
			}
		}
		void del_current_collision(int hbx1, int hbx2)
		{
			del_one_current_collision(hbx1, hbx2);
			del_one_current_collision(hbx2, hbx1);
		}
		void delete_area(int indice, int area_id)
		{
			int i_switch = hitboxs_to_areas[indice][area_id];
			if( i_switch != -1 )
			{
				
				int last_area=areas[indice][ areas[indice].size()-1 ];
				if(last_area==area_id)
				{
					areas[indice].pop_back();
					hitboxs_to_areas[indice][area_id]=-1;
				}
				else
				{
					areas[indice][i_switch]=last_area;
					areas[indice].pop_back();
					hitboxs_to_areas[indice][area_id]=-1;
					hitboxs_to_areas[indice][last_area]=i_switch;
					//cout<<hitboxs_to_areas[indice][last_area]<<" "<<i_switch<<endl;
				}

				#if defined( SGL_PRINT_COLLISION_AGLO ) || defined( SGL_PRINT_COLLISION_AGLO_ADD_DEL )
				seer(" DELETE: " + to_string(area_id) + " in " + to_string(indice) );
				#endif
			}
			else
			{
				err("Collision algorithm: MAX overflow self-MIN");
			}
		}
		void add_one_current_collision(int on_id_hbx, int src_id_hbx)
		{	
					
			//seer(to_string(on_id_hbx)+" prep " + to_string((int64_t)hitboxs_infos()[on_id_hbx].reactive_function));
			int area_test=-1;
			if(current_collisions[on_id_hbx].curcol_enable)
			{
				area_test=is_in_area_collision_axis_group(three_axis, on_id_hbx, src_id_hbx);
				if(area_test)
				{
					current_collisions[on_id_hbx].ids.add(hitbox_id_to_id_indice(src_id_hbx));
					//seer(to_string(on_id_hbx)+" laa " + to_string((int64_t)hitboxs_infos()[on_id_hbx].reactive_function));
				}
			}
			
			if((hitboxs_infos()[on_id_hbx].reactive_function != NULL && is_hitbox_solid(src_id_hbx))
			&& ( 
			 	   ( area_test!=0) 
			 	   || (area_test != -1 && is_in_area_collision_axis_group(three_axis, on_id_hbx, src_id_hbx))
			    )
			   )
			{

				collision c = physic_id_collision(on_id_hbx, src_id_hbx);
				if(c.id_indice != -1)
				{
					call_reactive_function(on_id_hbx, c);
				}
			}
		}
		void add_current_collision(int hbx1, int hbx2)
		{
			add_one_current_collision(hbx1, hbx2);
			add_one_current_collision(hbx2, hbx1);
		}
		void add_area(int indice, int area_id)
		{
			if( hitboxs_to_areas[indice][area_id] == -1 )
			{
				areas[indice].push_back(area_id);
				hitboxs_to_areas[indice][area_id]=areas[indice].size()-1;

				#if defined( SGL_PRINT_COLLISION_AGLO ) || defined( SGL_PRINT_COLLISION_AGLO_ADD_DEL ) 
				seer(" ADD: " + to_string(area_id) + " in " + to_string(indice) );
				#endif
			}
			else
			{
				err("Collision algorithm: MIN overflow self-MAX");
			}
		}
		void resort_minmax(int minmax_id)
		{
			bool good_place=false;

			#ifdef SGL_PRINT_COLLISION_AGLO_N_TEST_UPDATE
			int n_test_up=0;
			#endif

			while(!good_place)
			{
				#ifdef SGL_PRINT_COLLISION_AGLO_N_TEST_UPDATE
				n_test_up++;
				#endif

				good_place=true;
				int sort_indice = minsmaxs[minmax_id].sort_binding_indice;
				int ito_modify=-1;
				bool deletion=false;

				if(sort_indice>0 && minsmaxs[ minmax_id ].value <  minsmaxs[ sort_binding[sort_indice-1] ].value)
				{
					ito_modify=sort_indice-1;

					if( minsmaxs[ minmax_id ].t == MIN )
					{
						add_area(sort_binding[ito_modify], minsmaxs[minmax_id].id() );
					}
					else
					{
						delete_area(sort_binding[ito_modify], minsmaxs[minmax_id].id() );
						deletion=true;
					}
					#if defined( SGL_PRINT_COLLISION_AGLO )
					seer("(left)");
					#endif

					good_place=false;
				}
				if(good_place && sort_indice < (int)minsmaxs.size()-1 && minsmaxs[ minmax_id ].value >  minsmaxs[ sort_binding[sort_indice+1] ].value)
				{
					ito_modify=sort_indice+1;
					if( minsmaxs[ minmax_id ].t == MAX )
					{
						add_area(sort_binding[ito_modify], minsmaxs[minmax_id].id() );
					}
					else
					{
						delete_area(sort_binding[ito_modify], minsmaxs[minmax_id].id() );
						deletion=true;
					}
					#if defined( SGL_PRINT_COLLISION_AGLO )
					seer("(right)");
					#endif

					good_place=false;
				}

				if(ito_modify!=-1)
				{
					#if defined( SGL_PRINT_COLLISION_AGLO )
					see("-self:");
					#endif
					int to_modify=sort_binding[ito_modify];

					if( (minsmaxs[to_modify].t == MIN && ito_modify==sort_indice+1)
					 || (minsmaxs[to_modify].t == MAX && ito_modify==sort_indice-1))
					{
						add_area(minmax_id, minsmaxs[to_modify].id());
					}
					else
					{
						delete_area(minmax_id, minsmaxs[to_modify].id());
					}
					switch_sort_binding(sort_indice, ito_modify);
				

					if(minsmaxs[to_modify].t!=minsmaxs[minmax_id].t)
					{
						int hbx_tomod=minmax_id_to_hitbox_id(to_modify);
						int hbx_mmid=minmax_id_to_hitbox_id(minmax_id);
						if(!deletion)
						{
							if(is_in_area_collision_axis_group(three_axis, hbx_tomod, hbx_mmid))
							{
								add_current_collision(hbx_tomod, hbx_mmid);
							}
						}
						else
						{
							del_current_collision(hbx_tomod, hbx_mmid);
						}
					}



				}
			}

			#ifdef SGL_PRINT_COLLISION_AGLO_N_TEST_UPDATE
			seer("N_TEST UPDATE: " + to_string(n_test_up));
			#endif
		}
		bool areas_collision(int id1, int id2)
		{
			/*#ifdef SGL_CHECK_ERRORS
			if(id1<0)
				err("negative hitbox id requested: " + to_string(id1));
			if(id2<0)
				err("negative hitbox id requested: " + to_string(id2));
			if(id_minmax_indice(id1, MIN)>=(int)hitboxs_to_areas.size())
				err("area not now anymore (hitboxs_to_areas: "+ to_string(id_minmax_indice(id1, MIN)) + "/" + to_string(hitboxs_to_areas.size()) +")");
			if(id2>=(int)hitboxs_to_areas[id_minmax_indice(id1, MIN)].size())
				err("area not now anymore (hitboxs_to_areas["+to_string(id_minmax_indice(id1, MIN))+"]: "+ to_string(id2) + "/" + to_string(hitboxs_to_areas[id_minmax_indice(id1, MIN)].size()) +")");
			if(id_minmax_indice(id2, MIN)>=(int)hitboxs_to_areas.size())
				err("area not now anymore (hitboxs_to_areas: "+ to_string(id_minmax_indice(id2, MIN)) + "/" + to_string(hitboxs_to_areas.size()) +")");
			if(id1>=(int)hitboxs_to_areas[id_minmax_indice(id2, MIN)].size())
				err("area not now anymore (hitboxs_to_areas["+to_string(id_minmax_indice(id2, MIN))+"]: "+ to_string(id1) + "/" + to_string(hitboxs_to_areas[id_minmax_indice(id2, MIN)].size()) +")");
			#endif*/

			if(  hitboxs_to_areas[id_minmax_indice(id1, MIN)][id2]==-1
			  && hitboxs_to_areas[id_minmax_indice(id2, MIN)][id1]==-1 )
				return false;
			return true;
		}

		void destroy_minmax(int area_indice)
		{
			const int idmin=id_minmax_indice(area_indice, MIN);
			const int idmax=id_minmax_indice(area_indice, MAX);
			switch_pop_vector(minsmaxs, idmax);
			switch_pop_vector(minsmaxs, idmin);
		}


		void destroy_binding_area(int area_indice)
		{
			const int idmin = id_minmax_indice(area_indice, MIN);
			const int idmax = id_minmax_indice(area_indice, MAX);
			const int indice_min= minsmaxs[ idmin ].sort_binding_indice;
			const int indice_max= minsmaxs[ idmax ].sort_binding_indice;
			const int indice_last_min = minsmaxs[ minsmaxs.size()-2 ].sort_binding_indice;
			const int indice_last_max = minsmaxs[ minsmaxs.size()-1 ].sort_binding_indice;	
			
			int neg=-1;
			for(int i=indice_min+1; i<(int)sort_binding.size(); i++)
			{
				if(i==indice_max)
				{
					neg--;
				}
				else
				{
					//seer("enlevé:" + to_string(neg) + " à " + to_string(sort_binding[i]));
					minsmaxs[ sort_binding[i] ].sort_binding_indice+=neg;
				}
			}
			//seer("=");
			sort_binding[ indice_last_min ] = idmin;
			sort_binding[ indice_last_max ] = idmax;

			sort_binding.erase(sort_binding.begin() + indice_max);
			sort_binding.erase(sort_binding.begin() + indice_min);
		}

		void destroy_area_by_hitbox(int area_indice)
		{
			const int idmin = id_minmax_indice(area_indice, MIN);
			const int idmax = id_minmax_indice(area_indice, MAX);
			const int indice_min= minsmaxs[ idmin ].sort_binding_indice;
			const int indice_max= minsmaxs[ idmax ].sort_binding_indice;
			const int id_last_min = minsmaxs.size()-2;
			const int id_last_max = minsmaxs.size()-1;
			int last_area=minsmaxs.size()/2-1;
			for(int i= indice_min; i<indice_max; i++)
			{
				this->delete_area( sort_binding[i], area_indice );
			}

			if(area_indice != (int)minsmaxs.size()/2-1)
			{
				for(unsigned int i=0; i<areas.size(); i++)
				{
					const int& indice_in_area=hitboxs_to_areas[i][last_area];
					if(indice_in_area != -1)
					{
						areas[i][indice_in_area]=area_indice;
					}
				}
			}

			areas[idmin]=areas[id_last_min];
			areas[idmax]=areas[id_last_max];

			areas.pop_back();
			areas.pop_back();
		}

		void destroy_area_in_hitbox_to_areas(int area_indice)
		{
			const int idmin = id_minmax_indice(area_indice, MIN);
			const int idmax = id_minmax_indice(area_indice, MAX);
			const int id_last_min = minsmaxs.size()-2;
			const int id_last_max = minsmaxs.size()-1;
			int last_area=minsmaxs.size()/2-1;

			hitboxs_to_areas[idmin]=hitboxs_to_areas[id_last_min];
			hitboxs_to_areas[idmax]=hitboxs_to_areas[id_last_max];

			hitboxs_to_areas.pop_back();
			hitboxs_to_areas.pop_back();

			for(unsigned int i=0; i<hitboxs_to_areas.size(); i++)
			{
				hitboxs_to_areas[i][area_indice]=hitboxs_to_areas[i][last_area];
				hitboxs_to_areas[i].pop_back();
			}
		}
		void destroy_current_collision(int area_indice)
		{
			for(sgl_curcol& cc : current_collisions)
			{
				cc.ids.del(hitbox_id_to_id_indice(area_indice));
			}
		}

		void destroy_area(int area_indice)
		{
			this->destroy_current_collision(area_indice);
			this->destroy_area_by_hitbox(area_indice);
			this->destroy_area_in_hitbox_to_areas(area_indice);
			this->destroy_binding_area(area_indice);
			this->destroy_minmax(area_indice);
		}
	};
};

#endif //_COLLISION_ALGORITHM_AXIS_DATA_HPP_
