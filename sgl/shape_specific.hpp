/*#ifndef _SHAPE_SPECIFIC_HPP_
#define _SHAPE_SPECIFIC_HPP_*/

class FacedShape : public Shape
{
public:
	FacedShape(){}
	template<typename... TArgs> FacedShape(TArgs... args)
	{
		this->init(args...);
	}
	SGL_SHAPE_VIRTUAL_GET_COPY(FacedShape)
protected:
	virtual void drawing_function();
	virtual void init_indices_default();
	void fill_rectangle(const vec3& position, const vec3& direction1, const vec3& direction2);
};

class Triangle : public Shape
{
public:
	Triangle(){}
	template<typename... TArgs> Triangle(TArgs... args)
	{
		this->init(args...);
	}

	template<typename... TArgs> void init(TArgs... args)
	{
		init_drawing_buffers(N_INDICES_TRIANGLE);
		this->init_data(args...);
		init_data_buffer();

		this->set_vertexed_up_front();
	}
	void init_data(const vec3 points[], const vec3& new_color)
	{
		this->fill_one_color(new_color);

		fill_triangle(points);

	}
	void init_data(const vec3& p1, const vec3& p2, const vec3& p3, const vec3& new_color)
	{
		vec3 p[]={p1,p2,p3};
		this->init_data(p, new_color);
	}

	SGL_SHAPE_VIRTUAL_GET_COPY(Triangle)
protected:
};

class Rectangle : public FacedShape
{	
public:
	Rectangle(){};
	template<typename... TArgs> Rectangle(TArgs... args)
	{
		this->init(args...);
	}
	template<typename... TArgs> void init(TArgs... args)
	{
		init_drawing_buffers(N_VERTEX_RECTANGLE);
		this->init_data(args...);
		init_data_buffer();

		this->set_vertexed_up_front();
	}		
	void init_data(const vec3& position, const vec3& direction1, const vec3& direction2, const vec3& new_color=WHITE);

	SGL_SHAPE_VIRTUAL_GET_COPY(Rectangle)
protected:

};

class Hexahedron : public FacedShape
{
public:
	Hexahedron(){};
	template<typename... TArgs> Hexahedron(TArgs... args)
	{
		this->init(args...);
	}
	template<typename... TArgs> void init(TArgs... args)
	{
		init_drawing_buffers(N_VERTEX_SQUARE);
		this->init_data(args...);
		init_data_buffer();

		this->set_vertexed_up_front();
	}	
	void init_data(const vec3& position, const vec3& direction1, const vec3& direction2, const vec3& direction3, const vec3& new_color=WHITE);

	SGL_SHAPE_VIRTUAL_GET_COPY(Hexahedron)
protected:

};

class Paver : public Hexahedron
{
public:
	Paver(){};
	template<typename... TArgs> Paver(TArgs... args)
	{
		this->init(args..., N_VERTEX_SQUARE);
	}
	void init(const vec3& position, const vec3& size, int n_vertices)
	{
		init_drawing_buffers(n_vertices);
		this->init_data(position,size);
		init_data_buffer();

		this->set_vertexed_up_front();
	}
	void init(const vec3& position, const vec3& size, const vec3& new_color, int n_vertices)
	{
		init_drawing_buffers(n_vertices);
		this->init_data(position,size, new_color);
		init_data_buffer();
	}

	void init_data(const vec3& position, const vec3& size, const vec3& new_color);
	void init_data(const vec3& position, const vec3& size);

	void transform(const vec3& new_position, const vec3& new_size, const vec3& new_color);

	SGL_SHAPE_VIRTUAL_GET_COPY(Paver)
protected:

};

#define SGL_TESS_FACTOR_TO_NPOINTS_CIRCLE(TESS_FACTOR) (TESS_FACTOR)
#define SGL_CIRCLE_TESS_FACTOR_DEFAULT (40)

class vCircle : public Shape
{
public:
	vCircle(){}
	template<typename... TArgs> vCircle(TArgs... args)
	{
		this->init(args...);
	}
	template<typename... TArgs> void init(TArgs... args)
	{
		TESS_FACTOR=SGL_CIRCLE_TESS_FACTOR_DEFAULT;
		this->init_drawing_buffers(SGL_TESS_FACTOR_TO_NPOINTS_CIRCLE(TESS_FACTOR));
		this->init_data(args...);
		init_data_buffer();
	}
	void init_data(const vec3& point, const vec3& normal, float radius, const vec3& new_color=WHITE);
	
	virtual void init_indices_default();
	virtual inline vec3 get_position() const {return _center;}

	SGL_SHAPE_VIRTUAL_GET_COPY(vCircle)
protected:

	virtual void post_tp(const vec3& position);
	virtual void post_rotate(float angle, const vec3& axis, const vec3& axis_point);
	virtual void post_scale_by_point(float ratio, const vec3& point);

	int TESS_FACTOR=SGL_CIRCLE_TESS_FACTOR_DEFAULT;
	vec3 _center;
};

#define SGL_TESS_FACTOR_TO_NPOINTS_CYLINDER(TESS_FACTOR) (2*TESS_FACTOR)
#define SGL_CYLINDER_TESS_FACTOR_DEFAULT (40)

class vCylinder : public Shape 
{
public:
	vCylinder(){}
	template<typename... TArgs> vCylinder(TArgs... args)
	{
		this->init(args...);
	}
	template<typename... TArgs> void init(TArgs... args)
	{
		TESS_FACTOR=SGL_CYLINDER_TESS_FACTOR_DEFAULT;
		init_drawing_buffers(SGL_TESS_FACTOR_TO_NPOINTS_CYLINDER(TESS_FACTOR));
		this->init_data(args...);
		init_data_buffer();
	}
	void init_data(const vec3& point, const vec3& sized_direction, float radius, const vec3& new_color=WHITE);
	virtual void init_indices_default();
	virtual inline vec3 get_position() const {return _begin;};

	SGL_SHAPE_VIRTUAL_GET_COPY(vCylinder)
protected:

	virtual void post_tp(const vec3& position);
	virtual void post_rotate(float angle, const vec3& axis, const vec3& axis_point);
	virtual void post_scale_by_point(float ratio, const vec3& point);

	vec3 _begin;
	int TESS_FACTOR=SGL_CYLINDER_TESS_FACTOR_DEFAULT;
};



#define SGL_TESS_FACTOR_TO_NPOINTS(TESS_FACTOR) (TESS_FACTOR*(TESS_FACTOR-2)+2)
#define SGL_SPHERE_TESS_FACTOR_DEFAULT 20

class vSphere : public Shape
{
public:
	vSphere(){}
	template<typename... TArgs> vSphere(TArgs... args)
	{
		this->init(args...);
	}

	template<typename... TArgs> void init(TArgs... args)
	{
		TESS_FACTOR=SGL_SPHERE_TESS_FACTOR_DEFAULT;
		init_drawing_buffers(SGL_TESS_FACTOR_TO_NPOINTS(TESS_FACTOR));
		this->init_data(args...);
		init_data_buffer();
	}
	void init_data(const vec3& position, float radius, const vec3& color=WHITE);
	virtual void init_indices_default();

	inline vec3 get_center() const {return (get_vertex(0)+get_vertex(1))/2.0f;}
	virtual inline vec3 get_position() const {return get_center();}

	SGL_SHAPE_VIRTUAL_GET_COPY(vSphere)
protected:

	int sphere_point(int thetha_indice, int phi_indice);
	void iadd_extrm(const vec3& center, const vec3& extrm_direction);

    int TESS_FACTOR;
};

class Shape_model : public Shape
{
public:
	Shape_model(){}
	template<typename... TArgs> Shape_model(TArgs... args)
	{
		this->init(args...);
	}

	void init(const Model& m, const vec3& pos);

	inline const vec3& get_begin(){return begin;}
	inline const vec3& get_size(){return size;}

	virtual void init_indices_default(){}
	virtual inline vec3 get_position() const  {return begin;}

	SGL_SHAPE_VIRTUAL_GET_COPY(Shape_model)
protected:
	virtual void post_tp(const vec3& position);
	virtual void post_rotate(float angle, const vec3& axis, const vec3& axis_point);
	virtual void post_scale_by_point(float ratio, const vec3& point);

	vec3 begin;
	vec3 size;
};


//#endif //_SHAPE_SPECIFIC_HPP_
