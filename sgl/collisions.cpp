#include "collisions.hpp"
#include "hitboxs.hpp"


//#define PRINT_TYPE_COLLISION


namespace sgl
{
	//===== SPHERE ========

	collision collision_data::sphere_on_sphere(collision_data& sphere_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("sphere on sphere");
		#endif

		float radius_sum       = this->sphere_radius()+sphere_data.sphere_radius();
		vec3  normal           = this->sphere_center()-sphere_data.sphere_center();
		float distance_spheres = glm::length(normal);

		normal=glm::normalize(normal);

		vec3 contact=sphere_data.sphere_center()+normal*distance_spheres*sphere_data.sphere_radius();

		if(distance_spheres<=radius_sum)
		{
			return collision(contact, normal);
		}
		else
		{
			return NO_COLLISION;
		}
	}
	collision collision_data::sphere_on_triangle(collision_data& triangle_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("sphere on triangle");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::sphere_on_hexahedron(collision_data& hexahedron_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("sphere on hexahedron");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::sphere_on_cylinder(collision_data& cylinder_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("sphere on cylinder");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::sphere_on_circle(collision_data& circle_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("sphere on circle");
		#endif
		return NULL_COLLISION;
	}



	//===== TRIANGLE ========




	
	collision collision_data::triangle_on_sphere(collision_data& triangle_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("triangle on sphere");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::triangle_on_triangle(collision_data& triangle_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("triangle on triangle");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::triangle_on_hexahedron(collision_data& hexahedron_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
				
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::triangle_on_cylinder(collision_data& cylinder_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("triangle on cylinder");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::triangle_on_circle(collision_data& circle_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("triangle on circle");
		#endif
		return NULL_COLLISION;
	}



	//===== HEXAHEDRON ========





	collision collision_data::hexahedron_on_sphere(collision_data& triangle_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("hexahedron on sphere");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::hexahedron_on_triangle(collision_data& triangle_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("hexahedron on sphere");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::hexahedron_on_hexahedron(collision_data& hexahedron_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("hexahedron on hexahedron");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::hexahedron_on_cylinder(collision_data& cylinder_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("hexahedron on cylinder");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::hexahedron_on_circle(collision_data& circle_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("hexahedron on circle");
		#endif
		return NULL_COLLISION;
	}




	//===== CYLINDER ========



	
	collision collision_data::cylinder_on_sphere(collision_data& triangle_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("cylinder on sphere");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::cylinder_on_triangle(collision_data& triangle_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("cylinder on triangle");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::cylinder_on_hexahedron(collision_data& hexahedron_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("cylinder on hexahedron");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::cylinder_on_cylinder(collision_data& cylinder_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("cylinder on cylinder");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::cylinder_on_circle(collision_data& circle_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("cylinder on circle");
		#endif
		return NULL_COLLISION;
	}




	//===== CIRCLE ========





	
	collision collision_data::circle_on_sphere(collision_data& triangle_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("circle on sphere");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::circle_on_triangle(collision_data& triangle_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("circle on triangle");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::circle_on_hexahedron(collision_data& hexahedron_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("circle on hexahedron");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::circle_on_cylinder(collision_data& cylinder_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("circle on cylinder");
		#endif
		return NULL_COLLISION;
	}
	collision collision_data::circle_on_circle(collision_data& circle_data)
	{
		//TODO
		#ifdef PRINT_TYPE_COLLISION
		seer("circle on circle");
		#endif
		return NULL_COLLISION;
	}

	//====== FUNCTIONS ======

	using cd_t = collision_data;

	static test_collision specific_collision[SGL_N_HITBOX_TYPES][SGL_N_HITBOX_TYPES]=
	{
		{&cd_t::sphere_on_sphere      , &cd_t::sphere_on_triangle    , &cd_t::sphere_on_hexahedron    , &collision_data::sphere_on_cylinder    , &collision_data::sphere_on_circle    },
		{&cd_t::triangle_on_sphere    , &cd_t::triangle_on_triangle  , &cd_t::triangle_on_hexahedron  , &collision_data::triangle_on_cylinder  , &collision_data::triangle_on_circle  },
		{&cd_t::hexahedron_on_sphere  , &cd_t::hexahedron_on_triangle, &cd_t::hexahedron_on_hexahedron, &collision_data::hexahedron_on_cylinder, &collision_data::hexahedron_on_circle},
		{&cd_t::cylinder_on_sphere    , &cd_t::cylinder_on_triangle  , &cd_t::cylinder_on_hexahedron  , &collision_data::cylinder_on_cylinder  , &collision_data::cylinder_on_circle  },
		{&cd_t::circle_on_sphere      , &cd_t::circle_on_triangle    , &cd_t::circle_on_hexahedron    , &collision_data::circle_on_cylinder    , &collision_data::circle_on_circle    }
	};

	test_collision get_test_collision(hitbox_type from, hitbox_type on)
	{
		return specific_collision[from-SGL_HITBOX_FIRST][on-SGL_HITBOX_FIRST];
	}

	//======= HITBOXS DATA =======

	static vector<int>            _binding      (0);
	static vector<collision_data> _hitboxs_infos(0);

	vector<int>& binding()
	{
		return _binding;
	}
	vector<collision_data>& hitboxs_infos()
	{	
		return _hitboxs_infos;
	}

	unsigned int maximum_hitbox_numer()
	{
		return _binding.size();
	}
	unsigned int hitboxs_total_count()
	{
		int total=0;
		for(int& ib : _binding)
		{
			if(ib!=-1)
				total++;
		}
		return total;
	}

	collision_data sphere_collision_data(const vec3& position, float radius)
	{
		return collision_data({position, vec3(radius, 0.0, 0.0)}, SGL_HITBOX_SPHERICAL);
	}
	collision_data triangle_collision_data(const vector<vec3>& points)
	{
		return collision_data(points, SGL_HITBOX_TRIANGLE);
	}
	/*static collision_data triangle_collision_data(const vec3& p1, const vec3& p2, const vec3& p3)
	{
		return triangle_collision_data({p1, p2, p3});
	}*/
	collision_data cylinder_collision_data(const vec3& begin_center, const vec3& sized_direction, float radius)
	{
		vector<vec3> data(0);
		data.push_back(begin_center);
		data.push_back(sized_direction);
		data.push_back(vec3(radius, -444.0, -444.0));
		return collision_data(data, SGL_HITBOX_CYLINDER);
	}
	collision_data circle_collision_data(const vec3& center, const vec3& normal, float radius)
	{
		vector<vec3> data(0);
		data.push_back(center);
		data.push_back(normal);
		data.push_back(vec3(radius, -444.0, -444.0));
		return collision_data(data, SGL_HITBOX_CIRCLE);
	}

	void Hitbox::add_sphere(const vec3& position, float radius)
	{
		check_not_created();
		_hitboxs_infos.push_back(sphere_collision_data(position, radius));
		bind_hitbox();
	}
	void Hitbox::add_triangle(const vector<vec3>& three_points)
	{
		check_not_created();
		_hitboxs_infos.push_back(collision_data(three_points,
												 SGL_HITBOX_TRIANGLE));
		bind_hitbox();
	}
	void Hitbox::add_triangle(const vec3& p1, const vec3& p2, const vec3& p3)
	{
		vector<vec3> v={p1,p2,p3};
		add_triangle(v);
	}
	void Hitbox::add_hexahedron(const vector<vec3>& eight_points)
	{
		check_not_created();
		_hitboxs_infos.push_back(collision_data(eight_points,
												 SGL_HITBOX_HEXAHEDRON));
		bind_hitbox();
	}
	void Hitbox::add_cylinder(const vec3& begin_center, const vec3& sized_direction, float height, float radius)
	{
		check_not_created();
		_hitboxs_infos.push_back(cylinder_collision_data(begin_center,normalize(sized_direction)*height, radius));
		bind_hitbox();
	}
	void Hitbox::add_circle(const vec3& center, const vec3& normal, float radius)
	{
		check_not_created();
		_hitboxs_infos.push_back(circle_collision_data(center,normal, radius));
		bind_hitbox();
	}

	void Hitbox::bind_hitbox()
	{
		unsigned int new_id_indice;
		for(new_id_indice=0; new_id_indice<_binding.size();new_id_indice++)
			if(_binding[new_id_indice]==-1)
				break;
			
		id_indice=new_id_indice;

		int bound_indice=_hitboxs_infos.size()-1;
		if(new_id_indice ==_binding.size())
		{
			_binding.push_back(bound_indice);
		}
		else
		{
			_binding[new_id_indice]=bound_indice;
		}
	}

	int Hitbox::id() const
	{
		if(id_indice==-1)
		{
			err("Hitbox::id(): Hitbox not initialized");
		}
		return  _binding[id_indice];
	}

	//====== SOLID ======

	void set_hitbox_solid(int id)
	{
		_hitboxs_infos[id].solid=true;
	}
	void unset_hitbox_solid(int id)
	{
		_hitboxs_infos[id].solid=false;
	}
	bool is_hitbox_solid(int id)
	{
		return _hitboxs_infos[id].solid;
	}

	//SPHERE

	vec3& Spheric_hitbox::center() const
	{
		return _hitboxs_infos[id()].sphere_center();
	}
	float& Spheric_hitbox::radius() const
	{
		return _hitboxs_infos[id()].sphere_radius();
	}

	//CYLINDER

	vec3& Cylinder_hitbox::begin() const
	{
		return _hitboxs_infos[id()].cylinder_begin();
	}
	vec3& Cylinder_hitbox::sized_direction() const
	{
		return _hitboxs_infos[id()].cylinder_sized_direction();
	}
	float& Cylinder_hitbox::radius() const
	{
		return _hitboxs_infos[id()].cylinder_radius();
	}

	//CIRCLE

	vec3& Circle_hitbox::center() const
	{
		return _hitboxs_infos[id()].circle_center();;
	}
	vec3& Circle_hitbox::normal() const
	{
		return _hitboxs_infos[id()].circle_normal();;
	}
	float& Circle_hitbox::radius() const
	{
		return _hitboxs_infos[id()].circle_radius();
	}

	//POLYGONS (TRIANGLES, HEXAHEDRON)

	vector<vec3>& Hitbox::points() const
	{
		return _hitboxs_infos[id()].points();
	}

	// REACTIVE COLLISIONS

	void Hitbox::set_reactive_collision(rfunc new_reactive_function)
	{
		_hitboxs_infos[id()].reactive_function = new_reactive_function;
		//seer(to_string(id())+" set: "+ to_string((int64_t)_hitboxs_infos[id()].reactive_function));
	}

	void Hitbox::unset_reactive_collision()
	{
		_hitboxs_infos[id()].reactive_function = NULL;
	}

	void call_reactive_function(int hbx_id, const collision& data_col)
	{
		if(_hitboxs_infos[hbx_id].reactive_function==NULL)
			err("No reactive collision function");

		(*_hitboxs_infos[hbx_id].reactive_function)(data_col);
	}


};