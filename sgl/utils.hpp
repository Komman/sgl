#ifndef _UTILS_HPP_
#define _UTILS_HPP_

#include <stdio.h> 
#include <stdlib.h>
#include <iostream>     
#include <fstream> 
#include <vector>

#include <glm/glm.hpp> 
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/rotate_vector.hpp>

using namespace std;
using namespace glm;

#define SGL_VIRTUAL_GET_COPY(original_class, class_name) \
	virtual original_class* get_copy()  const            \
	{                                                    \
		class_name* s=new class_name;                    \
		*s=*this;                                        \
		return s;                                        \
	}

#define SGL_EPSILON (0.0001)

#define VEC3_ORIGIN (vec3(0.0,0.0,0.0))
#define VEC3_UP     (vec3(0.0,1.0,0.0))
#define VEC3_FRONT  (vec3(1.0,0.0,0.0))

namespace sgl
{
	inline void scale(vec3& v, float ratio, const vec3& point)
	{
		v=point+(v-point)*ratio;
	}
	inline bool eq_0(float x)
	{
		return (abs(x)<=SGL_EPSILON);
	}
	inline vec3 get_sec_vec3_for_plan(const vec3& v1, const vec3& v2)
	{
		vec3 axis_tmp=glm::normalize(glm::cross(v1, v2));
		return glm::normalize(glm::rotate(v1, (float)((M_PI)/2.0), axis_tmp));
	}
	inline bool vec3_equal(const vec3& v1, const vec3& v2)
	{
		return (abs(glm::length(glm::abs(v1)-glm::abs(v2)))<=SGL_EPSILON);
	}
	inline bool vec3_colinear(const vec3& v1, const vec3& v2)
	{
		return vec3_equal(glm::normalize(v1), glm::normalize(v2));
	}
	inline vec3 rotate_point(const vec3& point, float angle, const vec3& axis, const vec3& axis_point)
	{
		return (axis_point+glm::rotate(point-axis_point, angle, axis));
	}
	inline vector<vec3> paver_vector(const vec3& size, const vec3& position = vec3(0.0,0.0,0.0))
	{
		vector<vec3> points=
		{
			vec3(0.0, 0.0, 0.0)+position,
			vec3(1.0, 0.0, 0.0)+position,
			vec3(0.0, 1.0, 0.0)+position,
			vec3(0.0, 0.0, 1.0)+position,
			vec3(1.0, 1.0, 0.0)+position,
			vec3(1.0, 0.0, 1.0)+position,
			vec3(0.0, 1.0, 1.0)+position,
			vec3(1.0, 1.0, 1.0)+position
		};
		return points;
	}
};

#endif //_UTILS_HPP_
