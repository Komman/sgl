#include "solid_specific.hpp"
#include "utils.hpp"

using namespace glm;
using namespace std;
using namespace sgl;

#define SGL_SOLID_COPY_CONSTRUCTOR_AND_COPY(class_name) \
	class_name::class_name(const class_name& s)                   \
	{                                                             \
		*(this)=s;                                                \
	}                                                             \
	Solid* class_name::get_copy()  const                  \
	{                                                             \
		class_name* s=new class_name;                             \
		*s=*this;                                                 \
		return s;                                                 \
	}

#define CHECK_INIT_SOLID() if(hbx_ptr!=NULL || shape_ptr!=NULL)\
			err("Solid already initialized");

namespace sgl
{
	//=========== SPHERIC ===========

	void Spheric_solid::init(const vec3& center, float radius, const vec3& color)
	{
		CHECK_INIT_SOLID()

		Spheric_hitbox* shp = new Spheric_hitbox;
		shp->create(center, radius);
		this->hbx_ptr = shp;

		Sphere* sp = new Sphere;
		sp->init(center, radius, color);
		this->shape_ptr=sp;
	}
	const Spheric_solid& Spheric_solid::operator=(const Spheric_solid& s)
	{
		this->destroy();

		if(s.is_init())
		{
			Spheric_hitbox* shp = new Spheric_hitbox;
			(*shp)=*((Spheric_hitbox*)(s.hbx_ptr));
			this->hbx_ptr=shp;

			Sphere* sp = new Sphere;
			*(sp)=*((Sphere*)(s.shape_ptr));
			this->shape_ptr=sp;
		}
		return (*this);
	}
	
	SGL_SOLID_COPY_CONSTRUCTOR_AND_COPY(Spheric_solid)

	//=========== TRIANGLE ===========

	void Triangle_solid::init(const vector<vec3> points, const vec3& color)
	{
		CHECK_INIT_SOLID()

		Triangle_hitbox* shp = new Triangle_hitbox;
		shp->create(points);
		this->hbx_ptr = shp;

		Triangle* sp = new Triangle;
		sp->init(points.data(), color);
		this->shape_ptr=sp;
	}
	void Triangle_solid::init(const vec3& p0, const vec3& p1, const vec3& p2, const vec3& color)
	{
		vector<vec3> v={p0,p1,p2};
		this->init(v, color);
	}
	const Triangle_solid& Triangle_solid::operator=(const Triangle_solid& s)
	{
		this->destroy();

		if(s.is_init())
		{
			Triangle_hitbox* shp = new Triangle_hitbox;
			(*shp)=*((Triangle_hitbox*)(s.hbx_ptr));
			this->hbx_ptr=shp;

			Triangle* sp = new Triangle;
			*(sp)=*((Triangle*)(s.shape_ptr));
			this->shape_ptr=sp;
		}
		return (*this);
	}
	
	SGL_SOLID_COPY_CONSTRUCTOR_AND_COPY(Triangle_solid)

	//=========== HEXAHEDRON ===========

	void Hexahedron_solid::init(const vec3& position, const vec3& direction1, const vec3& direction2, const vec3& direction3, const vec3& color)
	{
		CHECK_INIT_SOLID()

		Hexahedron_hitbox* shp = new Hexahedron_hitbox; 

		vector<vec3> v={position,
						position+direction1,
						position+direction2,
						position+direction3,
						position+direction1+direction2,
						position+direction1+direction3,
						position+direction2+direction3,
						position+direction1+direction2+direction3
						};

		shp->create(v);
		this->hbx_ptr = shp;

		Hexahedron* sp = new Hexahedron;
		sp->init(position, direction1, direction2, direction3, color);
		this->shape_ptr=sp;
	}
	const Hexahedron_solid& Hexahedron_solid::operator=(const Hexahedron_solid& s)
	{
		this->destroy();

		if(s.is_init())
		{
			Hexahedron_hitbox* shp = new Hexahedron_hitbox;
			(*shp)=*((Hexahedron_hitbox*)(s.hbx_ptr));
			this->hbx_ptr=shp;

			Hexahedron* sp = new Hexahedron;
			*(sp)=*((Hexahedron*)(s.shape_ptr));
			this->shape_ptr=sp;
		}
		return (*this);
	}
	
	SGL_SOLID_COPY_CONSTRUCTOR_AND_COPY(Hexahedron_solid)

	//=========== CIRCLE ===========

	void Circle_solid::init(const vec3& center, const vec3& normal, float radius, const vec3& color)
	{
		CHECK_INIT_SOLID()

		Circle_hitbox* shp = new Circle_hitbox;
		shp->create(center, normal, radius);
		this->hbx_ptr = shp;

		Circle* sp = new Circle;
		sp->init(center, normal, radius, color);
		this->shape_ptr=sp;
	}

	const Circle_solid& Circle_solid::operator=(const Circle_solid& s)
	{
		this->destroy();

		if(s.is_init())
		{
			Circle_hitbox* shp = new Circle_hitbox;
			(*shp)=*((Circle_hitbox*)(s.hbx_ptr));
			this->hbx_ptr=shp;

			Circle* sp = new Circle;
			*(sp)=*((Circle*)(s.shape_ptr));
			this->shape_ptr=sp;
		}
		return (*this);
	}
	
	SGL_SOLID_COPY_CONSTRUCTOR_AND_COPY(Circle_solid)

	//=========== CYLINDER ===========

	void Cylinder_solid::init(const vec3& point, const vec3& sized_direction, float radius, const vec3& color)
	{
		CHECK_INIT_SOLID()

		Cylinder_hitbox* shp = new Cylinder_hitbox;
		shp->create(point, sized_direction, glm::length(sized_direction), radius);
		this->hbx_ptr = shp;

		Cylinder* sp = new Cylinder;
		sp->init(point, sized_direction, radius, color);
		this->shape_ptr=sp;
	}

	const Cylinder_solid& Cylinder_solid::operator=(const Cylinder_solid& s)
	{
		this->destroy();

		if(s.is_init())
		{
			Cylinder_hitbox* shp = new Cylinder_hitbox;
			(*shp)=*((Cylinder_hitbox*)(s.hbx_ptr));
			this->hbx_ptr=shp;

			Cylinder* sp = new Cylinder;
			*(sp)=*((Cylinder*)(s.shape_ptr));
			this->shape_ptr=sp;
		}
		return (*this);
	}
	
	SGL_SOLID_COPY_CONSTRUCTOR_AND_COPY(Cylinder_solid)

};