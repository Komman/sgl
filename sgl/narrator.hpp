#ifndef _NARRATOR_HPP_
#define _NARRATOR_HPP_

#include "object.hpp"
#include "camera.hpp"

using namespace std;
using namespace glm;
using namespace sgl;

#define CAMERA_CONTROL_JUMP CAMERA_CONTROL_UP

namespace sgl
{
	using NARRATOR_TYPE = Object;

	class Narrator: public NARRATOR_TYPE
	{
	public:
		Narrator();
		template<typename... TArgs> 
		Narrator(const vec3& start_position, TArgs... args,
			     const vec3& new_relative_camera_position = vec3(0.0,0.0,0.0),
			     CONTROLABLE_MODES cmode                  = CAMERA_UNDER_CONTROL)
			: NARRATOR_TYPE(start_position, args...)
		{
			init_camera(start_position+new_relative_camera_position);
			init_narrator(new_relative_camera_position, cmode);
		}

		void control_view();

		inline void  set_sensibility(float nsensibility){camera_sensibility=nsensibility;}
		inline float get_sensibility(){return camera_sensibility;}

		void set_control_key(CONTROLABLE_MODES key_code, int new_key);
		void reset_control_keys(const vector<int>& new_control_keys);
		int get_control_key(CONTROLABLE_MODES key_code);

		void disable_inside_camera();
		void enable_inside_camera();

		virtual ~Narrator();

	private:
		void init_narrator(const vec3& new_relative_camera_position, CONTROLABLE_MODES cmode);
		void init_camera(const vec3& initial_position);
		void set_view();

		bool control_enable;
		float speed;

		//camera
		bool inside_camera=true;
		vec3 relative_camera_position;
		CONTROLABLE_MODES control_type;
		vector<int> control_keys;
		float camera_sensibility;
		Camera cam;
	};
};

#endif //_NARRATOR_HPP_
