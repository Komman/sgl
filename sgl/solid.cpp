#include "solid.hpp"
#include "display.hpp"
#include "utils.hpp"

using namespace glm;
using namespace std;
using namespace sgl;

namespace sgl
{

	void Solid::tp(const vec3& pos)
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		hbx_ptr->tp(pos);
		shape_ptr->tp(pos);
	}
	void Solid::rotate(float angle, const vec3& axis, const vec3& point)
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		hbx_ptr->rotate(angle, axis, point);
		shape_ptr->rotate(angle, axis, point);
	}
	void Solid::scale_by_point(float ratio, const vec3& point)
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		hbx_ptr->scale_by_point(ratio, point);
		shape_ptr->scale_by_point(ratio, point);
	}
	void Solid::scale(float ratio)
	{
		this->scale_by_point(ratio, this->get_position());
	}

	void Solid::set_rotation(const vec3& up_vector, const vec3& front_vector)
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		hbx_ptr->set_rotation(up_vector, front_vector);
		shape_ptr->set_rotation(up_vector, front_vector);
	}
	//====== SHAPE SPECIFIC ======

	void Solid::draw() const
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		shape_ptr->draw();
	}
	void Solid::apply_texture(Image new_texture, vec2 fixed_size)
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		shape_ptr->apply_texture(new_texture, fixed_size);
	}
	void Solid::apply_color(const vec3& new_color)
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		shape_ptr->apply_color(new_color);
	}


	//====== HITBOX SPECIFIC =====


	void Solid::set_solid()
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		hbx_ptr->set_solid();
	}
	void Solid::unset_solid()
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		hbx_ptr->unset_solid();
	}
	bool Solid::is_solid() const
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		return hbx_ptr->is_solid();
	}

	vector<collision> Solid::collisions() const
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		return hbx_ptr->collisions();
	}
	bool Solid::is_in_collision() const
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		return hbx_ptr->is_in_collision();
	}

	collision Solid::collision_with(const Hitbox& h) const
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		return hbx_ptr->collision_with(h);
	}
	bool Solid::is_in_collision(const Hitbox& h) const
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		return hbx_ptr->is_in_collision(h);
	}

	collision Solid::collision_with(const Solid& h) const
	{
		#ifdef SGL_CHECK_ERRORS	
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		return hbx_ptr->collision_with(*(h.hbx_ptr));
	}
	bool Solid::is_in_collision(const Solid& h) const
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		return hbx_ptr->is_in_collision(*(h.hbx_ptr));
	}
	int Solid::hitbox_id_indice() const
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		return hbx_ptr->get_id_indice();
	}
	void Solid::destroy()
	{
		if(this->is_init())
		{		
			if(hbx_ptr!=NULL)
			{
				delete hbx_ptr;
				hbx_ptr=NULL;
			}
			if(shape_ptr!=NULL)
			{
				delete shape_ptr;
				shape_ptr=NULL;
			}
		}
	}

	void Solid::draw_borders(float thickness) const
	{
		#ifdef SGL_CHECK_ERRORS
		if(!is_init())
			err("Solid not initialized for " + string(__FUNCTION__) + "()");
		#endif

		hbx_ptr->draw_borders(thickness);
	}
};