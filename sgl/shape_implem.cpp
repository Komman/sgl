#include "shape.hpp"
#include "utils.hpp"
#include "display.hpp"
#include <glm/gtx/vector_angle.hpp>

using namespace std;
using namespace glm;
using namespace sgl;

namespace sgl
{

	void Shape::init_data(vec3 new_vertexs[], vec3 new_colors[])
	{
		for(int i=0;i<N_VERTICES;i++)
		{
			fill_vertex_vector(vertexs, new_vertexs[i]);
			fill_vertex_vector(colors , new_colors[i]);
		}
	}
	void Shape::init_data(vec3 new_vertexs[], const vec3& new_color)
	{
		this->fill_one_color(new_color);
		for(int i=0;i<N_VERTICES;i++)
		{
			fill_vertex_vector(vertexs, new_vertexs[i]);
		}
	}
	void Shape::init_data(GLfloat new_vertexs[], GLfloat new_colors[])
	{
		for(int i=0;i<N_VERTICES;i++)
		{
			fill_vertex_data(vertexs, new_vertexs+3*i);
			fill_vertex_data(colors , new_colors+3*i);
		}
	}
	void Shape::init_data(GLfloat new_vertexs[], const vec3& new_color)
	{
		this->fill_one_color(new_color);
		for(int i=0;i<N_VERTICES;i++)
		{
			fill_vertex_data(vertexs, new_vertexs+3*i);
		}
	}

	void Shape::add_circle(const vec3& pos, const vec3& normal, float radius, int n_points)
	{
		float dangle=2*M_PI/(float)(n_points);

		vec3 ini=normalize(glm::cross(normal, vec3(1.0,1.0,1.0)))*radius;

		for(int i=0;i<n_points;i++)
		{
			this->add_point(pos+glm::rotate(ini, dangle*(float)(i), normal));
		}
	}

	void Shape::link_circle(int begin_indice, int n_points, bool inv_normal)
	{
		for(int i=0;i<n_points-2;i++)
		{
			if(!inv_normal)
				ilinnk_triangle(ivec3(begin_indice,begin_indice+i+1,begin_indice+i+2));
			else
				ilinnk_triangle(ivec3(begin_indice,begin_indice+i+2,begin_indice+i+1));
		}
	}

	void Shape::compute_rotate(float angle, const vec3& axis, const vec3& point)
	{
		//TOOPT if possible
		vec3 cv;
		for(int i=0;i<(int)vertexs.data.size()/3;i++)
		{
			cv.x=vertexs.data[3*i];
			cv.y=vertexs.data[3*i+1];
			cv.z=vertexs.data[3*i+2];
			insert_vertex_vector(i, vertexs, rotate_point(cv, angle, axis, point));
		}

		this->post_rotate(angle, axis, point);

		update_potision();
	}
	void Shape::set_unidirectionnal_up_front(const vec3& direction)
	{
		vec3 axis_rot=VEC3_FRONT;
		this->up=glm::normalize(direction);
		if(vec3_equal(up, axis_rot))
		{
			axis_rot=VEC3_UP;
		}
		this->front=glm::normalize(glm::cross(up, axis_rot));
	} 
	void Shape::set_vertexed_up_front()
	{
		if(vertexs.data.size()<9)
		{
			err("Shape must have at least 3 vertexs");
		}

		vec3 v1=glm::normalize(this->get_vertex(1)-this->get_vertex(0));
		vec3 v2=glm::normalize(this->get_vertex(2)-this->get_vertex(0));
		float angle=glm::angle(v1, v2);
		if(eq_0(angle))
		{
			err("Bad vertex repatrition (aligned) (//TODO: take the goods vertexs)");
		}
		else
		{
			this->up=v1;
			this->front=get_sec_vec3_for_plan(v1, v2);
		}
	}
	void Shape::tp(const vec3& position)
	{
		//TOOPT if possible
		vec3 pi=this->get_position();
		vec3 cv;
		for(int i=0;i<(int)vertexs.data.size()/3;i++)
		{
			cv.x=vertexs.data[3*i];
			cv.y=vertexs.data[3*i+1];
			cv.z=vertexs.data[3*i+2];
			insert_vertex_vector(i, vertexs, position+cv-pi);
		}

		this->post_tp(position);
		update_potision();
	}

	void Shape::scale_by_point(float ratio, const vec3& point)
	{
		//TOOPT if possible
		vec3 cv;
		for(int i=0;i<(int)vertexs.data.size()/3;i++)
		{
			cv.x=vertexs.data[3*i];
			cv.y=vertexs.data[3*i+1];
			cv.z=vertexs.data[3*i+2];
			insert_vertex_vector(i, vertexs, point+(cv-point)*ratio);
		}

		this->post_scale_by_point(ratio, point);
		update_potision();
	}
	void Shape::scale(float ratio)
	{
		this->scale_by_point(ratio, this->get_position());
	}

	void Shape::set_face_on_plan(const ivec3& indices_face)
	{
		vec3 ipoint=get_vertex(indices_face[0]);
		if(fill_color_type==SHAPE_TEXTURE && is_texture_fixed_size)
		{
			set_draw_plan(ipoint,
							normalize(get_vertex(indices_face[1])-ipoint)*(float)(texture_fixed_size.x),
							normalize(get_vertex(indices_face[2])-ipoint)*(float)(texture_fixed_size.y));
		}
		else
		{
			set_draw_plan(ipoint,
						(get_vertex(indices_face[1])-ipoint),
						(get_vertex(indices_face[2])-ipoint));
		}
	}
	void Shape::apply_color(const vec3& new_color)
	{
		for(int i=0;i<N_VERTICES;i++)
		{
			insert_vertex_vector(i, colors , new_color);
		}
		update_color();
	}
	void Shape::apply_texture(Image new_texture, vec2 fixed_size)
	{
		fill_color_type=SHAPE_TEXTURE;
		texture=new_texture;
		if(fixed_size.x==0.0 && fixed_size.y==0.0)
		{
			is_texture_fixed_size=false;
		}
		else
		{
			is_texture_fixed_size=true;
			texture_fixed_size=fixed_size;
		}
	}

	void Shape::draw()
	{
		if(position_changed)
		{
			//TOOPT if possible
			update_buffer(this->vertexs);
			position_changed=false;
		}
		if(color_changed)
		{
			//TOOPT if possible
			update_buffer(this->colors);
			position_changed=false;
		}
		if(fill_color_type==SHAPE_COLOR)
		{
			set_color_only_drawing();
		}
		if(fill_color_type==SHAPE_TEXTURE)
		{
			set_drawing_texture(this->texture);
		}
		if(culling)
		{
			glEnable(GL_CULL_FACE);
		}
		else
		{
			glDisable(GL_CULL_FACE);
		}
		
		// premier tampon d'attributs : les sommets
		glEnableVertexAttribArray(0); 
		glEnableVertexAttribArray(1); 

		glBindBuffer(GL_ARRAY_BUFFER, this->vertexs.buffer); 
		glVertexAttribPointer( 
		   0,                  // attribut 0. Aucune raison particulière pour 0, mais cela doit correspondre au « layout » dans le shader 
		   3,                  // taille
		   GL_FLOAT,           // type 
		   GL_FALSE,           // normalisé ? 
		   0,                  // nombre d'octets séparant deux sommets dans le tampon
		   (void*)0            // décalage du tableau de tampon
		); 	
		glBindBuffer(GL_ARRAY_BUFFER, this->colors.buffer);
		glVertexAttribPointer( 
		    1,                                // attribut. Aucune raison particulière pour 1, mais cela doit correspondre au « layout » du shader 
		    3,                                // taille 
		    GL_FLOAT,                         // type 
		    GL_FALSE,                         // normalisé ? 
		    0,                                // nombre d'octets séparant deux sommets dans le tampon 
		    (void*)0                          // décalage du tableau de tampons 
		);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices.buffer); 
		
		this->drawing_function();
		 
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(0);
	}

	void Shape::drawing_function()
	{
		//TOOPT: d'abord calculer les points initiaux et directions, puis les bind et tout draw d'un coup
		int i=0;
		do
		{
			set_face_on_plan(ivec3(indices.data[3*i], indices.data[3*i+1], indices.data[3*i+2]));
			
			glDrawElements(GL_TRIANGLES,
						   N_INDICES_TRIANGLE,
						   GL_UNSIGNED_INT,
						   (void*)(i*N_INDICES_TRIANGLE*sizeof(unsigned int))
						   );
			
			i++;	
		}
		while((unsigned int)(i)<indices.data.size()/N_INDICES_TRIANGLE);
	}
};
