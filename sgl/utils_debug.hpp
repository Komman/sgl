#ifndef _UTILS_DEBUG_HPP_
#define _UTILS_DEBUG_HPP_

#include <stdio.h> 
#include <stdlib.h>
#include <iostream>     
#include <fstream> 
#include <vector>

using namespace std;

//#define SGL_COLLISINO_AGLO_PRINT_CYCLES

template<typename T>
static void print_vector(std::vector<T> v)
{
	for(T x:v)
	{
		cout<<x<<" ";
	}	
}

static void print_vec3(const vec3& v)
{
	cout<<"("<<v.x<<", "<<v.y<<", "<<v.z<<")";
}

#ifdef SGL_COLLISINO_AGLO_PRINT_CYCLES
static inline uint64_t get_cycles()
{
  uint64_t t;
  __asm volatile ("rdtsc" : "=A"(t));
  return t;
}
#endif

#endif //_UTILS_DEBUG_HPP_
