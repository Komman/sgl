#include "narrator.hpp"

using namespace std;
using namespace glm;
using namespace sgl;

static const CONTROLABLE_MODES camera_cmode_for_narrator = CAMERA_ONLY_ROTATION;

namespace sgl
{
	//====== INIT =======
	Narrator::Narrator() :  NARRATOR_TYPE()
	{

	}
	void Narrator::init_narrator(const vec3& new_relative_camera_position, CONTROLABLE_MODES cmode)
	{
		relative_camera_position = new_relative_camera_position;
		control_type=cmode;

		speed=10.0;
		control_enable=true;

		control_keys.resize(CAMERA_NUMBER_CONTROLS);

		control_keys[CAMERA_CONTROL_RIGHT] = SGL_KEY(D);
		control_keys[CAMERA_CONTROL_LEFT]  = SGL_KEY(Q);
		control_keys[CAMERA_CONTROL_FRONT] = SGL_KEY(Z);
		control_keys[CAMERA_CONTROL_BACK]  = SGL_KEY(S);
		control_keys[CAMERA_CONTROL_UP]    = SGL_KEY(SPACE);
		control_keys[CAMERA_CONTROL_DOWN]  = SGL_KEY(LEFT_SHIFT);
	}
	void Narrator::init_camera(const vec3& initial_position)
	{
		cam=Camera(initial_position, camera_cmode_for_narrator);
		enable_inside_camera();
		camera_sensibility=3.0;
	}

	//====== CONTROL ======

	void Narrator::set_control_key(CONTROLABLE_MODES key_code, int new_key)
	{
		if(key_code>=control_keys.size())
			err("Narrator::set_control_key: Not enougth key set");

		control_keys[key_code]=new_key;
	}

	void Narrator::reset_control_keys(const vector<int>& new_control_keys)
	{
		control_keys=new_control_keys;
	}
	int Narrator::get_control_key(CONTROLABLE_MODES key_code)
	{
		if(key_code>=control_keys.size())
			err("Narrator::get_control_key: Not enougth key set");

		return control_keys[key_code];
	}

	void Narrator::control_view()
	{
		//TODO
		
		cam.update_view();

		if(inside_camera)
		{
			if(cam.get_control_mode())
			this->set_view();
		}
	}
	void Narrator::set_view()
	{
		cam.tp(this->get_position()+relative_camera_position);
	}


	void Narrator::disable_inside_camera()
	{
		cam.set_control_mode(CAMERA_CREATIVE);
		inside_camera=false;
	}
	void Narrator::enable_inside_camera()
	{
		cam.set_control_mode(camera_cmode_for_narrator);
		inside_camera=true;
	}

	Narrator::~Narrator() 
	{

	}
};