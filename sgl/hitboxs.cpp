#include "hitboxs.hpp"
#include "orientation.hpp"
#include <glm/gtx/vector_angle.hpp>

#define SGL_CREATE_FUNCTION(HITBOX_TYPE, add_function ,...)\
	if(type!=HITBOX_TYPE)\
	{\
		err(string(__func__)  + "(" + #__VA_ARGS__ + ") most be called by a " + #HITBOX_TYPE +\
		 "\n(here used with hitbox_type:" + to_string(type) + ")");\
	}\
	add_function ( __VA_ARGS__ );\
	created=true;\
	this->install();

#define SGL_STRING_HITBOX_TYPE(HITBOX_TYPE) #HITBOX_TYPE

using namespace std;
using namespace glm;
using namespace sgl;


namespace sgl
{
	void Hitbox::check_not_created()
	{
		if(id_indice!=-1)
		{
			err("Hitbox already created (id: " + to_string(this->id()) + ")");
		}
	}
	void Hitbox::check_created()
	{
		if(id_indice==-1)
		{
			err("Hitbox not created");
		}
	}


	//======== CREATE ========

	void Hitbox::create(const vec3& p1, const vec3& p2, const vec3& p3)
	{
		SGL_CREATE_FUNCTION(SGL_HITBOX_TRIANGLE, add_triangle, 
			p1, p2, p3)
	}

	void Hitbox::create(const vec3& center, float radius)
	{	
		SGL_CREATE_FUNCTION(SGL_HITBOX_SPHERICAL, add_sphere,
			center, radius)
	}

	void Hitbox::create(const vector<vec3>& points)
	{
		if(type!=SGL_HITBOX_TRIANGLE && type!=SGL_HITBOX_HEXAHEDRON)
		{
			err(string(__func__)  + " most be called by a SGL_HITBOX_TRIANGLE or a SGL_HITBOX_HEXAHEDRON\n(here called with hitbox_type:"
			 + to_string(type) + ")");
		}
		if(type==SGL_HITBOX_TRIANGLE)
		{
			add_triangle ( points );
		}
		if(type==SGL_HITBOX_HEXAHEDRON)
		{
			add_hexahedron ( points );
		}
		created=true;
		this->install();
	}

	void Hitbox::create(const vec3& begin_center, const vec3& direction, float height, float radius)
	{
		SGL_CREATE_FUNCTION(SGL_HITBOX_CYLINDER, add_cylinder,
			begin_center, direction, height, radius)
	}

	void Hitbox::create(const vec3& center, const vec3& normal, float radius)
	{
		SGL_CREATE_FUNCTION(SGL_HITBOX_CIRCLE, add_circle,
			center, normal, radius)
	}

	const Hitbox& Hitbox::operator=(const Hitbox& h)
	{
		this->destroy(); this->build_copy(h); return *this;
	}

	void Hitbox::move(const vec3& dp)
	{
		this->tp(this->get_position()+dp);
	}
	void Hitbox::tp(const vec3& position)
	{
		this->compute_tp(position);
		this->update();
	}
	void Hitbox::set_rotation(const vec3& up_vector, const vec3& front_vector)
	{
		this->compute_set_rotation(up_vector, front_vector);
		this->update();
	}
	void Hitbox::rotate(float angle, const vec3& axis, const vec3& point)
	{
		this->compute_rotate(angle, axis, point);
		this->update();
	}
	void Hitbox::scale_by_point(float ratio, const vec3& point)
	{
		this->compute_scale_by_point(ratio, point);
		this->update();
	}
	void Hitbox::scale(float ratio)
	{
		this->scale_by_point(ratio, this->get_position());
	}
	void Hitbox::compute_vertexed_rotation(const vec3& up_vector, const vec3& front_vector)
	{
		if(!is_orthogonal(up_vector, front_vector))
		{
			err("up_vector and front_vector must be orthogonal");
		}

		vec3 v1=glm::normalize(this->points()[1]-this->points()[0]);
		vec3 v2=glm::normalize(this->points()[2]-this->points()[0]);
		
		float angle=glm::angle(v1, v2);
		if(eq_0(angle))
		{
			err("Bad vertex repatrition (aligned) (//TODO: take the goods vertexs)");
		}

		float dangle1=glm::angle(v1, up_vector);
		if(!eq_0(dangle1))
		{
			vec3 axis1=glm::normalize(glm::cross(v1, up_vector));
			this->rotate(dangle1, axis1, points()[0]);
		}

		v1=glm::normalize(this->points()[1]-this->points()[0]);
		v2=glm::normalize(this->points()[2]-this->points()[0]);
		v2=get_sec_vec3_for_plan(v1, v2);
		float dangle2=glm::angle(v2, front_vector);
		if(!eq_0(dangle2))
		{
			vec3 axis2=glm::normalize(glm::cross(v2, front_vector));
			this->rotate(dangle2, axis2, points()[0]);
		}

		
	}

	//======= SOLID =======

	void Hitbox::set_solid()
	{
		set_hitbox_solid(id());
	}
	void Hitbox::unset_solid()
	{
		unset_hitbox_solid(id());
	}
	bool Hitbox::is_solid() const
	{
		return is_hitbox_solid(id());
	}

	//===== DRAW BORDER ====

	void Hitbox::draw_borders(float thickness)
	{
		draw_border(this->get_min(),this->get_max(), thickness);
	}

};