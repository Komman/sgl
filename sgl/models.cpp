#include "models.hpp"

using namespace sgl;
using namespace sgl_dev;


namespace sgl
{
	static vector<string> split_string(const string& s, char separator=' ')
	{
		vector<string> vs(0);
		string word="";
		char c;
		for(unsigned int i=0;i<s.size();i++)
		{
			c=s[i];
			if(c==separator)
			{
				vs.push_back(word);
				word="";
			}
			else
			{
				word.push_back(c);
			}	
		}
		vs.push_back(word);

		return vs;
	}

	static void invalid_line_file(const string& file, const vector<string>& line, unsigned int n)
	{
		if(line.size()!=n)
		{
			string ac="";
			for(const string& ss : line)
			{
				ac+=ss+" ";
			}
			err("Invalid line in obj " + file +": " +to_string(n)+" args expected and "+to_string(line.size())+" given:\n" +ac );
		}
	}
	static void invalid_line_obj(const vector<string>& line, unsigned int n)
	{
		invalid_line_file("obj", line, n);
	}
	static void invalid_line_mtl(const vector<string>& line, unsigned int n)
	{
		invalid_line_file("mtl", line, n);
	}
	static vec3 str_to_vec3(const vector<string>& line, unsigned int offset=0)
	{
		return vec3(std::stof(line[offset+0]),
					std::stof(line[offset+1]),
					std::stof(line[offset+2])
			);
	}
	static uvec2 str_to_uvec2(const vector<string>& line, unsigned int offset=0)
	{
		return uvec2(std::stoul(line[offset+0]),
					 std::stoul(line[offset+1])
			);
	}

	static uvec3 parse_face(const string& face)
	{
		vector<string> sf=split_string(face, '/');

		if(sf.size()!=3)
			err("wrong face in obj");

		return uvec3(std::stoi(sf[0]),
					 std::stoi(sf[1]),
					 std::stoi(sf[2])
			);
	}

	void Model::init(const char* path_and_name)
	{
		const string obj_ext=".obj";
		const string mtl_ext=".mtl";

		string s(path_and_name);

		ifstream obj(s+obj_ext);
		ifstream mtl(s+mtl_ext);
		if (!obj.is_open())
  		{
  			err("Impossible to open "+s+obj_ext);
  		}
		if (!mtl.is_open())
  		{
  			err("Impossible to open "+s+mtl_ext);
  		}

  		string line;
  		while (getline(mtl,line))
	    {
	    	build_line_mtn(split_string(line));
	    }
  		while (getline(obj,line))
	    {
	    	build_line_obj(split_string(line));
	    }


	    mtl.close();
	    obj.close();


	    if(vertexs.data.size()!=colors.size())
	    {	
	    	string s1="-Number of vertexs: " + to_string(vertexs.data.size())+"\n";
	    	string s2="-Number of colors : " + to_string(colors.size())+"\n";
	    	err("No colors to all vertexs:\n"+s1+s2);
	    }

	    set_limits();
	}

	void Model::build_line_obj(const vector<string>& line)
	{
		const string& type=line[0];

		if(type=="v")
		{
			invalid_line_obj(line, 4);
			vertexs.data.push_back(str_to_vec3(line,1));
		}
		if(type=="vn")
		{
			invalid_line_obj(line, 4);
			normals.data.push_back(str_to_vec3(line,1));
		}
		if(type=="vt")
		{
			invalid_line_obj(line, 3);
			uv_coord.data.push_back(str_to_uvec2(line,1));
		}
		if(type=="f")
		{
			int narg=4;
			invalid_line_obj(line, narg);

			colors.resize(vertexs.data.size());

			uvec3 vc;
			for(int i=1;i<narg;i++)
			{
				vc=parse_face(line[i]);
				unsigned int& i_vertex=vc.x;
				unsigned int& i_uv=vc.y;
				unsigned int& i_normal=vc.z;

				vertexs.indices.push_back(i_vertex-1);
				uv_coord.indices.push_back(i_uv-1);
				normals.indices.push_back(i_normal-1);

				if(i_vertex-1>=colors.size())
					err("no valid color indice in vertexs : " + to_string(i_vertex)+"/"+to_string(colors.size()-1));

				colors[i_vertex-1]=current_vertex_color;
			}
		}
		if(type == "usemtl")
		{
			set_vertex_color(line[1]);
		}
	}

	void Model::build_line_mtn(const vector<string>& line)
	{
		const string& type=line[0];

		if(type=="newmtl")
		{
			invalid_line_mtl(line, 2);
			current_mat_dec=line[1];
		}
		if(type=="Kd")
		{
			invalid_line_mtl(line, 4);
			invalid_current_mat();

			mats.names.push_back(current_mat_dec);
			mats.colors.push_back(str_to_vec3(line,1));
		}

		mats.check();
	}

};	