#ifndef _ERR_HPP_
#define _ERR_HPP_

#include <string>
#include <iostream>

//#define SUPER_CHECK
#define SGL_CHECK_ERRORS

using namespace std;

namespace sgl
{
	inline void err(const string& msg)
	{
		cout<<endl<<"\033[1;31mSGL ERROR: \033[0m\033[1;33m" + msg + "\033[0m"<<endl<<endl;
		exit(-1);
	}

	inline void SGL_war(const string& msg)
	{
		cout<<"\033[1;35mSGL WARNING: \033[0m\033[1;33m"<<msg<<"\033[0m"<<endl;;
	}

	inline void sc_err(int cond, const string& msg)
	{
		#ifdef SUPER_CHECK
		if(cond)
		{
			cout<<"\033[1;31mSGL SUPER_CHECK ERROR: \033[0m\033[1;33m" + msg + "\033[0m"<<endl;endl;
			exit(-1);
		}
		#endif
	}

	inline void check(int cond, const string& msg)
	{
		if(!cond)
			err(msg);
	}

	inline void qcheck(int cond)
	{
		check(cond, "\033[1;36mQuick check failed\033[0m");
	}

	template <typename T> inline void see(T v)
	{
		cout<<"\033[1;36m"<<v<<"\033[0m ";
	}

	template <typename T> inline void seer(T v) {see(v);cout<<endl;}

	inline void seeri()
	{
		static int i=0;
		seer(i);
		i++;
	}
};

#endif //_ERR_HPP_
