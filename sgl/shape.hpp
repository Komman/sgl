#ifndef _SHAPE_HPP_
#define _SHAPE_HPP_

#include <stdio.h> 
#include <stdlib.h>
#include <iostream>     
#include <fstream> 
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp> 
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/projection.hpp>

#include "err.hpp"
#include "image.hpp"
#include "colors.hpp"
#include "models.hpp"
#include "lights.hpp"
#include "utils.hpp"
#include "orientation.hpp"

using namespace glm;
using namespace std;
using namespace sgl;

#define N_VERTEX_TRIANGLE 3
#define N_VERTEX_RECTANGLE (N_VERTEX_TRIANGLE+1)
#define N_VERTEX_SQUARE (N_VERTEX_RECTANGLE*6)

#define N_INDICES_TRIANGLE 3
#define N_INDICES_RECTANGLE (N_INDICES_TRIANGLE*2)
#define N_INDICES_SQUARE (N_INDICES_RECTANGLE*6)

#define SGL_SHAPE_VIRTUAL_GET_COPY(solid_class_name)\
	SGL_VIRTUAL_GET_COPY(Shape, solid_class_name)

namespace sgl
{

	enum SHAPE_FILL_TYPES {SHAPE_COLOR=0, SHAPE_TEXTURE};

	template<typename T=GLfloat> struct drawing_buffer
	{
		vector<T> data;
		GLuint buffer;

		void reset()
		{
			data.resize(0);
		}
	};

	class Shape : public Orientation
	{
	public:
		Shape(){};
		Shape(const Shape& s)
		{
			copy_shape_members(s);
			init_all_buffers();
		};
		const Shape& operator=(const Shape& s)
		{	
			copy_shape_members(s);
			init_all_buffers();
			return *this;
		}
		template<typename... TArgs> Shape(TArgs... args, int n_vertices)
		{

			this->init(args..., n_vertices);
		}

		template<typename... TArgs> void init(TArgs... args, int n_vertices)
		{
			init_drawing_buffers(n_vertices);
			this->init_data(args...);
			init_data_buffer();
		}		

		void init_data(vec3 new_vertexs[], vec3 new_colors[]);
		void init_data(vec3 new_vertexs[], const vec3& new_color = WHITE);
		void init_data(GLfloat new_vertexs[], GLfloat new_colors[]);
		void init_data(GLfloat new_vertexs[], const vec3& new_color = WHITE);
		
		void apply_texture(Image new_texture, vec2 fixed_size=vec2(0.0,0.0));
		void apply_color(const vec3& new_color);
		void set_face_on_plan(const ivec3& indices_face);
		//TODO: éclairage avec culling désactivé (deux faces visibles)
		inline void set_double_face(){culling=false;}
		inline void set_one_face(){culling=true;}

		void draw();

		void tp(const vec3& position);
		void scale(float ratio);
		void scale_by_point(float ratio, const vec3& point);

		virtual inline vec3 get_position() const {
			#ifdef SGL_CHECK_ERRORS
			if(indices.data.size()==0)
				err("no indices in shape");
			#endif

			return get_vertex(0);
		}
		
		inline void move(const vec3& dv)
		{
			//TOOPT but avoid abstraction
			this->tp(this->get_position()+dv);
		}

		virtual ~Shape(){};

		SGL_SHAPE_VIRTUAL_GET_COPY(Shape)
	protected:
		virtual void drawing_function();
		virtual void post_tp(const vec3& position){}
		virtual void post_rotate(float angle, const vec3& axis, const vec3& axis_point){}
		virtual void post_scale_by_point(float ratio, const vec3& point){}
		virtual void compute_rotate(float angle, const vec3& axis, const vec3& axis_point);

		void set_unidirectionnal_up_front(const vec3& direction);
		void set_vertexed_up_front();

		#define SGL_COPY_MEMBER(member)  this->member = s.member;
		void copy_shape_members(const Shape& s)
		{	
			SGL_COPY_MEMBER(vertexs.data)
			SGL_COPY_MEMBER(indices.data)
			SGL_COPY_MEMBER(colors.data)

			SGL_COPY_MEMBER(fill_color_type)
			SGL_COPY_MEMBER(position_changed)
			SGL_COPY_MEMBER(color_changed)
			SGL_COPY_MEMBER(texture)
			SGL_COPY_MEMBER(texture_fixed_size)
			SGL_COPY_MEMBER(is_texture_fixed_size)
			SGL_COPY_MEMBER(culling)

			SGL_COPY_MEMBER(N_VERTICES)
		}
		void init_drawing_buffers(int n_vertices)
		{
			N_VERTICES=n_vertices;
			colors.data.resize(0);
			vertexs.data.resize(0);
		}
		void init_all_buffers()
		{
			this->init_buffer(vertexs);
			this->init_buffer(colors);
			this->init_indices_buffer();
		}
		void init_data_buffer()
		{
			if((int)vertexs.data.size()!=N_VERTICES*3)
			{
				err("Unexpected vertex count: "+to_string(vertexs.data.size()/3) +" (expected: "+to_string(N_VERTICES)+" )");
			}
			
			this->init_indices_default();
			init_all_buffers();

			fill_color_type=SHAPE_COLOR;
			position_changed=false;
			color_changed=false;
		}
		void update_indices_buffer()
		{
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices.buffer); 
			//TOOPT: GL_STATIC_DRAW or GL_DYNAMIC_DRAW
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices.data[0])*indices.data.size(), indices.data.data(), GL_DYNAMIC_DRAW);
		}
		template<typename T>
		void gen_buffer(drawing_buffer<T>& db)
		{
			glGenBuffers(1, &(db.buffer)); 
		}
		void init_indices_buffer()
		{
			gen_buffer(indices);
			update_indices_buffer();
		}
		virtual void init_indices_default()
		{
			indices.data.resize(N_VERTICES);

			for(int i=0;i<N_VERTICES;i++)
			{
				indices.data[i]=i;
			}
		}
		void add_circle(const vec3& pos, const vec3& normal, float radius, int n_points);
		void link_circle(int begin_indice, int n_points, bool inv_normal=false);
		inline void update_potision()
		{
			position_changed=true;
		}
		inline void update_color()
		{
			color_changed=true;
		}
		void update_buffer(drawing_buffer<>& db)
		{
			glBindBuffer(GL_ARRAY_BUFFER, db.buffer); 
			//TOOPT: GL_STATIC_DRAW or GL_DYNAMIC_DRAW
			glBufferData(GL_ARRAY_BUFFER, sizeof(db.data[0])*db.data.size(), db.data.data(), GL_DYNAMIC_DRAW);
		}
		template<typename T>
		void init_buffer(drawing_buffer<T>& db)
		{
			gen_buffer(db);
			update_buffer(db);
		}
		inline void fill_vertex_vector(drawing_buffer<>& db, const vec3& vect)
		{
			db.data.push_back(vect.x);
			db.data.push_back(vect.y);
			db.data.push_back(vect.z);
		}
		inline void insert_vertex_vector(int indice, drawing_buffer<>& db, const vec3& vect)
		{
			if(indice>(int)db.data.size())
				err("insert vertex vector out of bounds");

			db.data[indice*3+0]=vect.x;
			db.data[indice*3+1]=vect.y;
			db.data[indice*3+2]=vect.z;
		}

		inline void fill_vertex_data(drawing_buffer<>& db, GLfloat vect_data[3])
		{
			db.data.push_back(vect_data[0]);
			db.data.push_back(vect_data[1]);
			db.data.push_back(vect_data[2]);
		}
		inline void add_point(const vec3& point)
		{
			fill_vertex_vector(this->vertexs, point);
		}
		inline void fill_triangle(const vec3 points[])
		{
			this->fill_vertex_vector(this->vertexs, points[0]);
			this->fill_vertex_vector(this->vertexs, points[1]);
			this->fill_vertex_vector(this->vertexs, points[2]);
		}
		inline void fill_triangle(const vec3& p1, const vec3& p2, const vec3& p3)
		{
			vec3 ppp[]={p1,p2,p3};
			fill_triangle(ppp);
		}
		inline void resize_buffer(int n_points, drawing_buffer<>& b)
		{
			b.data.resize(3*n_points);
		}
		inline void resize_vertexs(int n_points)
		{
			resize_buffer(n_points, this->vertexs);
		}
		inline void resize_colors(int n_points)
		{
			resize_buffer(n_points, this->colors);
		}
		inline void ilinnk_triangle(const ivec3& triangle_vertices)
		{
			indices.data.push_back(triangle_vertices.x);
			indices.data.push_back(triangle_vertices.y);
			indices.data.push_back(triangle_vertices.z);
		}
		inline void fill_one_color(const vec3& color)
		{
			for(int i=0;i<N_VERTICES;i++)
			{
				fill_vertex_vector(this->colors, color);
			}
		}
		inline vec3 get_vertex(const int& indice) const
		{
			#ifdef SUPER_CHECK
			if((unsigned int)(3*indice+2)>vertexs.data.size())
			{
				SGL_war("vertex out of bounds: " + to_string(3*indice+2) + " on " + to_string(vertexs.data.size()));
			}
			#endif
			return vec3(vertexs.data[3*indice],vertexs.data[3*indice+1],vertexs.data[3*indice+2]);
		} 
		inline vec3 get_indexed_vertex(int indice)
		{
			return get_vertex(indices.data[ indice ]);
		}

		drawing_buffer<> vertexs;
		drawing_buffer<unsigned int> indices;
		drawing_buffer<> colors;


		SHAPE_FILL_TYPES fill_color_type;
		bool position_changed=true;
		bool color_changed=true;
		Image texture;
		vec2 texture_fixed_size;
		bool is_texture_fixed_size;
		bool culling = true;

		int N_VERTICES;
	};

	#include "shape_specific.hpp"
	
	
	using Square     = Paver;
	using Sphere     = vSphere;
	using Cylinder   = vCylinder;
	using Circle     = vCircle;	

};


#endif //_SHAPE_HPP_
