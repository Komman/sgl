#ifndef _IMAGE_HPP_
#define _IMAGE_HPP_

#include <stdio.h> 
#include <stdlib.h>  
#include <fstream> 
#include <iostream>
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp> 
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/projection.hpp>

#include "err.hpp"

using namespace std;
using namespace glm;
using namespace sgl;

namespace sgl
{	
	struct Image
	{
		glm::uvec2 size;
		GLuint textureID;
		unsigned char * data;

		void destroy()
		{
			delete [] data;
		}

		void load_BMP(const char * imagepath)
		{
			unsigned char header[54];
			unsigned int dataPos; 
			FILE * file = fopen(imagepath,"rb"); 
			if (!file)
			{
				err("Image could not be opened\n");
				return;
			}

			if ( fread(header, 1, 54, file)!=54 )
			{ // S'il n'est pas possible de lire 54 octets : problème
			    err("Not a correct BMP file\n"); 
			    return; 
			}

			if ( header[0]!='B' || header[1]!='M' ){ 
			    err("Not a correct BMP file\n"); 
			    return; 

			}
			dataPos    = *(int*)&(header[0x0A]); 
			unsigned int imageSize  = *(int*)&(header[0x22]); 
			size.x      = *(int*)&(header[0x12]); 
			size.y     = *(int*)&(header[0x16]);

			// Certains fichiers BMP sont mal formés, on devine les informations manquantes
			if (imageSize==0)
			{
				imageSize=size.x*size.y*3; // 3 : un octet pour chaque composante rouge, vert et bleu
			}
			if (dataPos==0)
			{
				dataPos=54; // l'en-tête BMP est fait de cette façon
			}

			data = new unsigned char [imageSize]; 
	 
			// Lit les données à partir du fichier pour les mettre dans le tampon
			fread((void*)data,1,imageSize,file); 
			 
			// Tout est en mémoire maintenant, le fichier peut être fermé

			fclose(file);

			glGenTextures(1, &(textureID)); 

			glBindTexture(GL_TEXTURE_2D, textureID); 
			glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, this->size.x, this->size.y, 0, GL_BGR, GL_UNSIGNED_BYTE, this->data); 
			
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			
			glGenerateMipmap(GL_TEXTURE_2D);
		}
	};

};

#endif //_IMAGE_HPP_
