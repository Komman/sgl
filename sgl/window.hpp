#ifndef _WINDOW_HPP_
#define _WINDOW_HPP_


#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "err.hpp"
#include "default.hpp"

#include <stdio.h> 
#include <stdlib.h>
#include <iostream>

#include <glm/glm.hpp> 

using namespace glm;
using namespace std;
using namespace sgl;

#define SGL_KEY(K) GLFW_KEY_ ## K
#define SGL_ESCAPE_KEY SGL_KEY(F4)
#define SGL_PAUSE_KEY SGL_KEY(ESCAPE)

#define SGL_PRINT_FPS 1
#define SGL_ENABLE_CULLING 1
#define SGL_ENABLE_BLEND false

namespace sgl
{
	struct uint_xy
	{
		uint x;
		uint y;
	};

	void init_window(uint size_x=DF_WINDOW_X, uint size_y=DF_WINDOW_Y, const char* name=DF_WINDOW_NAME);
	void close_window();
	void flip();
	inline void clear(){glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);}
	void exit_window();
	bool quit();
	void get_events();
	float delta_time();
	void set_mouse_visibility(bool visible);
	int is_mouse_in_game();
	void mouse_quit_game();
	void mouse_enter_game();
	bool is_fps_loop();

	uint_xy window_size();
}

#endif //_WINDOW_HPP_
