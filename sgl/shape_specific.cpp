#include "shape.hpp"
#include "utils.hpp"
#include "colors.hpp"
#include <glm/gtx/vector_angle.hpp>

using namespace std;
using namespace glm;
using namespace sgl;

namespace sgl
{
	//======== FACED SHAPE ========

	void FacedShape::drawing_function()
	{
		
		//TOOPT: d'abord calculer les points initiaux et directions, puis les bind et tout draw d'un coup
		int i=0;
		do
		{
			//SGL_war("prep with i*N_VERTEX_RECTANGLE = " + to_string(i*N_VERTEX_RECTANGLE));
			Shape::set_face_on_plan(ivec3(this->indices.data[N_INDICES_RECTANGLE*i], this->indices.data[N_INDICES_RECTANGLE*i+1], this->indices.data[N_INDICES_RECTANGLE*i+2]));

			//SGL_war("finished");
			glDrawElements(GL_TRIANGLES,
						   N_INDICES_RECTANGLE,
						   GL_UNSIGNED_INT,
						   (void*)(i*N_INDICES_RECTANGLE*sizeof(unsigned int))
						   );
			
			i++;
		}
		while((unsigned int)(i)<(this->indices.data.size())/N_INDICES_RECTANGLE);
	}

	void FacedShape::init_indices_default()
	{
		if(N_VERTICES%(N_VERTEX_RECTANGLE))
		{
			err("Invalid vertex number for a FaceShaped Shape");
		}

		this->indices.data.resize(N_VERTICES/N_VERTEX_RECTANGLE*N_INDICES_RECTANGLE);

		for(int i=0;i<N_VERTICES/N_VERTEX_RECTANGLE;i++)
		{
			this->indices.data[N_INDICES_RECTANGLE*i]   = N_VERTEX_RECTANGLE*i;
			this->indices.data[N_INDICES_RECTANGLE*i+1] = N_VERTEX_RECTANGLE*i+1;
			this->indices.data[N_INDICES_RECTANGLE*i+2] = N_VERTEX_RECTANGLE*i+2;
			
			this->indices.data[N_INDICES_RECTANGLE*i+3] = N_VERTEX_RECTANGLE*i+2;
			this->indices.data[N_INDICES_RECTANGLE*i+4] = N_VERTEX_RECTANGLE*i+1;
			this->indices.data[N_INDICES_RECTANGLE*i+5] = N_VERTEX_RECTANGLE*i+3;
		}
	}


	void FacedShape::fill_rectangle(const vec3& position, const vec3& direction1, const vec3& direction2)
	{
		this->fill_vertex_vector(this->vertexs, position);
		this->fill_vertex_vector(this->vertexs, position+direction1);
		this->fill_vertex_vector(this->vertexs, position+direction2);
		this->fill_vertex_vector(this->vertexs, position+direction1+direction2);
		
	}

	//======== RECTANGLE ========

	void Rectangle::init_data(const vec3& position, const vec3& direction1, const vec3& direction2, const vec3& new_color)
	{
		this->fill_one_color(new_color);

		fill_rectangle(position, direction1, direction2);
	}



	//======== HEXAHEDRON ========


	void Hexahedron::init_data(const vec3& position, const vec3& direction1, const vec3& direction2, const vec3& direction3, const vec3& new_color)
	{
		this->fill_one_color(new_color);

		vec3 oppose=position+direction1+direction2+direction3;

		fill_rectangle(position, direction3, direction2);
		fill_rectangle(position+direction1, -direction1, direction2);
		fill_rectangle(position, direction1, direction3);	

		fill_rectangle(position+direction3, direction1, direction2);
		fill_rectangle(oppose, -direction3, -direction1);
		fill_rectangle(oppose-direction2, -direction3, direction2);
	}

	//======== PAVER ========


	void Paver::init_data(const vec3& position, const vec3& size, const vec3& new_color)
	{
		this->Hexahedron::init(position,
				   vec3(1.0,0.0,0.0)*size.x,
				   vec3(0.0,1.0,0.0)*size.y,
				   vec3(0.0,0.0,1.0)*size.z,
				   new_color);
	}	
	void Paver::init_data(const vec3& position, const vec3& size)
	{
		this->init_data(position, size, WHITE);
	}
	void Paver::transform(const vec3& new_position, const vec3& new_size, const vec3& new_color)
	{
		colors.reset();
		vertexs.reset();
		this->Hexahedron::init_data(new_position,
				   vec3(1.0,0.0,0.0)*new_size.x,
				   vec3(0.0,1.0,0.0)*new_size.y,
				   vec3(0.0,0.0,1.0)*new_size.z,
				   new_color);
		update_color();
		update_potision();
	}

	//======== SPHERE ========

	void vSphere::init_data(const vec3& position, float radius, const vec3& color)
	{
		this->fill_color_type=SHAPE_COLOR;
		this->fill_one_color(color);

		vec3 xrad=vec3(radius,    0.0, 0.0   );
		vec3 yrad=vec3(0.0   , radius, 0.0   );	

		int n=TESS_FACTOR;

		float dangle=M_PI/(float)(n-1);

		this->iadd_extrm(position, yrad);
		int nn=2;

		for(int i=0;i<n;i++)
		{
			float phi = (float)(i)*dangle*2.0;
			vec3 rot_axis=glm::rotate(xrad, phi, yrad);

			for(int j=1;j<n-1;j++)
			{
				float theta = (float)(j)*dangle;
				vec3 rax = glm::rotate(yrad, theta, rot_axis);
				nn++;
				this->add_point(position + rax);
			}
		}
	}
	void vSphere::init_indices_default()
	{
		this->indices.reset();

		int n=TESS_FACTOR;


		for(int i=0;i<n-1;i++)
		{
			for(int j=0; j<n-1; j++)
			{
				this->ilinnk_triangle(ivec3(sphere_point(j  , i  ),
											sphere_point(j+1, i  ),
											sphere_point(j+1, i+1)));

				this->ilinnk_triangle(ivec3(sphere_point(j  , i+1),
											sphere_point(j  , i  ),
											sphere_point(j+1, i+1)));
			}
		}
	}


	int vSphere::sphere_point(int thetha_indice, int phi_indice)
	{
		if(thetha_indice==0)
			return 0;
		if(thetha_indice==TESS_FACTOR-1)
			return 1;
		int ret=2+(TESS_FACTOR-2)*phi_indice+(thetha_indice-1);
		return ret;
	}
	void vSphere::iadd_extrm(const vec3& center, const vec3& extrm_direction)
	{
		this->add_point(center+extrm_direction);
		this->add_point(center-extrm_direction);
	}


	//======= CYLINDER ========
	
	void vCylinder::init_data(const vec3& point, const vec3& direction, float radius, const vec3& new_color)
	{
		this->fill_color_type=SHAPE_COLOR;
		this->fill_one_color(new_color);

		_begin=point;
		this->add_circle(point, direction, radius, TESS_FACTOR);
		this->add_circle(point+direction, direction, radius, TESS_FACTOR);

		this->set_unidirectionnal_up_front(direction);
	}

	void vCylinder::init_indices_default()
	{
		this->link_circle(0, TESS_FACTOR, true);
		this->link_circle(TESS_FACTOR, TESS_FACTOR);

		for(int i=0;i<TESS_FACTOR-1;i++)
		{
			this->ilinnk_triangle(ivec3(i  , i+1+TESS_FACTOR, i+TESS_FACTOR));
			this->ilinnk_triangle(ivec3(i+1, i+1+TESS_FACTOR, i));
		}
		this->ilinnk_triangle(ivec3(TESS_FACTOR-1  , TESS_FACTOR, 2*TESS_FACTOR-1));
		this->ilinnk_triangle(ivec3(0, TESS_FACTOR, TESS_FACTOR-1));
	}

	void vCylinder::post_tp(const vec3& position)
	{
		_begin=position;
	}

	void vCylinder::post_rotate(float angle, const vec3& axis, const vec3& axis_point)
	{
		_begin=rotate_point(_begin, angle, axis, axis_point);
	}
	void vCylinder::post_scale_by_point(float ratio, const vec3& point)
	{
		_begin=point+(_begin-point)*ratio;
	}


	//======= CIRCLE ========

	void vCircle::init_data(const vec3& point, const vec3& normal, float radius, const vec3& new_color)
	{
		this->fill_color_type=SHAPE_COLOR;
		this->fill_one_color(new_color);

		_center=point;
		this->add_circle(point, normal, radius, TESS_FACTOR);
	
		this->set_unidirectionnal_up_front(normal);
	}

	void vCircle::init_indices_default()
	{
		this->link_circle(0, TESS_FACTOR);
	}
	void vCircle::post_tp(const vec3& position)
	{
		_center=position;
	}
	void vCircle::post_rotate(float angle, const vec3& axis, const vec3& axis_point)
	{
		_center=rotate_point(_center, angle, axis, axis_point);
	}
	void vCircle::post_scale_by_point(float ratio, const vec3& point)
	{
		_center=point+(_center-point)*ratio;
	}

	//======= SHAPE_MODEL ========


	void Shape_model::init(const Model& m, const vec3& pos)
	{
		this->init_drawing_buffers(m.vertexs.data.size());

		begin=pos;
		size=m.size;

		this->vertexs.data.resize(0);
		for(const vec3& v : m.vertexs.data)
		{
			this->vertexs.data.push_back(pos.x + v.x -m.position.x);
			this->vertexs.data.push_back(pos.y + v.y -m.position.y);
			this->vertexs.data.push_back(pos.z + v.z -m.position.z);
		}
		this->indices.data=m.vertexs.indices;
		#ifdef SGL_CHECK_ERRORS
		for(const unsigned int& i : m.vertexs.indices)
		{
			if(i>=(unsigned int)(N_VERTICES) || i<0)
			{
				string s="Indice: " + to_string(i) + " , N_VERTICES: " + to_string(N_VERTICES);
				err("Vertex not well indiced:\n"+s);
			}
		}
		#endif

		this->colors.data.resize(0);
		for(const vec3& v : m.colors)
		{
			this->colors.data.push_back(v.x);
			this->colors.data.push_back(v.y);
			this->colors.data.push_back(v.z);
		}

		if(colors.data.size()!=vertexs.data.size())
			err("colors.size()!=vertexs.data.size() : "+to_string(colors.data.size())+" != "+to_string(vertexs.data.size()));

		init_data_buffer();
	}

	void Shape_model::post_tp(const vec3& position)
	{
		begin=position;
	}
	void Shape_model::post_rotate(float angle, const vec3& axis, const vec3& axis_point)
	{
		size=glm::rotate(size, angle, axis);
	}
	void Shape_model::post_scale_by_point(float ratio, const vec3& point)
	{
		size=size*ratio;
		begin=point+(begin-point)*ratio;
	}

};