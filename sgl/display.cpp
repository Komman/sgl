#include "display.hpp"
#include "lights.hpp"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <vector>


using namespace glm;
using namespace std;
using namespace sgl;

namespace sgl
{
	static Display disp;

	static GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path)
	{ 
 
	    // Crée les shaders 
	    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER); 
	    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER); 
	 
	    // Lit le code du vertex shader à partir du fichier
	    std::string VertexShaderCode; 
	    std::ifstream VertexShaderStream(vertex_file_path, std::ios::in); 
	    if(VertexShaderStream.is_open()) 
	    { 
	        std::string Line = ""; 
	        while(getline(VertexShaderStream, Line)) 
	            VertexShaderCode += "\n" + Line; 
	        VertexShaderStream.close(); 
	    } 
	 
	    // Lit le code du fragment shader à partir du fichier
	    std::string FragmentShaderCode; 
	    std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in); 
	    if(FragmentShaderStream.is_open()){ 
	        std::string Line = ""; 
	        while(getline(FragmentShaderStream, Line)) 
	            FragmentShaderCode += "\n" + Line; 
	        FragmentShaderStream.close(); 
	    } 
	 
	    GLint Result = GL_FALSE; 
	    int InfoLogLength; 
	 
	    // Compile le vertex shader 
	    if(SGL_PRINT_SHADERS_LOAD) printf("Compiling shader : %s\n", vertex_file_path); 
	    char const * VertexSourcePointer = VertexShaderCode.c_str(); 
	    glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL); 
	    glCompileShader(VertexShaderID); 
	 
	    // Vérifie le vertex shader 
	    glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result); 
	    glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength); 
	    std::vector<char> VertexShaderErrorMessage(InfoLogLength); 
	    glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]); 
	    if(SGL_PRINT_SHADERS_LOAD) fprintf(stdout, "%s\n", &VertexShaderErrorMessage[0]); 
	 
	    // Compile le fragment shader 
	    if(SGL_PRINT_SHADERS_LOAD) printf("Compiling shader : %s\n", fragment_file_path); 
	    char const * FragmentSourcePointer = FragmentShaderCode.c_str(); 
	    glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL); 
	    glCompileShader(FragmentShaderID); 
	 	
	    // Vérifie le fragment shader 
	    glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result); 
	    glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength); 
	    std::vector<char> FragmentShaderErrorMessage(InfoLogLength); 
	    glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]); 
	    if(SGL_PRINT_SHADERS_LOAD) fprintf(stdout, "%s\n", &FragmentShaderErrorMessage[0]); 
	 
	    // Lit le programme
	    if(SGL_PRINT_SHADERS_LOAD) fprintf(stdout, "Linking program\n"); 
	    GLuint ProgramID = glCreateProgram(); 
	    glAttachShader(ProgramID, VertexShaderID); 
	    glAttachShader(ProgramID, FragmentShaderID); 
	    glLinkProgram(ProgramID); 
	 
	    // Vérifie le programme
	    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result); 
	    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength); 
	    std::vector<char> ProgramErrorMessage( std::max(InfoLogLength, int(1)) ); 
	    glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]); 
	    if(SGL_PRINT_SHADERS_LOAD) fprintf(stdout, "%s\n", &ProgramErrorMessage[0]); 
	 
	    glDeleteShader(VertexShaderID); 
	    glDeleteShader(FragmentShaderID); 
	 
	    return ProgramID; 
	}

	Shader load_shaders(string name)
	{
		string begfile="sgl/shaders/";
		string vertex_extension=".vert";
		string fragment_extension=".frag";
		return LoadShaders((begfile + name + vertex_extension).data(), (begfile + name+ fragment_extension).data());
	}
	void use_shader(Shader s)
	{
		disp.change_shader(s);
	}

	Shader get_current_shader()
	{
		return disp.get_shader();
	}

	void init_display()
	{	
		set_clear_color(BLACK);

		glGenVertexArrays(1, &(disp.VertexArrayID));
		glBindVertexArray((disp.VertexArrayID));

		glEnable(GL_DEPTH_TEST); 
		glDepthFunc(GL_LESS);

		Shader shad = load_shaders("shader");
		disp.init(shad);
	}

	void set_color_only_drawing()
	{
		glUniform1i(disp.draw_type_ID, DRAW_TYPE_COLOR_ONLY);
	}
	void set_drawing_texture(const Image& im)
	{		
		glUniform1i(disp.draw_type_ID, DRAW_TYPE_TEXTURE);
		
		glUniform1i(disp.texture_ID, 0);

		glBindTexture(GL_TEXTURE_2D, im.textureID);
	}
	void set_draw_plan(const vec3& point_i, const vec3& d1, const vec3& d2)
	{
		vec3 plan[]={
			point_i,
			d1,
			d2
		};
		glUniform3fv(disp.plan_ID, 3, (float*)(plan));
	}

	void submit_lights(light_info* tl, int* l_types, int n)
	{
		if(tl!=NULL && l_types!=NULL)
		{
			glUniform3fv(disp.lights_ID,
						 n*sizeof(light_info)/sizeof(float)/3,
						 (GLfloat*)(tl));
			glUniform1iv(disp.lights_types_ID,
						 n,
						 l_types);
				
		}
		glUniform1i(disp.light_count_ID, n);
	}

	//CLASS DISPLAY
	void Display::change_shader(Shader new_shader)
	{
		if(shaders_initialized)
		{
			err("No shaders initialized");
		}
		else
		{
			current_shader=new_shader;
			glUseProgram(current_shader);
		}
	}

	#define GET_SHADER(shader_name) shader_name ## _ID = glGetUniformLocation(get_current_shader(), #shader_name);

	void Display::init(Shader initial_shader)
	{
		use_shader(initial_shader);
		shaders_initialized=true;
		texture_ID=glGetUniformLocation(get_current_shader(), "texture2D"); 
		
		GET_SHADER(plan)
		GET_SHADER(draw_type)
		GET_SHADER(lights)
		GET_SHADER(lights_types)
		GET_SHADER(light_count)
		GET_SHADER(n_params_light_info)
		GET_SHADER(camera_position)

		//TO faire attention quand on change de shaders, s'il faut ou non recharger les uniforms
		glUniform1i(n_params_light_info_ID, sizeof(light_info)/sizeof(float));
		submit_lights(NULL, NULL, 0);

		set_color_only_drawing();
	}

	void update_camera_position(const vec3& pos)
	{
		float T[3]={pos.x, pos.y, pos.z};
		glUniform3fv(disp.camera_position_ID, 3*sizeof(float), T);
	}

};