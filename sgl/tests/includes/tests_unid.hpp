
bool unid_private_creation()
{
	unid_private i;
	i=2;
	return (i.id()==2);
}
bool unid_private_instance()
{
	unid_private i(5);
	return (i.id()==5);
}
bool unid_private_copy_constructor()
{
	unid_private i(7);
	unid_private i2(i);
	i=9;
	return (i==9 && i2==9);
}
bool unid_creation()
{
	unid_private i(2);
	unid ii=i;
	return (ii.id()==2);
}
bool unid_diffusion()
{
	unid_private i(2);
	unid i1=i;
	unid i2=i;
	unid i3=i1;
	return (i.id() == i1.id() && i1.id()==i2.id() && i2.id()==i3.id());
}
bool unid_change_diffusion()
{
	unid_private i(2);
	unid i1=i;
	i=8;
	unid i2=i;
	unid i3=i1;
	return (i.id() == i1.id() && i1.id()==i2.id() && i2.id()==i3.id() && i.id()==8);
}
bool unid_many_diffusions()
{
	unid_private ip1;
	unid_private ip2;

	ip1=5;
	ip2=9;

	unid i1;
	unid i2;

	i1=ip1;
	i2=ip2;

	return (i1==5 && i2==9);
}