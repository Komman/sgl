int max_circle=50;
vector<Circle_hitbox> v(max_circle);
Group group_g1;
Group group_g2;

bool group_creation()
{
	for(int i=0;i<max_circle;i++)
	{
		v[i].create(vec3(0.0,0.0,0.0), vec3(0.0,1.0,0.0), 3.0);
		if(!v[i].is_created())
		{
			return false;
		}
	}

	for(unsigned int i=0;i<maximum_hitbox_numer();i++)
	{
		if(group_g1.contains(i))
			return false;
		if(group_g2.contains(i))
			return false;
	}

	return true;
}

bool group_add_one_element()
{
	group_g1.add(4);
	return group_g1.contains(4);
}

bool group_dont_contains_one_element()
{
	group_g1.add(4);
	return !(group_g1.contains(8));
}

bool group_del_one_element()
{
	group_g1.del(4);
	return !(group_g1.contains(4));
}

bool group_add_many_element()
{
	group_g1.add(4);
	group_g1.add(5);
	group_g1.add(8);
	group_g1.add(9);
	return (group_g1.contains(4) && group_g1.contains(5) && group_g1.contains(8) && group_g1.contains(9));
}
bool group_del_many_element()
{
	group_g1.add(4);
	group_g1.del(5);
	group_g1.add(8);
	group_g1.del(9);
	return (group_g1.contains(4) && (!group_g1.contains(5)) && group_g1.contains(8) && (!group_g1.contains(9)));
}
bool group_del_all_element()
{
	group_g1.del(4);
	group_g1.del(5);
	group_g1.del(8);
	group_g1.del(9);
	return !(group_g1.contains(4) && group_g1.contains(5) && group_g1.contains(8) && group_g1.contains(9));
}
bool group_check_all_element_destroyed()
{
	for(unsigned int i=0;i<maximum_hitbox_numer();i++)
	{
		if(group_g1.contains(i))
			return false;
		if(group_g2.contains(i))
			return false;
	}
	return true;
}

bool group_destruction()
{
	for(int i=0;i<max_circle;i++)
	{
		v[i].destroy();
		if(v[i].is_created())
		{
			return false;
		}
	}
	for(unsigned int i=0;i<maximum_hitbox_numer();i++)
	{
		group_g1.del(i);
		group_g2.del(i);
	}

	for(unsigned int i=0;i<maximum_hitbox_numer();i++)
	{
		if(group_g1.contains(i))
			return false;
		if(group_g2.contains(i))
			return false;
	}

	group_g1.destroy();
	group_g2.destroy();
		
	return true;
}