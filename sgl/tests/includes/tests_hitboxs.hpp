static Spheric_hitbox hitboxs_s1;
static Spheric_hitbox hitboxs_s2;
static Triangle_hitbox hitboxs_t1;
static vector<Circle_hitbox> hitboxs_vc1;
static const unsigned int ncircles=50;

bool hitbox_creation()
{
	hitboxs_vc1.resize(ncircles);
	for(Circle_hitbox& ch1 : hitboxs_vc1)
	{
		ch1.create(vec3(100.0,120.0,110.0), vec3(1.0,2.0,3.0), 5.0);
		if(!ch1.is_created())
			return false;
	}
	hitboxs_s1.create(vec3(2.0,3.0,5.0), 4.0);
	hitboxs_s2.create(vec3(-6.0,3.0,5.0), 3.0);
	hitboxs_t1.create(vec3(-10.0,-10.0,-10.0),  vec3(-8.0,-9.0,-11.0), vec3(-8.0,-12.0,7.0));
	
	return (hitboxs_s1.is_created() && hitboxs_s2.is_created() && hitboxs_t1.is_created());
}
bool no_collisions_beetwen_shperes_no_move()
{
	return (!hitboxs_s1.is_in_collision(hitboxs_s2));
}
bool no_collisions_beetwen_shperes_first_move()
{
	hitboxs_s1.tp(hitboxs_s2.get_position()-vec3(0.0,7.1,0.0));
	return (!hitboxs_s1.is_in_collision(hitboxs_s2));
}
bool collisions_beetwen_shperes_first_move()
{
	hitboxs_s1.move(vec3(0.0,0.2,0.0));
	return (hitboxs_s1.is_in_collision(hitboxs_s2));
}
bool is_in_collisions_with_id()
{
	hitboxs_s1.move(vec3(0.0,0.2,0.0));
	return (hitboxs_s1.is_in_collision(hitboxs_s2.get_id_indice()));
}
bool no_collisions_shpere_triangle_first_move()
{
	return (!hitboxs_s1.is_in_collision(hitboxs_t1));
}
bool decollision_beetwen_shperes()
{
	hitboxs_s1.move(vec3(0.0,14.0,0.0));
	return (!hitboxs_s1.is_in_collision(hitboxs_s2));
}
bool many_collisions_beetwen_shpere_and_triangles()
{
	hitboxs_s1.tp(hitboxs_vc1[0].get_position());
	return (hitboxs_s1.collisions().size()==ncircles);
}
bool many_same_collisions()
{
	return (hitboxs_vc1[0].collisions().size()==ncircles);
}
bool hitbox_unsolidity()
{
	hitboxs_s1.unset_solid();
	return (!hitboxs_s1.is_solid());
}
bool hitbox_collision_unsolidity()
{	
	return (hitboxs_vc1[0].collisions().size()==ncircles-1);
}
bool hitbox_resolidity()
{
	hitboxs_s1.set_solid();
	return (hitboxs_s1.collisions().size()==ncircles);
}
bool collision_after_many_collisions()
{
	hitboxs_s1.tp(hitboxs_s2.get_position());
	return (hitboxs_s1.collisions().size()==1 && hitboxs_s1.is_in_collision(hitboxs_s2));
}
bool hitbox_prescale()
{
	hitboxs_s1.move(vec3(0.0,8,0.0));
	return (hitboxs_s2.collisions().size()==0);
}
bool hitbox_scale()
{
	hitboxs_s2.scale(1.5);
	return (hitboxs_s2.collisions().size()==1 && hitboxs_s2.is_in_collision(hitboxs_s1));
}
bool hitbox_scale_hexahedron()
{
	Hexahedron_hitbox h1(paver_vector(vec3(1.0,1.0,1.0), vec3(-100.0, 100.0, 100.0)));;
	Hexahedron_hitbox h2(paver_vector(vec3(1.0,1.0,1.0), vec3(-100.0, 102.0, 100.0)));;
	h1.scale(2.1);
	bool b=h1.is_in_collision(h2);
	h1.destroy();
	h2.destroy();
	return b;
}
bool hitbox_no_scale_hexahedron()
{
	Hexahedron_hitbox h1(paver_vector(vec3(1.0,1.0,1.0), vec3(-100.0, 100.0, 100.0)));
	Hexahedron_hitbox h2(paver_vector(vec3(1.0,1.0,1.0), vec3(-100.0, 102.0, 100.0)));
	h1.scale(1.9);
	bool b=h1.is_in_collision(h2);
	h1.destroy();
	h2.destroy();
	return !b;
}
static int react_var=0;
static void react_fun(const collision&)
{
	react_var++;
}
bool hitbox_prereactive_collision()
{
	hitboxs_s1.move(vec3(0.0,8,0.0));
	return (!hitboxs_s2.is_in_collision());
}
bool hitbox_reactive_collision()
{
	hitboxs_s2.set_reactive_collision(&react_fun);
	hitboxs_s1.tp(hitboxs_s2.get_position());
	return (react_var==1);
}
bool hitbox_unset_reactive_collision()
{
	hitboxs_s2.unset_reactive_collision();
	hitboxs_s1.move(vec3(0.0,20,0.0));
	hitboxs_s1.tp(hitboxs_s2.get_position());
	return (react_var==1);
}
bool hitbox_get_copy()
{
	Cylinder_hitbox cyhr(vec3(3.0,4.0,5.0), vec3(2.0,3.0,1.0), 5.0, 1.5);
	Cylinder_hitbox* cyh=(Cylinder_hitbox*)cyhr.get_copy();
	bool cond=(cyh->get_radius()==cyhr.get_radius()
			&& cyh->get_begin()==cyhr.get_begin()
			&& cyh->get_sized_direction()==cyhr.get_sized_direction());
	delete cyh;
	return cond;
}
bool hitbox_destruction()
{
	hitboxs_s1.destroy();
	hitboxs_s2.destroy();
	hitboxs_t1.destroy();
	for(Circle_hitbox& ch1 : hitboxs_vc1)
	{
		ch1.destroy();
		if(ch1.is_created())
			return false;
	}
	return (!hitboxs_s1.is_created() && !hitboxs_s2.is_created() && !hitboxs_t1.is_created());
}

Spheric_hitbox hitbox_sh1;
Spheric_hitbox hitbox_sh2;
bool hitbox_operator_eq_sphere()
{
	hitbox_sh1.create(vec3(0.0,0.0,0.0), 1.0);

	if(hitbox_sh1.is_in_collision())
		return false;

	hitbox_sh2=(hitbox_sh1);

	return hitbox_sh1.is_in_collision();
}
bool hitbox_operator_eq_sphere_not_same_id()
{
	return (hitbox_sh1.get_id_indice() != hitbox_sh2.get_id_indice());
}
bool hitbox_operator_eq_sphere_independent_move()
{
	hitbox_sh2.move(vec3(0.0,2.1,0.0));

	return (!hitbox_sh2.is_in_collision());
}
bool hitbox_operator_eq_sphere_destroy()
{
	hitbox_sh2.destroy();
	hitbox_sh1.destroy();
	return true;
}
bool hitbox_maximum_hitbox_number_destroyed()
{
	return (maximum_hitbox_numer()==0);
}