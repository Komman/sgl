
Triangle_hitbox object_t1;

Object object_o1(vec3(0.0,0.2,0.0));
unid object_id1;
unid object_id2;
unid object_id3;
unid object_id4;

bool object_creation()
{
	object_t1.create(vec3(-10.0,0.0,-5.0),vec3(10.0,0.0,-5.0), vec3(0.0,0.0,5.0));

	return !object_o1.is_in_collision();
}

bool object_counts0()
{
	return (object_o1.count_hitboxs()==0
		 && object_o1.count_solids() ==0
		 && object_o1.count_shapes() ==0
		 && object_o1.count_objects() ==0
		 && object_o1.count_lights() ==0
		 );
}

bool object_add_solids()
{
	Spheric_solid s1(vec3(0.0,1.0,0.0), 1.1);
	Spheric_solid s2(vec3(0.0,5.0,0.0), 1.0);

	object_id1=object_o1.add(s1);
	object_id2=object_o1.add(s2);

	s1.destroy();
	s2.destroy();

	return (!object_o1.is_in_collision() && object_o1.count_solids()==2);
}
bool object_detsroyed_colllision()
{
	Spheric_solid s1(vec3(0.0,1.0,0.0), 1.1);
	s1.destroy();
	return (!object_o1.is_in_collision() && object_o1.count_solids()==2);
}
bool object_put_in_solids()
{
	Spheric_solid s1(vec3(0.0,1.0,0.0), 1.1);
	Spheric_solid s2(vec3(0.0,5.0,0.0), 1.0);

	object_id3=object_o1.put_in(s1);
	object_id4=object_o1.put_in(s2);

	return (!object_o1.is_in_collision() && object_o1.count_solids()==4);
}

bool object_is_in_all_collisions_solids()
{
	return (object_o1.is_in_all_collision());
}

bool object_is_in_collision()
{
	object_o1.move(vec3(0.0,-0.2,0.0));
	return object_o1.is_in_collision();
}
bool object_collision_with_hitbox()
{
	return (object_o1.collision_with(object_t1).size()==2 
		&& object_o1.collision_with(object_t1)[0].id_indice==object_t1.get_id_indice()
		&& object_o1.collision_with(object_t1)[1].id_indice==object_t1.get_id_indice()
		);
}
bool object_not_collision_with_hitbox()
{
	object_o1.move(vec3(0.0,0.2,0.0));
	return (object_o1.collision_with(object_t1).size()==0);
}
bool object_select_solid()
{
	object_o1.move(vec3(0.0,-0.2,0.0));

	return (object_o1.select_solid(object_id1).is_in_collision(object_t1) && !object_o1.select_solid(object_id2).is_in_collision(object_t1));
}
bool object_copy_constructor()
{
	object_o1.move(vec3(0.0,0.2,0.0));
	Object o2(object_o1);
	o2.move(vec3(2.1,0.0,0.0));
	return (o2.collision_with(object_o1).size()==4);
}
bool object_copy_constructor_all_collisions()
{
	Object o2(object_o1);
	o2.move(vec3(1.9,0.0,0.0));
	return (o2.collision_with(object_o1).size()==8);
}
bool object_copy_constructor_no_collisions()
{
	Object o2(object_o1);
	o2.move(vec3(5.0,0.0,0.0));
	return (o2.collision_with(object_o1).size()==0);
}
bool object_operator_eq()
{
	Object o2(vec3(20.0,30.0,40.0));
	o2=object_o1;
	o2.move(vec3(2.1,0.0,0.0));
	return (o2.collision_with(object_o1).size()==4);
}
bool object_operator_eq_all_collisions()
{
	Object o2(vec3(20.0,30.0,40.0));
	o2=object_o1;
	o2.move(vec3(1.9,0.0,0.0));
	return (o2.collision_with(object_o1).size()==8);
}
bool object_operator_eq_no_collisions()
{
	Object o2(vec3(20.0,30.0,40.0));
	o2=object_o1;
	o2.move(vec3(5.0,0.0,0.0));
	return (o2.collision_with(object_o1).size()==0);
}
bool object_operator_eq_after_init()
{
	Object o2(vec3(20.0,30.0,40.0));
	o2.add(Triangle_solid(vec3(1.0,1.0,1.0),vec3(-1.0,1.0,-1.0),vec3(10.0,-1.0,2.0)));
	Spheric_hitbox sht;
	sht.create(vec3(0.0,0.0,0.0), 5.0);
	o2.add(sht);
	o2=object_o1;
	return (o2.collision_with(object_o1).size()==8);
}
bool object_check_hitboxs_total_count()
{
	return (hitboxs_total_count()==5);
}
bool object_collision_with_solid()
{
	Circle_solid s1(vec3(0.0, 1.0, 0.0), vec3(1.0,0.0,0.0), 3.0);
	return object_o1.is_in_collision(s1);
}
bool object_destroy_solid()
{
	object_o1.move(-vec3(0.0,0.2,0.0));
	object_o1.destroy_solid(object_id1);
	object_o1.destroy_solid(object_id3);
	return  (object_o1.count_solids() ==2 && !object_o1.is_in_collision());
}
Spheric_solid* object_sptr;
bool object_pop_solid()
{
	object_o1.move(-vec3(0.0,4.1,0.0));
	object_sptr=(Spheric_solid*)object_o1.pop_solid(object_id2);
	return  (object_o1.count_solids() ==1 && object_o1.is_in_collision(*object_sptr));
}
bool object_pop_solid_recup()
{
	bool ret=object_sptr->is_in_collision(object_t1) && object_sptr->collisions().size()==2;
	delete object_sptr;
	return  ret;
}
bool object_destroy()
{
	object_o1.destroy();
	bool no_objs=(object_o1.count_hitboxs()==0
		 && object_o1.count_solids() ==0
		 && object_o1.count_shapes() ==0
		 && object_o1.count_objects() ==0
		 && object_o1.count_lights() ==0
		 );
	return (!object_o1.is_in_collision() && no_objs);
}