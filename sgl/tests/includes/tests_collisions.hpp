#define NO_FUNC(func) no_ ## func

#define TEST_COLLISION_V(test_func)\
	TEST(test_func)\
	TEST( NO_FUNC(test_func) )

#define TEST_COLLISION(t1, t2)\
	TEST_COLLISION_V(collisions_##t1##_##t2)


bool collisions_sphere_sphere()
{
	Spheric_hitbox s1;
	Spheric_hitbox s2;
	s1.create(vec3(0.0,0.0,0.0), sqrt(3.0)/2.0);
	s2.create(vec3(1.0,1.0,1.0), sqrt(3.0)/2.0+0.1);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return r;
}
bool no_collisions_sphere_sphere()
{
	Spheric_hitbox s1;
	Spheric_hitbox s2;
	s1.create(vec3(0.0,0.0,0.0), sqrt(3.0)/2.0);
	s2.create(vec3(1.0,1.0,1.0), sqrt(3.0)/2.0-0.1);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return !r;
}
bool collisions_sphere_triangle()
{
	Spheric_hitbox s1;
	Triangle_hitbox s2;
	s1.create(vec3(0.5,1.0,0.0), sqrt(3.0)/2.0+0.1);
	s2.create(vec3(0.0,0.0,0.0), vec3(1.0,0.0,0.0), vec3(0.5,1.0,1.0));
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return r;
}
bool no_collisions_sphere_triangle()
{
	Spheric_hitbox s1;
	Triangle_hitbox s2;
	s1.create(vec3(0.5,1.0,0.0), sqrt(3.0)/2.0-0.1);
	s2.create(vec3(0.0,0.0,0.0), vec3(1.0,0.0,0.0), vec3(0.5,1.0,1.0));
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return !r;
}

bool collisions_sphere_hexahedron()
{
	Spheric_hitbox s1;
	Hexahedron_hitbox s2;
	s1.create(vec3(-0.5,1.5,-0.5), sqrt(3.0)/2.0+0.1);
	s2.create(paver_vector(vec3(1.0,1.0,1.0)));
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return r;
}
bool no_collisions_sphere_hexahedron()
{
	Spheric_hitbox s1;
	Hexahedron_hitbox s2;
	s1.create(vec3(-0.5,1.5,-0.5), sqrt(3.0)/2.0-0.1);
	s2.create(paver_vector(vec3(1.0,1.0,1.0)));
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return !r;
}

bool collisions_sphere_cylinder()
{
	Spheric_hitbox s1;
	Cylinder_hitbox s2;
	s1.create(vec3(-1.0, 1.0, 0.0), 1.0+0.1);
	s2.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), 5.0, sqrt(2.0)/2.0);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return r;
}
bool no_collisions_sphere_cylinder()
{
	Spheric_hitbox s1;
	Cylinder_hitbox s2;
	s1.create(vec3(0.0, 0.0, 0.0), sqrt(2.0)/2.0-0.1);
	s2.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), 5.0, sqrt(2.0)/2.0);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return !r;
}

bool collisions_sphere_circle()
{
	Spheric_hitbox s1;
	Circle_hitbox s2;
	s1.create(vec3(-1.0, 1.0, 0.0), 1.0+0.1);
	s2.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), sqrt(2.0)/2.0);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return r;
}
bool no_collisions_sphere_circle()
{
	Spheric_hitbox s1;
	Circle_hitbox s2;
	s1.create(vec3(0.0, 0.0, 0.0), sqrt(2.0)/2.0-0.1);
	s2.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), sqrt(2.0)/2.0);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return !r;
}

bool collisions_triangle_hexahedron()
{
	Triangle_hitbox s1;
	Hexahedron_hitbox s2;
	s1.create(vec3(0.0,-5.0,1.0), vec3(0.0, -5.0, -1.0), vec3(0.1, 4.0, 0.0));
	s2.create(paver_vector(vec3(1.0,1.0,1.0)));
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return r;
}
bool no_collisions_triangle_hexahedron()
{
	Triangle_hitbox s1;
	Hexahedron_hitbox s2;
	s1.create(vec3(0.01,-5.0,1.0), vec3(0.01, -5.0, -1.0), vec3(-0.1, 4.0, 0.0));
	s2.create(paver_vector(vec3(1.0,1.0,1.0)));
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return !r;
}


bool collisions_triangle_cylinder()
{
	Triangle_hitbox s1;
	Cylinder_hitbox s2;
	s1.create(vec3(0.0, 0.0, -1.0), vec3(0.0, 0.0, 1.0), vec3(0.5, 0.5, 0.0)+vec3(0.1,0.1,0.0));
	s2.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), 5.0, sqrt(2.0)/2.0);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return r;
}
bool no_collisions_triangle_cylinder()
{
	Triangle_hitbox s1;
	Cylinder_hitbox s2;
	s1.create(vec3(0.0, 0.0, -1.0), vec3(0.0, 0.0, 1.0), vec3(0.5, 0.5, 0.0)-vec3(0.1,0.1,0.0));
	s2.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), 5.0, sqrt(2.0)/2.0);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return !r;
}

bool collisions_triangle_circle()
{
	Triangle_hitbox s1;
	Circle_hitbox s2;
	s1.create(vec3(0.0, 0.0, -1.0), vec3(0.0, 0.0, 1.0), vec3(0.5, 0.5, 0.0)+vec3(0.1,0.1,0.0));
	s2.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), sqrt(2.0)/2.0);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return r;
}
bool no_collisions_triangle_circle()
{
	Triangle_hitbox s1;
	Circle_hitbox s2;
	s1.create(vec3(0.0, 0.0, -1.0), vec3(0.0, 0.0, 1.0), vec3(0.5, 0.5, 0.0)-vec3(0.1,0.1,0.0));
	s2.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), sqrt(2.0)/2.0);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return !r;
}

bool collisions_hexahedron_hexahedron()
{
	Hexahedron_hitbox s1;
	Hexahedron_hitbox s2;
	s1.create(paver_vector(vec3(1.0,1.0,1.0)));
	s2.create(paver_vector(vec3(1.0,1.0,1.0), vec3(-1.0, -1.0, -1.0)+vec3(0.0, 0.0, 0.1)));
	s1.rotate(M_PI/4.0, vec3(0.0, 1.0, 0.0), vec3(0.0, 0.0, 0.0));
	s2.rotate(M_PI/4.0, vec3(0.0, 1.0, 0.0), vec3(0.0, 0.0, 0.0));
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return r;
}
bool no_collisions_hexahedron_hexahedron()
{
	Hexahedron_hitbox s1;
	Hexahedron_hitbox s2;
	s1.create(paver_vector(vec3(1.0,1.0,1.0)));
	s2.create(paver_vector(vec3(1.0,1.0,1.0), vec3(-1.0, 0.0, 0.0)-vec3(0.1, 0.0, 0.1)));
	s1.rotate(M_PI/4.0, vec3(0.0, 1.0, 0.0), vec3(0.0, 0.0, 0.0));
	s2.rotate(M_PI/4.0, vec3(0.0, 1.0, 0.0), vec3(0.0, 0.0, 0.0));
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return !r;
}


bool collisions_hexahedron_cylinder()
{
	Hexahedron_hitbox s1;
	Cylinder_hitbox s2;
	s1.create(paver_vector(vec3(sqrt(2.0)/2.0,sqrt(2.0)/2.0,sqrt(2.0)/2.0)+vec3(0.1,0.1,0.1)));
	s2.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), 5.0, sqrt(2.0)/2.0);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return r;
}
bool no_collisions_hexahedron_cylinder()
{
	Hexahedron_hitbox s1;
	Cylinder_hitbox s2;
	s1.create(paver_vector(vec3(sqrt(2.0)/2.0,sqrt(2.0)/2.0,sqrt(2.0)/2.0)-vec3(0.1,0.1,0.1)));
	s2.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), 5.0, sqrt(2.0)/2.0);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return !r;
}

bool collisions_hexahedron_circle()
{
	Hexahedron_hitbox s1;
	Circle_hitbox s2;
	s1.create(paver_vector(vec3(sqrt(2.0)/2.0,sqrt(2.0)/2.0,sqrt(2.0)/2.0)+vec3(0.1,0.1,0.1)));
	s2.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), sqrt(2.0)/2.0);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return r;
}
bool no_collisions_hexahedron_circle()
{
	Hexahedron_hitbox s1;
	Circle_hitbox s2;
	s1.create(paver_vector(vec3(sqrt(2.0)/2.0,sqrt(2.0)/2.0,sqrt(2.0)/2.0)-vec3(0.1,0.1,0.1)));
	s2.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), sqrt(2.0)/2.0);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return !r;
}


bool collisions_cylinder_cylinder()
{
	Cylinder_hitbox s1;
	Cylinder_hitbox s2;
	s1.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0)-vec3(sqrt(2.0)/2.0,sqrt(2.0)/2.0,sqrt(2.0)/2.0)+vec3(0.1,0.0,0.0), 1.0, sqrt(2.0)/2.0);
	s2.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), 5.0, sqrt(2.0)/2.0);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return r;
}
bool no_collisions_cylinder_cylinder()
{
	Cylinder_hitbox s1;
	Cylinder_hitbox s2;
	s1.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0)-vec3(sqrt(2.0)/2.0,sqrt(2.0)/2.0,sqrt(2.0)/2.0)-vec3(0.1,0.0,0.0), 1.0, sqrt(2.0)/2.0);
	s2.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), 5.0, sqrt(2.0)/2.0);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return !r;
}


bool collisions_cylinder_circle()
{
	Cylinder_hitbox s1;
	Circle_hitbox s2;
	s1.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), 5.0, sqrt(2.0)/2.0);
	s2.create(vec3(0.0,0.5,0.0)+vec3(0.0, 0.1, 0.0), vec3(0.0,1.0,0.0), 0.5);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return r;
}
bool no_collisions_cylinder_circle()
{
	Cylinder_hitbox s1;
	Circle_hitbox s2;
	s1.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), 5.0, sqrt(2.0)/2.0);
	s2.create(vec3(0.0,0.5,0.0)-vec3(0.0, 0.1, 0.0), vec3(0.0,1.0,0.0), 0.5);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return !r;
}

bool collisions_circle_circle()
{
	Circle_hitbox s1;
	Circle_hitbox s2;
	s1.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), sqrt(2.0)/2.0);
	s2.create(vec3(0.0,0.5,0.0)+vec3(0.0, 0.1, 0.0), vec3(0.0,1.0,0.0), 0.5);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return r;
}
bool no_collisions_circle_circle()
{
	Circle_hitbox s1;
	Circle_hitbox s2;
	s1.create(vec3(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.0), vec3(1.0,1.0,0.0), sqrt(2.0)/2.0);
	s2.create(vec3(0.0,0.5,0.0)-vec3(0.0, 0.1, 0.0), vec3(0.0,1.0,0.0), 0.5);
	bool r=s1.is_in_collision(s2);
	s1.destroy();
	s2.destroy();
	return !r;
}