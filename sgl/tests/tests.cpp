#include <iostream>
#include <string>
#include "../sgl.hpp"
#include "../../game/game.hpp"
#include <vector>

using namespace std;

const string ter_red="\033[1;31m";
const string ter_green="\033[1;32m";
const string ter_orange="\033[1;33m";
const string ter_blue="\033[1;34m";
const string ter_purple="\033[1;35m";
const string ter_cyan="\033[1;36m";
const string ter_nocol="\033[1;0m";
const string failed=ter_red+"FAILED"+ter_nocol;
const string passed=ter_green+"PASSED"+ter_nocol;

#define TEST(function) TEST_APP(function)

#define TEST_APP(function)    \
	tmp_passed=function();\
	cout<<"  -"<< #function <<" : "<<(tmp_passed ? (passed) : (failed))<<endl;\
	test_number++;        \
	if(tmp_passed)        \
		test_passed++;

void section(const string&  title)
{
	cout<<endl<<ter_blue<<"== Testing "<<title<<" =="<<ter_nocol<<endl;
}

//========= TESTING FUNCTIONS ======

//Trivial
bool always_true(){return true;}
bool always_false(){return false;}

//Hitboxs
#include "includes/tests_hitboxs.hpp"

//Collisions
#include "includes/tests_collisions.hpp"

//Groups
#include "includes/tests_group.hpp"

//Objects
#include "includes/tests_object.hpp"

//unid
#include "includes/tests_unid.hpp"


//==========   MAIN   ==============

int main()
{
	cout<<endl<<ter_orange<<"=== ALL TESTS: ==="<<ter_nocol<<endl;
	int test_number=0;
	int test_passed=0;
	bool tmp_passed;

	init_sgl(1920, 1080, "tests"); 

	//======   TESTS   ======

	section("Trivial");
	{
		TEST(always_true)
	}
	
	section("Hitboxs");
	{
		// /!\   ORDER OF TESTS IS IMPORTANT 
		TEST(hitbox_creation)
		TEST(no_collisions_beetwen_shperes_no_move)
		TEST(no_collisions_beetwen_shperes_first_move)
		TEST(collisions_beetwen_shperes_first_move)
		TEST(is_in_collisions_with_id)
		TEST(no_collisions_shpere_triangle_first_move)
		TEST(decollision_beetwen_shperes)
		TEST(many_collisions_beetwen_shpere_and_triangles)
		TEST(many_same_collisions)
		TEST(hitbox_unsolidity)
		TEST(hitbox_collision_unsolidity)
		TEST(hitbox_resolidity)
		TEST(collision_after_many_collisions)
		TEST(hitbox_prescale)
		TEST(hitbox_scale)
		TEST(hitbox_scale_hexahedron)
		TEST(hitbox_no_scale_hexahedron)
		TEST(hitbox_prereactive_collision)
		TEST(hitbox_reactive_collision)
		TEST(hitbox_unset_reactive_collision)
		TEST(hitbox_get_copy)
		TEST(hitbox_destruction)
		TEST(hitbox_operator_eq_sphere)
		TEST(hitbox_operator_eq_sphere_not_same_id)
		TEST(hitbox_operator_eq_sphere_independent_move)
		TEST(hitbox_operator_eq_sphere_destroy)
		TEST(hitbox_maximum_hitbox_number_destroyed)
	}
	section("Collisions");
	{
		TEST_COLLISION(sphere, sphere)
		TEST_COLLISION(sphere, triangle)
		TEST_COLLISION(sphere, hexahedron)
		TEST_COLLISION(sphere, cylinder)
		TEST_COLLISION(sphere, circle)
		TEST_COLLISION(triangle, hexahedron)
		TEST_COLLISION(triangle, cylinder)
		TEST_COLLISION(triangle, circle)
		TEST_COLLISION(hexahedron, hexahedron)
		TEST_COLLISION(hexahedron, cylinder)
		TEST_COLLISION(hexahedron, circle)
		TEST_COLLISION(cylinder, cylinder)
		TEST_COLLISION(cylinder, circle)
		TEST_COLLISION(circle, circle)
	}

	section("Group");
	{
		TEST(group_creation)
		TEST(group_add_one_element)
		TEST(group_dont_contains_one_element)
		TEST(group_del_one_element)
		TEST(group_add_many_element)
		TEST(group_del_many_element)
		TEST(group_del_all_element)
		TEST(group_check_all_element_destroyed)
		TEST(group_destruction)

	}

	section("unid");
	{
		TEST(unid_private_creation)
		TEST(unid_private_instance)
		TEST(unid_private_copy_constructor)
		TEST(unid_creation)
		TEST(unid_diffusion)
		TEST(unid_change_diffusion)
		TEST(unid_many_diffusions)
	}
	
	section("Object");
	{
		TEST(object_creation)
		TEST(object_counts0)
		TEST(object_add_solids)
		TEST(object_detsroyed_colllision)
		TEST(object_put_in_solids)
		TEST(object_is_in_all_collisions_solids)
		TEST(object_is_in_collision)
		TEST(object_collision_with_hitbox)
		TEST(object_not_collision_with_hitbox)
		TEST(object_select_solid)
		TEST(object_copy_constructor)
		TEST(object_copy_constructor_all_collisions)
		TEST(object_copy_constructor_no_collisions)
		TEST(object_operator_eq)
		TEST(object_operator_eq_all_collisions)
		TEST(object_operator_eq_no_collisions)
		TEST(object_operator_eq_after_init)
		TEST(object_check_hitboxs_total_count)
		TEST(object_collision_with_solid)
		TEST(object_destroy_solid)
		TEST(object_pop_solid)
		TEST(object_pop_solid_recup)
		TEST(object_destroy)
	}

	//=======================

	close_sgl();

	cout<<endl<<ter_cyan<<"=== Summary: ==="<<ter_nocol<<endl;
	cout<<"  -Passed: ("<<test_passed<<"/"<<test_number<<")"<<endl;
	cout<<"  -Global: "<<((test_passed==test_number) ? (passed) : (failed))<<endl;
	return 0;
}