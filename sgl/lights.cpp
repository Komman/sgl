#include <stdio.h> 
#include <stdlib.h>
#include <vector>

#include "utils.hpp"
#include "lights.hpp"
#include "err.hpp"
#include "display.hpp"
#include "colors.hpp"

using namespace std;
using namespace glm;
using namespace sgl;

namespace sgl
{
	static light_group all_lights={vector<light_info>(0), vector<int>(0)};
	static vector<Light> added_light(0);
	static bool light_changed=true;

	bool Light::is_set() const
	{
		return (this->index!=-1);
	}

	void set_light(Light& l)
	{
		if(l.is_set())
		{
			err("light already initialized");
		}

		l.index=all_lights.lights_info.size();
		all_lights.lights_info.push_back(l.infos);
		all_lights.light_types.push_back(l.type);

		light_changed=true;
		//seer("set_light");
	}

	void add_light(Light l)
	{
		added_light.push_back(l);
		set_light(added_light[added_light.size()-1]);
	}

	void update_lights()
	{
		if(light_changed==true)
		{
			//seer("submit_lights");
			submit_lights(all_lights.lights_info.data(),
					all_lights.light_types.data(),
					all_lights.light_types.size());
			light_changed=false;
		}
	}

	Light::Light(const vec3& new_position, float new_intensity, vec3 new_color
		, float new_max_saturation, float distance_only_coef, float new_white_cursor, int new_type)
	{
		infos.position=new_position;
		infos.color=new_color;
		infos.parameter=vec3(new_intensity, new_white_cursor,new_max_saturation);
		infos.advanced_parameter=vec3(distance_only_coef,0.0,0.0);
		type=new_type;
	}

	void Light::modification_assert()
	{
		//seer("modification_assert");
		if(index<0){err("light not set");}
		if((long unsigned int)index>=all_lights.lights_info.size()){err("light deleted (index out of bounds)");}
		light_changed=true;
	}

	void Light::tp(const vec3& new_position)
	{
		this->modification_assert();
		//seer("light_tp");

		infos.position=new_position;
		all_lights.lights_info[index]=infos;
	}

	void Light::rotate(float angle, const vec3& axis, const vec3& point)
	{
		this->modification_assert();
		infos.position=rotate_point(infos.position, angle, axis, point);
		all_lights.lights_info[index]=infos;
	}

};



