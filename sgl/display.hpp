#ifndef _DISPLAY_HPP_
#define _DISPLAY_HPP_

#include "err.hpp"
#include "default.hpp"
#include "image.hpp"
#include "lights.hpp"

#include <stdio.h> 
#include <stdlib.h>
#include <iostream>     
#include <fstream> 
#include <vector>

#include <glm/glm.hpp> 

using namespace glm;
using namespace std;
using namespace sgl;

#define SGL_PRINT_SHADERS_LOAD true


namespace sgl
{
	void init_display();

	using Shader = GLuint;

	Shader load_shaders(string name);
	Shader get_current_shader();
	void use_shader(Shader s);
	void set_color_only_drawing();
	void set_drawing_texture(const Image& im);
	void set_draw_plan(const vec3& point_i, const vec3& d1, const vec3& d2);
	void submit_lights(light_info tl[], int l_types[], int n);
	
	enum DRAW_TYPES {DRAW_TYPE_COLOR_ONLY=0,
					 DRAW_TYPE_TEXTURE,

					N_DRAW_TYPES};

    inline void set_clear_color(const vec3& color){glClearColor(color.x, color.y, color.z, 0.0f);}
    void update_camera_position(const vec3& pos);

	class Display
	{
		friend void set_color_only_drawing();
		friend void set_drawing_texture(const Image& im);
		friend void set_draw_plan(const vec3& point_i, const vec3& d1, const vec3& d2);
		friend void init_display();
		friend void submit_lights(light_info tl[], int l_types[], int n);
		friend void update_camera_position(const vec3& pos);
	public:
		Display(){};
		void init(Shader initial_shader);
		void change_shader(Shader new_shader);

		inline Shader get_shader(){return current_shader;}
	protected:
		GLuint VertexArrayID;

		Shader current_shader;
		bool shaders_initialized=false;
		GLuint draw_type_ID;
		GLuint plan_ID;
		GLuint texture_ID;
		GLuint camera_position_ID;

		GLuint lights_ID;
		GLuint lights_types_ID;
		GLuint light_count_ID;
		GLuint n_params_light_info_ID;
	};
}

#endif //_DISPLAY_HPP_
