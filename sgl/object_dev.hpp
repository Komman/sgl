namespace dev
{
	template<typename T> struct obj_attr
	{
		obj_attr(){}
		obj_attr(const T& value, int indice)
		{
			attr=value.get_copy();
			vector_indice=indice;
		}

		void destroy()
		{
			if(attr!=NULL)
			{
				delete attr;
			}
		}

		unid_private vector_indice;
		T* attr=NULL;
	};
};