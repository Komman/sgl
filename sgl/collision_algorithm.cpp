#include "hitboxs.hpp"
#include "collisions.hpp"
#include "collision_algorithm_axis_data.hpp"
#include "utils_debug.hpp"
#include "keyboard_and_mouse.hpp"

using namespace std;
using namespace glm;
using namespace sgl;

namespace sgl
{

	//======== 1 : DIMENSIONS AXIS ======== 

	int Hitbox::minmax_indice(minmax_type mm)
	{
		return id_minmax_indice(this->id(),mm);
	}

	// ----- THE THREE DIMENSIONS : -----  
	
	static axis_data axis[3]; 

	// ----------------------------------

	

	static bool is_in_area_collision(int id1, int id2)
	{
		return axis_data::is_in_area_collision_axis_group(axis, id1, id2);
	}

	collision physic_id_collision(int id1, int id2)
	{
		if(is_in_area_collision(id1, id2))
		{
			hitbox_type self_type=hitboxs_infos()[id1].type;
			hitbox_type other_type=hitboxs_infos()[id2].type;
				
			test_collision ft=get_test_collision(self_type, other_type);
			collision c=(hitboxs_infos()[id1].*ft)(hitboxs_infos()[id2]);
			
			//autoset
			c.hbx_data=hitboxs_infos()[id2].hbx_data;
			if(c.id_indice!=-1)
			{
				c.id_indice=axis[0].minsmaxs[id_minmax_indice(id2, MIN)].get_indice_id();
			}

			return c;
		}
		return NO_COLLISION;
	}


	//======= 2 : INSTALL ====

	//returns the indice in the sort_binding where the minmax were inserted
	static int insert_minmax_in_sort_binding(int axis_id, float value, int minmax_indice)
	{
		bool inserted=false;
		unsigned int size=axis[axis_id].sort_binding.size();

		unsigned int i;
		for(i=0;i<size;i++)
		{	
			if(value<axis[axis_id].minsmaxs[ axis[axis_id].sort_binding[i] ].value)
			{
				axis[axis_id].sort_binding.insert(axis[axis_id].sort_binding.begin()+i, minmax_indice);
				axis[axis_id].minsmaxs[minmax_indice].sort_binding_indice=i;
				inserted=true;
				break;
			}
		}
		for(i++;i<size+1;i++)
		{
			axis[axis_id].minsmaxs[ axis[axis_id].sort_binding[i] ].sort_binding_indice++;
		}

		if(inserted)
		{
			return axis[axis_id].minsmaxs[minmax_indice].sort_binding_indice;
		}

		axis[axis_id].sort_binding.push_back(minmax_indice);
		axis[axis_id].minsmaxs[minmax_indice].sort_binding_indice=size;
		return size;
	}

	void Hitbox::install_current_collision(int slot_id)
	{
		sgl_curcol cc;
		cc.ids=intid_set(binding().size());

		if(current_collisions.size()<binding().size())
		{
			current_collisions.push_back(cc);
			for(sgl_curcol& curc : current_collisions)
			{
				curc.ids.new_id();
			}
		}
		else
		{
			current_collisions[slot_id]=cc;
		}
	}

	void Hitbox::install()
	{
		#ifdef SGL_CHECK_ALGO
		check_created();
		#endif

		if(axis[0].three_axis==NULL)
		{
			for(int i=0;i<NUMBER_AXIS;i++)
			{
				axis[i].three_axis=axis;
			}
		}

		minmax mm;
		vec3 min_h=this->get_min();
		vec3 max_h=this->get_max();

		int i_minmax[2];

		for(int a=0;a<3;a++)
		{
			//=== MINS AND MAXS===

			mm.set_indice_id(this->id_indice);

			install_current_collision(this->id_indice);

			mm.t=MIN;
			mm.value=min_h[a];
			axis[a].minsmaxs.push_back(mm);

			mm.t=MAX;
			mm.value=max_h[a];
			axis[a].minsmaxs.push_back(mm);

			//=== BINDINGS ===

			//TOOPT: can be dichotomical research
			
			i_minmax[MIN]=insert_minmax_in_sort_binding(a, min_h[a], min_indice() );
			i_minmax[MAX]=insert_minmax_in_sort_binding(a, max_h[a], max_indice() );

			//=== PROJECTED AREAS & HITBOX TO AREAS ===

			//PROJECTED AREAS
			axis[a].areas.push_back(axis[a].prev_area(i_minmax[MIN]));
			axis[a].areas.push_back(axis[a].prev_area(i_minmax[MAX]));


			//HITBOX TO AREAS
			axis[a].hitboxs_to_areas.push_back(axis[a].prev_hitboxs_to_areas(i_minmax[MIN]));
			axis[a].hitboxs_to_areas.push_back(axis[a].prev_hitboxs_to_areas(i_minmax[MAX]));
			for(unsigned int i=0;i<axis[a].hitboxs_to_areas.size();i++)
			{
				axis[a].hitboxs_to_areas[i].push_back(-1);
			}

			

			int i_s;
			for(int i=i_minmax[MIN];i<i_minmax[MAX];i++)
			{
				i_s=axis[a].sort_binding[i];

				//PROJECTED AREAS
				axis[a].areas[i_s].push_back(this->id());

				//HITBOX TO AREAS
				axis[a].hitboxs_to_areas[i_s][axis[a].hitboxs_to_areas[0].size()-1]=axis[a].areas[i_s].size()-1;

			}
		}

		for(unsigned int i=0;i<axis[0].minsmaxs.size();i++)
		{
			axis[0].add_current_collision(this->id(), axis[0].minsmaxs[i].id());
		}
		//see(this->self_id());
		//seer("created");
	}

	//======= 3 : UPDATE =======

	void Hitbox::update()
	{
		#ifdef SGL_COLLISINO_AGLO_PRINT_CYCLES
		uint64_t c=get_cycles();
		#endif

		//NEW MINSMAXS
		vec3 vmin_new=this->get_min();
		vec3 vmax_new=this->get_max();

		for(int a=0;a<3;a++)
		{	
			float old[2];
			old[MIN]=axis[a].min_coord(id());
			old[MAX]=axis[a].max_coord(id());	

			#ifdef SGL_PRINT_COLLISION_AGLO
			seer("=" + axis_string[a] + "=");
			#endif

			//UPDATE MINMAX & SORT BINDING
			if(vmin_new[a]-old[MIN]>0.0 && vmax_new[a]-old[MAX]>0.0)
			{
				#ifdef SGL_PRINT_COLLISION_AGLO
				seer("max:");
				#endif
				axis[a].minsmaxs[ this->max_indice() ].value = vmax_new[a];
				axis[a].resort_minmax(this->max_indice());
				
				#ifdef SGL_PRINT_COLLISION_AGLO
				seer("min:");
				#endif

				axis[a].minsmaxs[ this->min_indice() ].value = vmin_new[a];
				axis[a].resort_minmax(this->min_indice());
			}
			else
			{
				#ifdef SGL_PRINT_COLLISION_AGLO
				seer("min:");
				#endif
				axis[a].minsmaxs[ this->min_indice() ].value = vmin_new[a];
				axis[a].resort_minmax(this->min_indice());
				
				#ifdef SGL_PRINT_COLLISION_AGLO
				seer("max:");
				#endif
				axis[a].minsmaxs[ this->max_indice() ].value = vmax_new[a];
				axis[a].resort_minmax(this->max_indice());
			}

		}

		#ifdef SGL_COLLISINO_AGLO_PRINT_CYCLES
		c=get_cycles()-c;
		seer("kCYCLES UPDATE: " + to_string(c/1000));
		if(c>=50*1000)
		{
			SGL_war("update: " + to_string(c/1000));
		}
		#endif

	}

	//======= 4 : TEST =======

	
	collision Hitbox::collision_with(const int& hbx_id_indice)
	{
		int hbx_id=binding()[hbx_id_indice];
		if(!is_hitbox_solid(hbx_id))
			return NO_COLLISION;

		return physic_id_collision(this->id(), hbx_id);
	}

	collision Hitbox::collision_with(const Hitbox& h)
	{
		if(!h.is_solid())
			return NO_COLLISION;

		return physic_id_collision(this->id(), h.id());
	}

	int Hitbox::get_sort_minmax_indice(axis_names a, minmax_type mm)
	{
		return axis[a].minsmaxs[this->minmax_indice(mm)].sort_binding_indice;
	}

	axis_names Hitbox::optimised_axis()
	{
		int n_test;
		int best_axis=NO_AXIS;
		int best_n_test=3*axis[0].areas.size();

		for(int a=0;a<NUMBER_AXIS;a++)
		{
			n_test=get_sort_minmax_indice((axis_names)a, MAX)-get_sort_minmax_indice((axis_names)a, MIN)-1;
			n_test+=axis[a].areas[this->min_indice()].size()-1;//-1 for himself

			if(n_test<0)
				err("negative number of potential collisions");

			if(n_test==0)
			{
				#ifdef SGL_PRINT_COLLISION_AGLO_OPT
				seer("No collisions from empty axis: " + axis_string[a]);
				#endif

				return NO_AXIS; //no collision
			}

			#ifdef SGL_PRINT_COLLISION_AGLO_OPT
			seer("n_test on " + axis_string[a]+ ": " + to_string(n_test) );
			#endif

			if(n_test<best_n_test)
			{
				best_n_test=n_test;
				best_axis=a;
			}
		}

		return (axis_names)best_axis;
	}

	vector<int> Hitbox::all_potential_collisions_id_indice(axis_names a)
	{
		vector<int> pot=axis[a].areas[this->min_indice()];
		int imin=get_sort_minmax_indice(a, MIN);
		int imax=get_sort_minmax_indice(a, MAX);
		for(unsigned int i=0;i<pot.size();i++)
		{
			pot[i]=axis[a].hitbox_id_to_id_indice(pot[i]);
		}
		for(int i=imin+1;i<imax;i++)
		{
			minmax min_found=axis[a].minsmaxs[ axis[a].sort_binding[i] ];
			if(min_found.t == MIN)
			{
				pot.push_back(min_found.get_indice_id());
			}
		}
		return pot;
	}

	bool Hitbox::is_in_axis_collision(axis_names a, int hitbox_id)
	{
		if(axis[a].hitboxs_to_areas[this->min_indice()][hitbox_id] != -1 )
			return true;

		int imin=get_sort_minmax_indice(a, MIN);
		int imax=get_sort_minmax_indice(a, MAX);

		for(int i=imin+1;i<imax;i++)
		{
			minmax min_found=axis[a].minsmaxs[ axis[a].sort_binding[i] ];
			if(min_found.t == MIN && min_found.id() == hitbox_id)
				return true;
		}

		return false;
	}



	vector<collision> Hitbox::full_collisions_test()
	{
		//TODO optimised_axis() returns the sorted list of axis
		axis_names a=this->optimised_axis();

		if(a==NO_AXIS)
		{
			#ifdef SGL_PRINT_COLLISION_AGLO_N_TEST
			seer("n_tests: 0/"+to_string(hitboxs_infos().size()));
			#endif

			return vector<collision>(0);
		}
		
		#ifdef SGL_PRINT_COLLISION_AGLO_OPT
		seer("may be collision on: " + axis_string[a]);
		#endif

		vector<int> pot_col_id_indice=this->all_potential_collisions_id_indice(a);

		vector<axis_names> others_axis(0);
		for(int ax=0;ax<NUMBER_AXIS;ax++)
		{
			if(ax!=a)
				others_axis.push_back((axis_names)ax);
		}

		vector<collision> return_collisions(0);

		int ntest=0;

		for(int col_indice : pot_col_id_indice)
		{
			if(col_indice != id_indice)
			{
				bool all_others_axis_in_collision=true;
				for(axis_names ax : others_axis)
				{
					ntest++;

					if(!is_in_axis_collision(ax, binding()[col_indice]))
					{
						all_others_axis_in_collision=false;
					}
				}

				if(all_others_axis_in_collision)
				{
					collision c=physic_id_collision(this->id(), binding()[col_indice]);

					if(c.id_indice!=-1)
					{	
						c.id_indice=col_indice;
						return_collisions.push_back(c);
						current_collisions[id()].ids.add(col_indice);
					} 
				}
			}
		}

		#ifdef SGL_PRINT_COLLISION_AGLO_N_TEST
		seer("n_tests: "+to_string(ntest)+"/"+to_string(hitboxs_infos().size()));
		#endif

		vector<collision> real_collisions(0);

		for(collision& c : return_collisions)
		{
			if(is_hitbox_solid(binding()[c.id_indice]))
			{
				real_collisions.push_back(c);
			}
		}

		return real_collisions;
	}

	vector<collision> Hitbox::collisions()
	{
		vector<collision> r(0);

		#ifdef SGL_COLLISINO_AGLO_PRINT_CYCLES
		uint64_t cy=get_cycles();
		#endif	
		
		if(current_collisions[id()].curcol_enable)
		{
			collision c;
			/*print_vector(current_collisions[id()].ids.inid);
			seer(current_collisions[id()].ids.inid[0]);
			seer(binding()[current_collisions[id()].ids.inid[0]]);*/
			for(int curcol_indice : current_collisions[id()].ids.inid)
			{
				c=physic_id_collision(id(), binding()[curcol_indice]);
				if(c.id_indice != -1 && is_hitbox_solid(binding()[c.id_indice]))
				{
					r.push_back(c);
				}
			}
		}
		else
		{
			r=this->full_collisions_test();
			current_collisions[id()].curcol_enable=true;
		}
		
		#ifdef SGL_COLLISINO_AGLO_PRINT_CYCLES
		cy=get_cycles()-cy;
		see("kCYCLES TEST: " + to_string(cy/1000) + "     ");	
		#endif

		return r;

	}

	//======= 5 : DESTROY =======

	void Hitbox::destroy_self_binding_and_info()
	{
		vector<int>& bin=binding();
		vector<collision_data>& hitinf=hitboxs_infos();

		if(bin[id_indice] != (int)hitinf.size()-1 )
		{
			int last_bindign_indice=axis[0].minsmaxs[ axis[0].minsmaxs.size()-1 ].get_indice_id();
			bin[ last_bindign_indice ]=bin[id_indice];
		}
		switch_pop_vector(hitinf, bin[id_indice]);
	}
	void Hitbox::destroy()
	{
		//see(this->self_id());
		//seer("destroyed");
		
		if(id_indice==-1)
			return;//err("Hitbox " + to_string(binding()[id_indice]) + " already destroyed");

		vector<int>& bin=binding();
		
		this->destroy_self_binding_and_info();

		for(int a=0;a<NUMBER_AXIS;a++)
		{
			axis[a].destroy_area(id());
		}
		switch_pop_vector(current_collisions, id());

		bin[id_indice]=-1;
		id_indice=-1;
		created=false;

		while(binding().size()>0 && binding()[binding().size()-1]==-1)
		{
			binding().pop_back();
		}
	}

	//======= 6 : DEBUG =======

	#include "print_hitbox_algo.hpp"


};