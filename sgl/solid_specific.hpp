#ifndef _SOLID_SPECIFIC_HPP_
#define _SOLID_SPECIFIC_HPP_

#include <stdio.h> 
#include <stdlib.h>
#include <iostream>     
#include <fstream> 
#include <vector>

#include <glm/glm.hpp> 
#include <glm/gtx/rotate_vector.hpp>

#include "solid.hpp"

using namespace glm;
using namespace std;
using namespace sgl;


namespace sgl
{
	class Spheric_solid : public Solid
	{
	public:
		Spheric_solid(){}
		Spheric_solid(const Spheric_solid& s);

		virtual Solid* get_copy() const;

		const Spheric_solid& operator=(const Spheric_solid& s);
		
		template <typename... TArgs> Spheric_solid(TArgs... args){init(args...);}

		void init(const vec3& center, float radius, const vec3& color = WHITE);
	protected:
	};

	class Triangle_solid : public Solid
	{
	public:
		Triangle_solid(){}
		Triangle_solid(const Triangle_solid& s);

		virtual Solid* get_copy() const;

		const Triangle_solid& operator=(const Triangle_solid& s);
		
		template <typename... TArgs> Triangle_solid(TArgs... args){init(args...);}

		void init(const vector<vec3> points, const vec3& color = WHITE);
		void init(const vec3& p0, const vec3& p1, const vec3& p2, const vec3& color = WHITE);
	protected:
	};

	class Hexahedron_solid : public Solid
	{
	public:
		Hexahedron_solid(){}
		Hexahedron_solid(const Hexahedron_solid& s);

		virtual Solid* get_copy() const;

		const Hexahedron_solid& operator=(const Hexahedron_solid& s);
		
		template <typename... TArgs> Hexahedron_solid(TArgs... args){init(args...);}

		void init(const vec3& position, const vec3& direction1, const vec3& direction2, const vec3& direction3, const vec3& color = WHITE);
		void init(const vec3& position, const vec3& size, const vec3& color = WHITE)
		{
			this->init(position,
				   vec3(1.0,0.0,0.0)*size.x,
				   vec3(0.0,1.0,0.0)*size.y,
				   vec3(0.0,0.0,1.0)*size.z,
				   color);
		}
	protected:
	};

	class Cylinder_solid : public Solid
	{
	public:
		Cylinder_solid(){}
		Cylinder_solid(const Cylinder_solid& s);

		virtual Solid* get_copy() const;

		const Cylinder_solid& operator=(const Cylinder_solid& s);
		
		template <typename... TArgs> Cylinder_solid(TArgs... args){init(args...);}

		void init(const vec3& point, const vec3& sized_direction, float radius, const vec3& color = WHITE);
	protected:
	};

	class Circle_solid : public Solid
	{
	public:
		Circle_solid(){}
		Circle_solid(const Circle_solid& s);

		virtual Solid* get_copy() const;

		const Circle_solid& operator=(const Circle_solid& s);
		
		template <typename... TArgs> Circle_solid(TArgs... args){init(args...);}

		void init(const vec3& center, const vec3& normal, float radius, const vec3& color = WHITE);
	protected:
	};

};

#endif //_SOLID_SPECIFIC_HPP_
