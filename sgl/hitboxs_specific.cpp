#include "collisions.hpp"
#include "hitboxs.hpp"
#include "utils.hpp" 
#include "orientation.hpp"
#include <glm/gtx/vector_angle.hpp>

namespace sgl
{

	static vec3 min_coord(const vector<vec3>& v)
	{
		vec3 minv=v[0];
		for(unsigned int i=1;i<v.size();i++)
		{
			minv=min(minv ,v[i]);
		}
		return minv;
	}
	static vec3 max_coord(const vector<vec3>& v)
	{
		vec3 maxv=v[0];
		for(unsigned int i=1;i<v.size();i++)
		{
			maxv=max(maxv ,v[i]);
		}
		return maxv;
	}

	vec3 Hitbox::min_coord_check(const vector<vec3>& v)
	{
		#ifdef SGL_CHECK_ALGO
		check_created();
		#endif

		return min_coord(v);
	}

	vec3 Hitbox::max_coord_check(const vector<vec3>& v)
	{
		#ifdef SGL_CHECK_ALGO
		check_created();
		#endif

		return max_coord(v);
	}


	//=========== CREATE COPY ============

#define CHECK_AND_SET_TYPED_HITBOX(hbx_type, macro_type, typed_var_name, hitbox_name)\
	hbx_type* typed_var_name; \
	if(h.get_type()!=macro_type)   \
	{                        \
			err(string("Hitbox most be a ")+ #hbx_type +" for " + string(#hbx_type) + "::build_copy(const Hitbox& h)"); \
	}                        \
	else                     \
	{                        \
		typed_var_name=((hbx_type*)(&hitbox_name));   \
	}

	void Spheric_hitbox::build_copy(const Hitbox& h)
	{
		CHECK_AND_SET_TYPED_HITBOX(Spheric_hitbox, SGL_HITBOX_SPHERICAL, htyped, h)
		this->Hitbox::create(htyped->center(), htyped->radius());
	}
	void Triangle_hitbox::build_copy(const Hitbox& h)
	{
		CHECK_AND_SET_TYPED_HITBOX(Triangle_hitbox, SGL_HITBOX_TRIANGLE, htyped, h)
		this->Hitbox::create(htyped->points());
	}
	void Hexahedron_hitbox::build_copy(const Hitbox& h)
	{
		CHECK_AND_SET_TYPED_HITBOX(Hexahedron_hitbox, SGL_HITBOX_HEXAHEDRON, htyped, h)
		this->Hitbox::create(htyped->points());
	}
	void Cylinder_hitbox::build_copy(const Hitbox& h)
	{
		CHECK_AND_SET_TYPED_HITBOX(Cylinder_hitbox, SGL_HITBOX_CYLINDER, htyped, h)
		this->Hitbox::create(htyped->begin(), htyped->sized_direction(), glm::length(htyped->sized_direction()), htyped->radius());
	}
	void Circle_hitbox::build_copy(const Hitbox& h)
	{
		CHECK_AND_SET_TYPED_HITBOX(Circle_hitbox, SGL_HITBOX_CIRCLE, htyped, h)
		this->Hitbox::create(htyped->center(), htyped->radius());
	}

	//=========== MINS AND MAX =========== 


	
	vec3 Spheric_hitbox::get_min()
	{
		#ifdef SGL_CHECK_ALGO
		check_created();
		#endif

		float radius=hitboxs_infos()[id()].float_infos[1].x;
		return (hitboxs_infos()[id()].float_infos[0]-vec3(radius,radius,radius));
	}
	vec3 Spheric_hitbox::get_max()
	{
		#ifdef SGL_CHECK_ALGO
		check_created();
		#endif

		float radius=hitboxs_infos()[id()].float_infos[1].x;
		return (hitboxs_infos()[id()].float_infos[0]+vec3(radius,radius,radius));
	}

	vec3 Triangle_hitbox::get_min()
	{
		return this->min_coord_check(hitboxs_infos()[this->id()].float_infos);
	}
	vec3 Triangle_hitbox::get_max()
	{
		return this->max_coord_check(hitboxs_infos()[this->id()].float_infos);
	}


	vec3 Hexahedron_hitbox::get_min()
	{
		return this->min_coord_check(hitboxs_infos()[this->id()].float_infos);
	}
	vec3 Hexahedron_hitbox::get_max()
	{
		return this->max_coord_check(hitboxs_infos()[this->id()].float_infos);
	}


	vec3 Cylinder_hitbox::get_min()
	{
		//TOUPGRADE
		vec3& pos=this->begin();
		vec3& dir=this->sized_direction();
		vec3 rad=vec3(1.0,1.0,1.0)*radius();
		return (-rad+glm::min(pos,pos+dir));
	}
	vec3 Cylinder_hitbox::get_max()
	{
		//TOUPGRADE
		vec3& pos=this->begin();
		vec3& dir=this->sized_direction();
		vec3 rad=vec3(1.0,1.0,1.0)*radius();
		return (rad+glm::max(pos,pos+dir));
	}

	
	vec3 Circle_hitbox::get_min()
	{
		//TOUPGRADE
		vec3 pos=hitboxs_infos()[id()].float_infos[0];
		vec3 maxworse=vec3(1.0,1.0,1.0)*hitboxs_infos()[id()].float_infos[2].x;
		return glm::min(pos-maxworse,pos+maxworse);
	}
	vec3 Circle_hitbox::get_max()
	{
		//TOUPGRADE
		vec3 pos=hitboxs_infos()[id()].float_infos[0];
		vec3 maxworse=vec3(1.0,1.0,1.0)*hitboxs_infos()[id()].float_infos[2].x;
		return glm::max(pos-maxworse,pos+maxworse);
	}

	//=========== GET_POSITION ========

	vec3 Spheric_hitbox::get_position() const
	{
		return this->center();		
	}

	vec3 Triangle_hitbox::get_position() const
	{
		return this->points()[0];		
	}

	vec3 Hexahedron_hitbox::get_position() const
	{
		return this->points()[0];		
	}

	vec3 Cylinder_hitbox::get_position() const
	{
		return this->begin();		
	}

	vec3 Circle_hitbox::get_position() const
	{
		return this->center();		
	}

	//=============== TP =============== 

	static void tp_all_points(vector<vec3>& points, const vec3& new_position)
	{
		vec3 dp=new_position-points[0];
		for(vec3& p : points)
		{
			p+=dp;
		}
	}

	void Spheric_hitbox::compute_tp(const vec3& position)
	{
		hitboxs_infos()[id()].float_infos=sphere_collision_data(position, get_radius()).float_infos;
	}

	void Triangle_hitbox::compute_tp(const vec3& position)
	{
		tp_all_points(this->points(), position);
	}

	void Hexahedron_hitbox::compute_tp(const vec3& position)
	{
		tp_all_points(this->points(), position);
	}

	void Cylinder_hitbox::compute_tp(const vec3& position)
	{
		this->begin()=position;
	}

	void Circle_hitbox::compute_tp(const vec3& position)
	{
		this->center()=position;
	}

	//=============== ROTATE =============== 

	static void rotate_points(vector<vec3>& points, float angle, const vec3& axis, const vec3& point)
	{
		for(vec3& v : points)
		{
			v=rotate_point(v, angle, axis, point);
		}
	}

	void Spheric_hitbox::compute_rotate(float angle, const vec3& axis, const vec3& point)
	{
		this->center()=rotate_point(this->get_center(), angle, axis, point);
	}
	void Triangle_hitbox::compute_rotate(float angle, const vec3& axis, const vec3& point)
	{
		rotate_points(this->points(), angle, axis, point);
	}
	void Hexahedron_hitbox::compute_rotate(float angle, const vec3& axis, const vec3& point)
	{
		rotate_points(this->points(), angle, axis, point);
	}
	void Cylinder_hitbox::compute_rotate(float angle, const vec3& axis, const vec3& point)
	{
		this->begin()=rotate_point(this->begin(), angle, axis, point);
		this->sized_direction()=glm::rotate(this->sized_direction(), angle, axis);
	}
	void Circle_hitbox::compute_rotate(float angle, const vec3& axis, const vec3& point)
	{
		this->center()=rotate_point(this->center(), angle, axis, point);
		this->normal()=glm::rotate(this->normal(), angle, axis);
	}

	//=============== SCALE =============== 

	static void scale_all_points(vector<vec3>& points, float ratio, const vec3& pi)
	{
		for(vec3& p : points)
		{
			sgl::scale(p, ratio, pi);
		}
	}

	void Spheric_hitbox::compute_scale_by_point(float ratio, const vec3& point)
	{
		this->radius()=this->radius()*ratio;
		sgl::scale(this->center(), ratio, point);
	}
	void Triangle_hitbox::compute_scale_by_point(float ratio, const vec3& point)
	{
		scale_all_points(this->points(), ratio, point);
	}
	void Hexahedron_hitbox::compute_scale_by_point(float ratio, const vec3& point)
	{
		scale_all_points(this->points(), ratio, point);
	}
	void Cylinder_hitbox::compute_scale_by_point(float ratio, const vec3& point)
	{
		this->radius()=this->radius()*ratio;
		this->sized_direction()=this->sized_direction()*ratio;
		sgl::scale(this->begin(), ratio, point);
	}
	void Circle_hitbox::compute_scale_by_point(float ratio, const vec3& point)
	{
		this->radius()=this->radius()*ratio;
		sgl::scale(this->center(), ratio, point);
	}

	//=========== SET ROTATION ============ 

	void Spheric_hitbox::compute_set_rotation(const vec3& up_vector, const vec3& front_vector)
	{

	}
	void Triangle_hitbox::compute_set_rotation(const vec3& up_vector, const vec3& front_vector)
	{
		this->compute_vertexed_rotation(up_vector, front_vector);
	}
	void Hexahedron_hitbox::compute_set_rotation(const vec3& up_vector, const vec3& front_vector)
	{
		this->compute_vertexed_rotation(up_vector, front_vector);
	}
	void Cylinder_hitbox::compute_set_rotation(const vec3& up_vector, const vec3& front_vector)
	{
		sized_direction()=glm::normalize(up_vector)*glm::length(sized_direction());
	}
	void Circle_hitbox::compute_set_rotation(const vec3& up_vector, const vec3& front_vector)
	{
		normal()=glm::normalize(up_vector);
	}

};