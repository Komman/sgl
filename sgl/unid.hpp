#ifndef _UNID_HPP_
#define _UNID_HPP_
#include "err.hpp"

using namespace std;
using namespace sgl;

namespace sgl
{
	struct unid
	{
	public:
		unid(){id_ptr=NULL;}

		inline int id() const 
		{
			#ifdef SGL_CHECK_ERRORS
			if(id_ptr==NULL)
				err("unid::value not initialized");
			#endif

			return *id_ptr;
		}

		explicit operator int() const{
	        return this->id();
	    }

	    inline bool operator==(const int value) const
	    {
	    	return (this->id()==value);
	    }
	    inline bool operator==(const unid& u) const
	    {
	    	return (this->id()==u.id());
	    }

	protected:
		int* id_ptr = NULL;
	};


	// /!\ for unid_private: the copy constructor link like an unid 
	struct unid_private : public unid
	{
	public:
		unid_private()
		{
			id_ptr=new int;
			count_ptr=new int;
			*count_ptr=1;
		}
		unid_private(int id_value) : unid_private()
		{
			*id_ptr=id_value;
		}
		unid_private(const unid_private& u)
		{
			this->link_with(u);
		}

		inline const unid_private& operator=(const unid_private& u)
		{
			this->unlink();
			link_with(u);
			return *this;
		}
		~unid_private()
		{
			this->unlink();
		}



		inline const unid_private& operator=(const int& id)
		{
			*id_ptr=id;
			return *this;
		}
		inline bool operator==(const int value) const
	    {
	    	return (*id_ptr==value);
	    }
		inline bool operator==(const unid& id) const
		{
			return (id.id()==*id_ptr);
		}

		int* count_ptr=NULL;
	private:
		inline void unlink()
		{	
			#ifdef SGL_CHECK_ERRORS
			if(count_ptr==NULL)
				err("count_ptr=NULL");
			#endif

			(*count_ptr)--;
			if(*count_ptr==0)
			{
				delete id_ptr;
				delete count_ptr;
				id_ptr=NULL;
				count_ptr=NULL;
			}
		}
		inline void link_with(const unid_private& u)
		{
			id_ptr=u.id_ptr;
			count_ptr=u.count_ptr;

			#ifdef SGL_CHECK_ERRORS
			if(count_ptr==NULL)
				err("count_ptr=NULL");
			#endif

			(*count_ptr)++;
		}
	};
};

#endif //_UNID_HPP_
