#include "sgl/sgl.hpp"

#include <stdio.h> 
#include <stdlib.h>
#include <iostream>
#include <string> 
#include <vector>

#include "game/game.hpp"

using namespace glm;
using namespace std;
using namespace sgl;


int main()
{
	init_sgl(1920, 1080, "bouh"); 

	set_clear_color(dark(BLUE, 0.1));

	build_buldings();

	//Light cam_light(vec3(0.0,0.0,0.0), 500.0, vec3(1.0,0.5,0.2), 500.0, 0.0, 0.6);
	Light cam_light(vec3(0.0,0.0,0.0), 80.0, vec3(1.0,0.5,0.2), 6.0, 0.0);
	set_light(cam_light);

	Triangle_hitbox hbx_floor(vec3(-100.0,-1.0,-100.0), vec3(-100.0,0.1,100.0), vec3(100.0,0.0,100.0));

	Model m("models/epee");
	Shape_model sm(m, vec3(0.0,3.0,0.0));
	sm.scale(0.2);

	Player oo(vec3(6.0,5.0,2.0));


	bool prr=false;
	bool rot=false;
	vec3 up(0.0,1.0,0.0);
	vec3 front(1.0,0.0,0.0);
	while(!quit())
	{
		get_events();
		clear();

		if(key_pressed('A'))
		{
			oo.disable_inside_camera();
		}
		if(key_pressed('P'))
		{
			up=glm::normalize(glm::rotate(up,0.02f, front));
			rot=true;
		}
		if(key_pressed('M'))
		{
			front=glm::normalize(glm::rotate(front,0.01f, up));
			rot=true;
		}if(key_pressed('O'))
		{
			up=glm::normalize(glm::rotate(up,-0.02f, front));
			rot=true;
		}
		if(key_pressed('L'))
		{
			front=glm::normalize(glm::rotate(front,-0.01f, up));
			rot=true;
		}
		if(rot)
		{
			rot=false;
			oo.set_rotation(up,front);
		}
		if(key_pressed('I'))
		{
			oo.scale(1.01);
		}
		if(key_pressed('N'))
		{
			oo.move(-front*0.1f);
		}
		if(key_pressed('B'))
		{
			oo.move(front*0.1f);
		}

		if(key_pressed('E') && prr)
		{
			prr=false;
			add_light(Light(oo.get_position(), 80.0, vec3(1.0,0.5,0.2)));
		}
		if(!key_pressed('E'))
		{
			prr=true;
		}

		std::vector<collision> v=oo.collisions();
		if(false)
		{
			see("{");
			for(collision& c : v)
			{
				see(c.id_indice);
			}
			seer("}");
		}


		oo.all_collisions();

		vec3 camp=oo.get_position();
		cam_light.tp(camp);
		
		update_lights();

		oo.control_view();

		draw_buildings();
		oo.draw();
		//oo.draw_borders();
		sm.draw();

	    flip();
	}

	free_buildings();

	close_sgl();
	return 0;
}
